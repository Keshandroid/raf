package com.raf


import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.os.PersistableBundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.raf.R.*


open class BaseActivity  : AppCompatActivity() {
    private  var progress:Dialog?=null
    var activity : Activity?=null
    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)

      this.activity =this@BaseActivity
    }


    open fun showProgress() {

        progress = Dialog(this)
        progress!!.setContentView(R.layout.dialog_layout)
        progress!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progress!!.setCancelable(false)
        progress!!.show()
    }

    open fun dismissProgress() {
        progress!!.dismiss()
    }

    open fun showToast(message : String){
        Toast.makeText(this,message,Toast.LENGTH_SHORT).show()
    }
}