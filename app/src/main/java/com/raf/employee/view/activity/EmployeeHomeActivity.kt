package com.raf.employee.view.activity

import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.raf.R
import com.raf.employee.view.fragment.EmployeeAccountFragment
import com.raf.employee.view.fragment.HomeFragmentNew
import kotlinx.android.synthetic.main.activity_employee_home.*

class EmployeeHomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_home)

        employ_viewpager.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(
                employ_tab_layout
            )
        )

        employ_tab_layout.tabRippleColor = null
        val adpt = ViewPagerAdapter(supportFragmentManager, employ_tab_layout.tabCount)
        employ_viewpager.adapter = adpt

        employ_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {

            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (tab!!.position == 0) {
                        window.statusBarColor = getColor(R.color.app_header)
                    } else if (tab.position == 1) {
                        window.statusBarColor = getColor(R.color.app_header)
                    } else {
                        window.statusBarColor = Color.parseColor("#B9B9BA")

                    }
                }
                employ_viewpager.currentItem = tab!!.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })

    }

    class ViewPagerAdapter(fm: FragmentManager, val tabCount: Int) : FragmentPagerAdapter(fm) {
        override fun getCount(): Int {
            return tabCount
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    HomeFragmentNew()
                }
                1 -> {
                    EmployeeAccountFragment()
                }
                else -> {
                    getItem(position)
                }
            }
        }
    }
}