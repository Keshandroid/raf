package com.raf.employee.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.raf.R
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.activity.CustomerLoginActivity
import com.raf.customer.view.activity.UpdateProfileActivity
import com.raf.employee.view.activity.*
import kotlinx.android.synthetic.main.employee_acc_fragment.*
import kotlinx.android.synthetic.main.topbar_view2.*

class EmployeeAccountFragment() : Fragment(R.layout.employee_acc_fragment) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txt_heading2.setText(getString(R.string.account))

        linear_employee_profile.setOnClickListener(View.OnClickListener {
            startActivity(Intent(activity, EmployeeUpdateProfileActivity::class.java))
        })

        linear_employee_changepasword.setOnClickListener(View.OnClickListener {
            requireActivity().startActivity(Intent(activity, EmployeeChangePwdActivity::class.java))
        })
        linear_employee_privacy.setOnClickListener(View.OnClickListener {
            requireActivity().startActivity(Intent(activity, EmployeePrivacyPolicy::class.java))
        })
        linear_employee_terms.setOnClickListener(View.OnClickListener {
            requireActivity().startActivity(
                Intent(
                    activity,
                    EmployeeTermsConditionActivity::class.java
                )
            )
        })
        linear_employee_contactus.setOnClickListener(View.OnClickListener {
            requireActivity().startActivity(Intent(activity, EmployeeContactUsActivity::class.java))

        })

        linear_employee_logout.setOnClickListener(View.OnClickListener {
            var appPreference = AppPreference(requireContext())
            appPreference.clearData(requireContext())
            appPreference.setBoolean(AppConstant.ISLOGIN, false)
            startActivity(Intent(activity, EmployeeLoginActivity::class.java))
            requireActivity().finishAffinity()
        })
    }
}