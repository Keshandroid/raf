package com.raf.employee.view.activity

import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.util.Patterns
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.core.app.ActivityCompat
import com.bumptech.glide.Glide
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.employee.EmployeeGetProfileController
import com.raf.customer.controller.employee.EmployeeUpdateProfileController
import com.raf.customer.retrofit.response.GetProfileResponse
import com.raf.customer.retrofit.response.UpdateProfileResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeGetProfileView
import com.raf.customer.view.viewInterface.IEmployeeUpdateProfileView
import com.ramo.utils.ImageFilePath
import kotlinx.android.synthetic.main.activity_register_activtiy.*
import kotlinx.android.synthetic.main.activity_update_profile.*
import kotlinx.android.synthetic.main.topbar_view.*
import java.io.ByteArrayOutputStream

class EmployeeUpdateProfileActivity : BaseActivity(), IEmployeeUpdateProfileView,
    IEmployeeGetProfileView {
    lateinit var iEmployeeUpdateProfileController: EmployeeUpdateProfileController
    lateinit var iEmployeeGetProfileController: EmployeeGetProfileController
    var gender = ""
    var imageuri: Uri? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_update_profile)
        window.statusBarColor = getColor(R.color.app_header)
        txt_heading.setText(getString(R.string.profile))
        iEmployeeUpdateProfileController = EmployeeUpdateProfileController(this, this)

        var appPreference = AppPreference(this)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)

        iEmployeeGetProfileController = EmployeeGetProfileController(this, this)
        iEmployeeGetProfileController.callGetProfileApi(uid!!, utoken!!)

        btn_save.setOnClickListener(View.OnClickListener {
            if (setValidation()) {
                // Utils.printLog(TAG, "onCreate:"+uid+"\t\t"+utoken+gender)
                var fnm = et_update_firstname.text.toString()
                var lnm = et_update_lastname.text.toString()
                var email = et_update_email.text.toString()
                var phone = et_update_phone.text.toString()
                if (imageuri != null) {
                    iEmployeeUpdateProfileController.callUpdateProfileApi(
                        uid,
                        utoken,
                        fnm,
                        lnm,
                        email,
                        phone,
                        gender,
                        ImageFilePath.getPath(this, imageuri!!) ?: ""
                    )


                } else {
                    iEmployeeUpdateProfileController.callUpdateProfileApi(
                        uid,
                        utoken,
                        fnm,
                        lnm,
                        email,
                        phone,
                        gender,
                        ""
                    )

                }
            }
        })

        img_update_profile.setOnClickListener(View.OnClickListener {

            if (checkpermission()) {

                selectProfilePic()
            }
        })
    }

    private fun selectProfilePic() {
        var choosedialog = Dialog(this)
        choosedialog.setCancelable(true)
        var layoutInflater = getLayoutInflater()
        var view = layoutInflater.inflate(R.layout.choose_profile_pic, null)
        var capturephoto: LinearLayout = view.findViewById(R.id.linear_capture_photo)
        var fromgallery: LinearLayout = view.findViewById(R.id.linear_take_from_gallary)
        choosedialog.setContentView(view)
        choosedialog.show()

        capturephoto.setOnClickListener(View.OnClickListener {
            captureFromCamera()
            choosedialog.dismiss()
        })

        fromgallery.setOnClickListener(View.OnClickListener {
            selectFromgallery()
            choosedialog.dismiss()
        })

    }

    private fun selectFromgallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, 1)
    }

    private fun captureFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, 2)

    }


    fun setValidation(): Boolean {


        if (et_update_firstname.text.toString() == "" && et_lastname.length() < 3) {
            Utils.showToast(this@EmployeeUpdateProfileActivity, "Name was to short")
            return false
        } else if (et_update_lastname.text.toString() == "" && et_update_lastname.length() < 3) {
            Utils.showToast(this, "LastName was to short")
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(et_update_email.text.toString()).matches()) {
            Utils.showToast(this, "Enter valid Email")
            return false
        } else if (et_update_phone.text.toString() == "" && et_update_phone.text.length < 10) {
            Utils.showToast(this@EmployeeUpdateProfileActivity, "Enter valid Number")
            return false
        }
        val id = update_radiogrp.checkedRadioButtonId
        if (id == -1) {


            Utils.showToast(this, "Select Gender")
            return false
        } else {
            var radioButton: RadioButton = findViewById(id)
            if (radioButton.text.toString() == "Male") {
                gender = "M"
            } else if (radioButton.text.toString() == "Female") {
                gender = "F"

            }
            return true
        }


    }


    @Override
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                if (resultCode == RESULT_OK) {

                    imageuri = data!!.data
                    Utils.printLog("taken image", "onActivityResult:" + imageuri)
                    ImageFilePath.getPath(this, imageuri!!)
                    Utils.printLog(
                        "ImagePath",
                        "onActivityResult: " + ImageFilePath.getPath(this, imageuri!!)
                    )
                    img_update_profile.setImageURI(imageuri)
                }
            }
            2 -> {
                if (resultCode == RESULT_OK) {
                    var bundle: Bundle? = data!!.extras
                    var bitmap: Bitmap = bundle!!.get("data") as Bitmap
                    val byte = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byte)
                    val path =
                        MediaStore.Images.Media.insertImage(this.contentResolver, bitmap, "", null)
                    imageuri = Uri.parse(path)
                    ImageFilePath.getPath(this, imageuri!!)
                    Utils.printLog(
                        "camera",
                        "onActivityResult: " + ImageFilePath.getPath(this, imageuri!!)
                    )
                    img_update_profile.setImageURI(imageuri)

                }
            }

        }
    }

    fun checkpermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA
                ), 0
            )
            return false

        }


    }


    private fun setDetails(
        name: String, email: String, pic: String, phone: String,
        gender: String, lastname: String
    ) {
        et_update_firstname.setText(name)
        et_update_lastname.setText(lastname)
        et_update_email.setText(email)
        et_update_phone.setText(phone)
        if (gender == "M") {
            update_radiogrp.check(R.id.upd_male)

        } else if (gender == "F") {
            update_radiogrp.check(R.id.upd_female)
        }

        Glide.with(this).load(pic).into(img_update_profile)
    }


    override fun onUpdateProfileSucsess(response: UpdateProfileResponse, message: String) {

    }

    override fun onGetProfileSuccess(response: GetProfileResponse, message: String) {

        if (message == "Profile_got_Success") {
            var name = response.getResult().firstname
            var email = response.getResult().email
            var pic = response.getResult().profile_pic
            var gender = response.getResult().gender
            var phone = response.getResult().contact_no
            var lastname = response.getResult().lastName

            setDetails(name, email, pic, phone, gender, lastname)

        }

    }

    override fun onFailure(msg: String) {
        Utils.printLog("UpdateProfile", "onFailure:=====>" + msg)

    }

    override fun showProgress() {
        super.showProgress()
    }

    override fun dismissProgress() {
        super.dismissProgress()
    }

}