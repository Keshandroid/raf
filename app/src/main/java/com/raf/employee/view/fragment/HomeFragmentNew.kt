package com.raf.employee.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import com.raf.R
import com.raf.employee.view.activity.EmployeeHomeActivity
import com.raf.employee.view.activity.EmployeeManageCategoryActivity
import com.raf.employee.view.activity.EmployeeManageOrderActivity
import com.raf.employee.view.activity.EmployeeManageServiceActivity
import kotlinx.android.synthetic.main.employee_home_fragment_new.*
import kotlinx.android.synthetic.main.topbar_view2.*

class HomeFragmentNew() : Fragment(R.layout.employee_home_fragment_new) {
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        txt_heading2.setText("Area Dipendente")

        ll_manage_category.setOnClickListener {
            startActivity(
                Intent(activity, EmployeeManageCategoryActivity::class.java)
            )
        }
        ll_manage_service.setOnClickListener {
            startActivity(
                Intent(activity, EmployeeManageServiceActivity::class.java)
            )
        }
        ll_manage_order.setOnClickListener {
            startActivity(
                Intent(activity, EmployeeManageOrderActivity::class.java)
            )
        }
    }
}