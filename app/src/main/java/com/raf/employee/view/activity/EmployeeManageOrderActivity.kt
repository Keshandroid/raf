package com.raf.employee.view.activity

import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.employee.view.fragment.EmployeeCompletedOrderFragment
import com.raf.employee.view.fragment.EmployeeCurrentOrderFragment
import kotlinx.android.synthetic.main.activity_employee_manage_order.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeManageOrderActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_manage_order)
        window.statusBarColor = getColor(R.color.app_header)

        txt_heading.setText(resources.getString(R.string.manage_order))
        back.setOnClickListener {
            finish()
        }
        viewpager_order.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(
                order_tablayout
            )
        )
        var adpt = ViewpagerAdapter(supportFragmentManager, order_tablayout.tabCount)
        order_tablayout.tabRippleColor = null
        order_tablayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewpager_order.currentItem = tab!!.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }
        })
        viewpager_order.adapter = adpt

    }

    override fun onResume() {
        super.onResume()
        var appPreference = AppPreference(this)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
    }


    class ViewpagerAdapter(fm: FragmentManager, var tabCount: Int) : FragmentPagerAdapter(fm) {
        override fun getCount(): Int {
            return tabCount
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    EmployeeCurrentOrderFragment()
                }
                1 -> {
                    EmployeeCompletedOrderFragment()
                }
                else -> {
                    getItem(position)
                }
            }
        }
    }
}