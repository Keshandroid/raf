package com.raf.employee.view.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.employee.EmployeeLoginController
import com.raf.customer.controller.listener.IEmployeeLoginController
import com.raf.customer.retrofit.response.employee.EmployeeLoginResponse
import com.raf.customer.utils.CustomPassword
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeLoginView
import kotlinx.android.synthetic.main.activity_customer_login.*
import kotlinx.android.synthetic.main.activity_employee_login.*
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.login_top.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeLoginActivity : BaseActivity(), IEmployeeLoginView {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_login)

        cust_login_back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        CustomPassword(et_emp_lgn_password, img_emp_seen)


        btn_emplogin.setOnClickListener(View.OnClickListener {
//          startActivity(Intent(this@EmployeeLoginActivity, EmployeeManageSubCategoryActivity::class.java))
            if (et_emp_lgn_email.text.toString() != "" && Utils.isEmailValid(et_emp_lgn_email.text.toString())) {
                if (et_emp_lgn_password.text.toString() != "") {
                    var email = et_emp_lgn_email.text.toString()
                    var password = et_emp_lgn_password.text.toString()
                    var iLoginController: IEmployeeLoginController =
                        EmployeeLoginController(this, this)
                    iLoginController.callLoginApi(email, password)
                } else {
                    Utils.showToast(this, "Please Enter Password")
                }
            } else {
                Utils.showToast(this, "Please Enter Valid Email")
            }

        })
        txt_emp_forgotpasword.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@EmployeeLoginActivity, EmployeeForgotPwdActivity::class.java))
        })

    }

    override fun onLoginSuccess(response: EmployeeLoginResponse, message: String) {
        if (message == "Login_Success") {

            startActivity(
                Intent(this@EmployeeLoginActivity, EmployeeHomeActivity::class.java)
            )

            /*  startActivity(Intent(this@CustomerLoginActivity, GetCategoryActivity::class.java))*/
            finish()
        }
    }

    override fun onFailure(msg: String) {
        Utils.printLog("LoginActivity", "onFailure:=====>" + msg)

    }
}