package com.raf.employee.view.activity

import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.R
import com.raf.customer.adapter.EmployeeHistoryDetailsAdapter
import kotlinx.android.synthetic.main.activity_employee_history_detail.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeHistoryDetailActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_history_detail)
        window.statusBarColor = getColor(R.color.app_header)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText("History Detail")

        var adpt=EmployeeHistoryDetailsAdapter(this)
        rc_employeehistorydetails.layoutManager=LinearLayoutManager(this)
        rc_employeehistorydetails.adapter=adpt

    }
}