package com.raf.employee.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.adapter.employee.ManageCategoryAdapter
import com.raf.customer.controller.GetCategoryController
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IGetCategoryView
import kotlinx.android.synthetic.main.activity_employee_manage_category.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeManageCategoryActivity : BaseActivity(), IGetCategoryView {
    lateinit var iGetCategoryController: GetCategoryController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_manage_category)
        window.statusBarColor = getColor(R.color.app_header)

        txt_heading.setText(getString(R.string.manage_category))

        iGetCategoryController = GetCategoryController(this, this)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        txt_add_category.setOnClickListener(View.OnClickListener {
            startActivity(
                Intent(this@EmployeeManageCategoryActivity, EmployeeAddCategoryActivity::class.java)
            )
        })
    }

    override fun onResume() {
        super.onResume()
        var appPreference = AppPreference(this)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
        var serviceproviderid = appPreference.getString(AppConstant.EMPLOYEE_SERVICE_PRO_ID)
        iGetCategoryController.callGetcategotyApi(uid, utoken, serviceproviderid)
    }

    override fun onGetCategorySuccess(response: GetCategoryResponse, message: String) {
        if (message == "Category_Got") {
            if (response.result != null && response.result!!.size > 0) {
                var adpt1 = ManageCategoryAdapter(this,response.result!!)
                recycler_view_manage_category.layoutManager = LinearLayoutManager(this)
                recycler_view_manage_category.adapter = adpt1
            }
        }
    }

    override fun onFailure(msg: String) {

    }
}