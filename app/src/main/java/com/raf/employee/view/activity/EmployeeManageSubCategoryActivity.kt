package com.raf.employee.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.adapter.employee.ManageSubCategoryAdapter
import com.raf.customer.controller.employee.GetSubCategoryEmpController
import com.raf.customer.retrofit.response.employee.GetEmpSubCategoryResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IGetEmpSubCategoryView
import kotlinx.android.synthetic.main.activity_employee_manage_sub_category.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeManageSubCategoryActivity : BaseActivity(), IGetEmpSubCategoryView {
    lateinit var iGetSubCategoryEmpController: GetSubCategoryEmpController
    var cat_id = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_manage_sub_category)
        window.statusBarColor = getColor(R.color.app_header)

        txt_heading.setText(getString(R.string.manage_sub_category))

        cat_id = intent.getStringExtra("cat_id")!!

        iGetSubCategoryEmpController = GetSubCategoryEmpController(this, this)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        txt_add_sub_category.setOnClickListener(View.OnClickListener {
            var intent = Intent(
                this@EmployeeManageSubCategoryActivity,
                EmployeeAddSubCategoryActivity::class.java
            )
            intent.putExtra("cat_id", cat_id)
            startActivity(intent)
        })

    }

    override fun onResume() {
        super.onResume()
        var appPreference = AppPreference(this)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
        var serviceproviderid = appPreference.getString(AppConstant.EMPLOYEE_SERVICE_PRO_ID)
        iGetSubCategoryEmpController.callGetSubCategoryEmpController(
            uid,
            utoken,
            serviceproviderid,
            cat_id
        )
    }

    override fun onGetEmpCategorySuccess(response: GetEmpSubCategoryResponse, message: String) {
        if (message == "Sub_Category_Got") {
            if (response.result != null && response.result!!.size > 0) {
                var adpt1 = ManageSubCategoryAdapter(this, response.result!!,cat_id)
                recycler_view_manage_sub_category.layoutManager = LinearLayoutManager(this)
                recycler_view_manage_sub_category.adapter = adpt1
            }
        }
    }


    override fun onFailure(msg: String) {

    }
}