package com.raf.employee.view.fragment

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.raf.R
import com.raf.customer.view.fragment.customer.CurrentBookingFragment
import kotlinx.android.synthetic.main.employee_home_fragment.*
import kotlinx.android.synthetic.main.topbar_view2.*

class HomeFragment():Fragment(R.layout.employee_home_fragment)
{
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        txt_heading2.setText("Booking")
        viewpager_emp_booking.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(emp_booking_tablayout))
        var adpt= ViewpagerAdapter(childFragmentManager,emp_booking_tablayout.tabCount)
        emp_booking_tablayout.tabRippleColor=null
        emp_booking_tablayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                viewpager_emp_booking.currentItem=tab!!.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }
        })
        viewpager_emp_booking.adapter=adpt
    }

    class ViewpagerAdapter(fm: FragmentManager, var tabCount: Int): FragmentPagerAdapter(fm) {
        override fun getCount(): Int {
            return tabCount
        }

        override fun getItem(position: Int): Fragment {
            return when(position)
            {
                0->{
                    CurrentBookingFragment()
                }
                1->{
                    CurrentBookingFragment()
                }
                else->{
                    getItem(position)
                }
            }
        }
    }
}
