package com.raf.employee.view.activity

import android.content.Intent
import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.raf.R
import kotlinx.android.synthetic.main.activity_employee_register.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeRegisterActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_register)
        window.statusBarColor = getColor(R.color.app_header)
    back.setOnClickListener({
        onBackPressed()
    })
        go_to_emp_login.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@EmployeeRegisterActivity, EmployeeLoginActivity::class.java))
        })
    }
}