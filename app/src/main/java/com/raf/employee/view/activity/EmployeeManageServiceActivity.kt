package com.raf.employee.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.util.Util
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.adapter.employee.ManageServiceAdapter
import com.raf.customer.controller.employee.GetEmpServiceController
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.retrofit.response.employee.GetEmpServiceResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetEmpServiceView
import kotlinx.android.synthetic.main.activity_employee_manage_service.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeManageServiceActivity : BaseActivity(), IGetEmpServiceView {
    lateinit var iGetEmpServiceController: GetEmpServiceController
    var cat_id = ""
    var sub_cat_id = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_manage_service)
        window.statusBarColor = getColor(R.color.app_header)

        txt_heading.setText(getString(R.string.manage_service))

        cat_id = intent.getStringExtra("cat_id")!!
        sub_cat_id = intent.getStringExtra("sub_cat_id")!!

        Utils.printLog("SERVICES====", cat_id + "=======" + sub_cat_id)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        txt_add_service.setOnClickListener(View.OnClickListener {
            var intent = Intent(this, EmployeeAddServiceActivity::class.java)
            intent.putExtra("cat_id", cat_id)
            intent.putExtra("sub_cat_id", sub_cat_id)
            startActivity(intent)
        })
    }

    override fun onGetEmpServiceSuccess(response: GetEmpServiceResponse, message: String) {
        if (message == "Service_Got") {
            var adpt1 = ManageServiceAdapter(this, response.result!!, cat_id, sub_cat_id)
            recycler_view_manage_service.layoutManager = LinearLayoutManager(this)
            recycler_view_manage_service.adapter = adpt1
        }
    }

    override fun onFailure(msg: String) {

    }

    override fun onResume() {
        super.onResume()
        var appPreference = AppPreference(this)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
        var serviceproviderid = appPreference.getString(AppConstant.EMPLOYEE_SERVICE_PRO_ID)

        iGetEmpServiceController = GetEmpServiceController(this, this)

        iGetEmpServiceController.callGetEmpServiceEmpController(
            uid,
            utoken,
            serviceproviderid,
            cat_id,
            sub_cat_id
        )
    }
}