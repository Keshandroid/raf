package com.raf.employee.view.activity

import android.os.Bundle
import android.view.View
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.employee.EmployeeForgotPasswordController
import com.raf.customer.retrofit.response.ForgotPasswordResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeForgotPassWordView
import kotlinx.android.synthetic.main.activity_employee_forgot_pwd.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeForgotPwdActivity : BaseActivity(), IEmployeeForgotPassWordView {
    lateinit var employeeForgotPasswordController: EmployeeForgotPasswordController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_forgot_pwd)
        window.statusBarColor = getColor(R.color.app_header)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText(getString(R.string.forgot_password))

        employeeForgotPasswordController = EmployeeForgotPasswordController(this, this)
        btn_emp_forgot.setOnClickListener(View.OnClickListener {
            if (et_emp_forgot_email.text.toString() != "" && Utils.isEmailValid(et_emp_forgot_email.text.toString())) {
                employeeForgotPasswordController.callForgotPasswordApi(et_emp_forgot_email.text.toString())
            } else {
                Utils.showToast(this, "Enter Valid Email")
            }
        })


    }


    override fun onForgotPasswordSuccess(response: ForgotPasswordResponse, message: String) {
        if (response.getStatus() == 1) {
            finish()
        }
    }

    override fun onFailure(msg: String) {

    }

    override fun showProgress() {
        super.showProgress()

    }

    override fun dismissProgress() {
        super.dismissProgress()
    }
}