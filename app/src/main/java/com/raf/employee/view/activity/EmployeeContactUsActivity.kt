package com.raf.employee.view.activity
import android.os.Bundle
import android.view.View
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.StaticPageController
import com.raf.customer.controller.listener.IStaticPageController
import com.raf.customer.retrofit.response.StaticPageResponse
import com.raf.customer.view.viewInterface.IStaticPageView
import kotlinx.android.synthetic.main.activity_employee_contact_us.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeContactUsActivity : BaseActivity(), IStaticPageView {
    lateinit var iStaticPageController: IStaticPageController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_contact_us)
        window.statusBarColor = getColor(R.color.app_header)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText(getString(R.string.contact_us))

        iStaticPageController = StaticPageController(this, this)
        iStaticPageController.callStaticPageApi("1")
    }

    override fun onIStaticPageSuccess(response: StaticPageResponse, message: String) {
        if (response.getStatus().equals(1)) {
            if (response.getStaticPageResult().contactNo != null &&
                response.getStaticPageResult().contactNo.length > 0
            ) {
                contact_no.setText(response.getStaticPageResult().contactNo)
            }
            if (response.getStaticPageResult().location != null &&
                response.getStaticPageResult().location.length > 0
            ) {
                location.setText(response.getStaticPageResult().location)
            }
        }
    }

    override fun onFailure(msg: String) {

    }
}