package com.raf.employee.view.activity

import android.os.Bundle
import android.text.Html
import android.view.View
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.StaticPageController
import com.raf.customer.controller.listener.IStaticPageController
import com.raf.customer.retrofit.response.StaticPageResponse
import com.raf.customer.view.viewInterface.IStaticPageView
import kotlinx.android.synthetic.main.activity_employee_terms_condition.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeTermsConditionActivity : BaseActivity(), IStaticPageView {
    lateinit var iStaticPageController: IStaticPageController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_terms_condition)
        window.statusBarColor = getColor(R.color.app_header)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText(getString(R.string.terms_and_condition))
        iStaticPageController = StaticPageController(this, this)
        iStaticPageController.callStaticPageApi("3")
    }

    override fun onIStaticPageSuccess(response: StaticPageResponse, message: String) {
        if (response.getStatus().equals(1)) {
            if (response.getStaticPageResult().title != null &&
                response.getStaticPageResult().title.length > 0
            ) {
                tv_heading1.setText(response.getStaticPageResult().title)
            }
            if (response.getStaticPageResult().shorDesc != null &&
                response.getStaticPageResult().shorDesc.length > 0
            ) {
                tv_heading5.setText(Html.fromHtml(response.getStaticPageResult().shorDesc))
            }

            if (response.getStaticPageResult().Desc != null &&
                response.getStaticPageResult().Desc.length > 0
            ) {
                tv_dis1.setText(Html.fromHtml(response.getStaticPageResult().Desc))
            }
        }
    }

    override fun onFailure(msg: String) {

    }
}