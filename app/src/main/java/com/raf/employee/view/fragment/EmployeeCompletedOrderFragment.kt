package com.raf.employee.view.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.R
import com.raf.customer.adapter.employee.EmployeeCurrentCompleteOrderAdapter
import com.raf.customer.controller.employee.EmployeeOrderController
import com.raf.customer.retrofit.response.employee.EmployeeOrderResponse
import com.raf.customer.retrofit.response.employee.SuccessResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IEmployeeOrderView
import com.raf.employee.interfaces.EmployeeOrderListener
import kotlinx.android.synthetic.main.fragment_employee_order.*


class EmployeeCompletedOrderFragment : Fragment(R.layout.fragment_employee_order),
    IEmployeeOrderView, EmployeeOrderListener {

    var controller: EmployeeOrderController? = null;
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var uid = ""
    var utoken = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        preferences = requireActivity().getSharedPreferences("Raf", AppCompatActivity.MODE_PRIVATE)
        editor = preferences.edit()
        var appPreference = AppPreference(this.requireContext())
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)
    }

    override fun onResume() {
        super.onResume()
        controller = EmployeeOrderController(this.requireActivity(), this)
        controller!!.callGetCompleteOrderApi(uid, utoken, "-1")
    }

    override fun onEmployeeCurrentOrderSuccess(response: EmployeeOrderResponse, message: String) {
    }

    override fun onEmployeeCompleteOrderSuccess(response: EmployeeOrderResponse, message: String) {
        if (response.getStatus() != 0) {
            if (response.result!!.size > 0) {
                rc_order.visibility = View.VISIBLE
                tv_not.visibility = View.GONE
                var adpt = EmployeeCurrentCompleteOrderAdapter(
                    activity,
                    response.result!!,
                    "EmployeeCompleteOrder",
                    this
                )
                rc_order.layoutManager = LinearLayoutManager(activity)
                rc_order.adapter = adpt
            } else {
                rc_order.visibility = View.GONE
                tv_not.visibility = View.VISIBLE
            }
        } else {
            rc_order.visibility = View.GONE
            tv_not.visibility = View.VISIBLE
        }
    }

    override fun onEmployeeOrderStatusSuccess(response: SuccessResponse, message: String) {

    }

    override fun onFailure(msg: String) {
        if (msg.equals("Employee_Complete_Order_got_UnSuccess", true)) {
            rc_order.visibility = View.GONE
            tv_not.visibility = View.VISIBLE
        }
    }

    override fun showProgress() {
    }

    override fun dismissProgress() {
    }

    override fun itemClick() {
    }
}