package com.raf.employee.view.activity

import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.employee.EmployeeChangePasswordController
import com.raf.customer.controller.listener.IEmployeeChangePasswordController
import com.raf.customer.retrofit.response.ChangePasswordResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.CustomPassword
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeChangePasswordView
import kotlinx.android.synthetic.main.activity_employee_change_pwd.*
import kotlinx.android.synthetic.main.topbar_view.*

class EmployeeChangePwdActivity : BaseActivity(), IEmployeeChangePasswordView {
    lateinit var iChangePasswordListener: IEmployeeChangePasswordController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_change_pwd)
        window.statusBarColor = getColor(R.color.app_header)
        CustomPassword(et_emp_old_pass, img_emp_old_seen)
        CustomPassword(et_emp_new_pass, img_emp_new_seen)
        CustomPassword(et_emp_confirm_pass, img_emp_confirmseen)

        iChangePasswordListener = EmployeeChangePasswordController(this, this)
        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText(getString(R.string.change_password))

        CustomPassword(et_emp_old_pass, img_emp_old_seen)
        CustomPassword(et_emp_new_pass, img_emp_new_seen)
        CustomPassword(et_emp_confirm_pass, img_emp_confirmseen)

        btn_change_pass.setOnClickListener(View.OnClickListener {
            if (setValidation()) {
                var appPreference = AppPreference(this)
                var uid = appPreference.getString(AppConstant.UID)
                var utoken = appPreference.getString(AppConstant.UTOKEN)

                var oldpass = et_emp_old_pass.text.toString()
                var newpass = et_emp_new_pass.text.toString()
                iChangePasswordListener.callChangePasswordApi(uid!!, utoken!!, oldpass, newpass)
            }
        })

    }

    fun setValidation(): Boolean {
        if (et_emp_old_pass.text.toString() == "" || et_emp_new_pass.text.toString() == "") {
            Utils.showToast(this, "Please fill blank fields")
            return false
        } else if (et_emp_old_pass.text.length < 6 && et_emp_new_pass.text.length < 6) {
            Utils.showToast(this, "Password is too short")
            return false
        } else if (et_emp_new_pass.text.toString() != et_emp_confirm_pass.text.toString()) {
            Utils.showToast(this, "Password and Confirm Password do not match")
            return false
        }

        return true
    }

    override fun onChangePasswordSuccess(response: ChangePasswordResponse, message: String) {

        if (response.getStatus() == 1) {
            finish()
        }

    }

    override fun onFailure(msg: String) {

    }
}