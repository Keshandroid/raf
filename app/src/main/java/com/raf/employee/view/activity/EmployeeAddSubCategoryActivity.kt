package com.raf.employee.view.activity

import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.employee.EmployeeAddCategoryController
import com.raf.customer.controller.employee.EmployeeAddSubCategoryController
import com.raf.customer.retrofit.response.ForgotPasswordResponse
import com.raf.customer.retrofit.response.employee.SuccessResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeAddCategoryView
import com.raf.customer.view.viewInterface.IEmployeeSubAddCategoryView
import com.ramo.utils.ImageFilePath
import kotlinx.android.synthetic.main.activity_employee_add_category.*
import kotlinx.android.synthetic.main.activity_employee_add_sub_category.*
import kotlinx.android.synthetic.main.activity_employee_add_sub_category.btn_save
import kotlinx.android.synthetic.main.topbar_view.*
import java.io.ByteArrayOutputStream

class EmployeeAddSubCategoryActivity : BaseActivity(), IEmployeeSubAddCategoryView {
    var imageuri: Uri? = null
    lateinit var iEmployeeAddSubCategoryController: EmployeeAddSubCategoryController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_add_sub_category)
        window.statusBarColor = getColor(R.color.app_header)

        txt_heading.setText(getString(R.string.add_sub_category))
        var cat_id = intent.getStringExtra("cat_id")

        iEmployeeAddSubCategoryController = EmployeeAddSubCategoryController(this, this)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        img_upload_sub_category.setOnClickListener(View.OnClickListener {

            if (checkPermission()) {
                selectProfilePic()
            }
        })

        var appPreference = AppPreference(this)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
        var serviceproviderid = appPreference.getString(AppConstant.EMPLOYEE_SERVICE_PRO_ID)

        btn_save.setOnClickListener(View.OnClickListener {
            if (setValidation()) {
                // Utils.printLog(TAG, "onCreate:"+uid+"\t\t"+utoken+gender)
                var strSubCatName = edt_sub_category_name.text.toString()
                var priceCurrency = edt_price_currency.text.toString()
                var subcategoryPrice = edt_sub_category_price.text.toString()
                if (imageuri != null) {
                    iEmployeeAddSubCategoryController.callAddSubCategoryApi(
                        uid,
                        utoken,
                        serviceproviderid,
                        cat_id.toString(),
                        strSubCatName,
                        ImageFilePath.getPath(this, imageuri!!) ?: "",
                        priceCurrency,
                        subcategoryPrice
                    )
                }
            }
        })


    }

    fun checkPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA
                ), 0
            )
            return false

        }


    }

    private fun selectProfilePic() {
        var choosedialog = Dialog(this)
        choosedialog.setCancelable(true)
        var layoutInflater = getLayoutInflater()
        var view = layoutInflater.inflate(R.layout.choose_profile_pic, null)
        var capturephoto: LinearLayout = view.findViewById(R.id.linear_capture_photo)
        var fromgallery: LinearLayout = view.findViewById(R.id.linear_take_from_gallary)
        choosedialog.setContentView(view)
        choosedialog.show()

        capturephoto.setOnClickListener(View.OnClickListener {
            captureFromCamera()
            choosedialog.dismiss()
        })

        fromgallery.setOnClickListener(View.OnClickListener {
            selectFromgallery()
            choosedialog.dismiss()
        })

    }

    private fun selectFromgallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, 1)
    }

    private fun captureFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, 2)

    }

    @Override
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                if (resultCode == RESULT_OK) {

                    imageuri = data!!.data
                    Utils.printLog("taken image", "onActivityResult:" + imageuri)
                    ImageFilePath.getPath(this, imageuri!!)
                    Utils.printLog(
                        "ImagePath",
                        "onActivityResult: " + ImageFilePath.getPath(this, imageuri!!)
                    )
                    img_upload_sub_category.setImageURI(imageuri)
                }
            }
            2 -> {
                if (resultCode == RESULT_OK) {
                    var bundle: Bundle? = data!!.extras
                    var bitmap: Bitmap = bundle!!.get("data") as Bitmap
                    val byte = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byte)
                    val path =
                        MediaStore.Images.Media.insertImage(this.contentResolver, bitmap, "", null)
                    imageuri = Uri.parse(path)
                    ImageFilePath.getPath(this, imageuri!!)
                    Utils.printLog(
                        "camera",
                        "onActivityResult: " + ImageFilePath.getPath(this, imageuri!!)
                    )
                    img_upload_sub_category.setImageURI(imageuri)
                }
            }
        }
    }

    fun setValidation(): Boolean {
        if (edt_sub_category_name.text.toString().isEmpty()) {
            Utils.showToast(this, "Please Enter Sub Category Name")
            return false
        } else if (edt_price_currency.text.toString().isEmpty()) {
            Utils.showToast(this, "Please Enter Price Currency")
            return false
        } else if (edt_sub_category_price.text.toString().isEmpty()) {
            Utils.showToast(this, "Please Enter Price")
            return false
        } else if (imageuri == null) {
            Utils.showToast(this, "Please Select Sub Category Image")
            return false
        }
        return true
    }

    override fun onSubCategoryAddSuccess(response: SuccessResponse, message: String) {
        finish()
    }

    override fun onFailure(msg: String) {

    }
}