package com.raf.employee.view.activity

import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.LinearLayout
import androidx.core.app.ActivityCompat
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.employee.EmployeeAddServiceController
import com.raf.customer.retrofit.response.employee.SuccessResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeAddServiceView
import com.ramo.utils.ImageFilePath
import kotlinx.android.synthetic.main.activity_employee_add_service.*
import kotlinx.android.synthetic.main.topbar_view.*
import java.io.ByteArrayOutputStream

class EmployeeAddServiceActivity : BaseActivity(), IEmployeeAddServiceView {
    var imageuri: Uri? = null
    var cat_id = ""
    var sub_cat_id = ""
    lateinit var iEmployeeAddServiceController: EmployeeAddServiceController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_employee_add_service)
        window.statusBarColor = getColor(R.color.app_header)

        cat_id = intent.getStringExtra("cat_id")!!
        sub_cat_id = intent.getStringExtra("sub_cat_id")!!

        txt_heading.setText(getString(R.string.add_service))
        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        img_upload_service.setOnClickListener(View.OnClickListener {

            if (checkPermission()) {
                selectProfilePic()
            }
        })

        iEmployeeAddServiceController = EmployeeAddServiceController(this, this)

        var appPreference = AppPreference(this)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
        var serviceproviderid = appPreference.getString(AppConstant.EMPLOYEE_SERVICE_PRO_ID)

        btn_service.setOnClickListener(View.OnClickListener {
            if (setValidation()) {
                // Utils.printLog(TAG, "onCreate:"+uid+"\t\t"+utoken+gender)
                var serviceName = edt_service_name.text.toString()
                var description = edt_service_description.text.toString()
                var average_time = edt_service_average_time.text.toString()
                if (imageuri != null) {
                    iEmployeeAddServiceController.callAddServiceApi(
                        uid,
                        utoken,
                        serviceproviderid,
                        cat_id,
                        sub_cat_id,
                        serviceName,
                        ImageFilePath.getPath(this, imageuri!!) ?: "",
                        description,
                        average_time,
                        "0"
                    )
                }
            }
        })

    }

    fun checkPermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA
                ), 0
            )
            return false

        }


    }

    private fun selectProfilePic() {
        var choosedialog = Dialog(this)
        choosedialog.setCancelable(true)
        var layoutInflater = getLayoutInflater()
        var view = layoutInflater.inflate(R.layout.choose_profile_pic, null)
        var capturephoto: LinearLayout = view.findViewById(R.id.linear_capture_photo)
        var fromgallery: LinearLayout = view.findViewById(R.id.linear_take_from_gallary)
        choosedialog.setContentView(view)
        choosedialog.show()

        capturephoto.setOnClickListener(View.OnClickListener {
            captureFromCamera()
            choosedialog.dismiss()
        })

        fromgallery.setOnClickListener(View.OnClickListener {
            selectFromgallery()
            choosedialog.dismiss()
        })

    }

    private fun selectFromgallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, 1)
    }

    private fun captureFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, 2)

    }

    @Override
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            1 -> {
                if (resultCode == RESULT_OK) {

                    imageuri = data!!.data
                    Utils.printLog("taken image", "onActivityResult:" + imageuri)
                    ImageFilePath.getPath(this, imageuri!!)
                    Utils.printLog(
                        "ImagePath",
                        "onActivityResult: " + ImageFilePath.getPath(this, imageuri!!)
                    )
                    img_upload_service.setImageURI(imageuri)
                }
            }
            2 -> {
                if (resultCode == RESULT_OK) {
                    var bundle: Bundle? = data!!.extras
                    var bitmap: Bitmap = bundle!!.get("data") as Bitmap
                    val byte = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byte)
                    val path =
                        MediaStore.Images.Media.insertImage(this.contentResolver, bitmap, "", null)
                    imageuri = Uri.parse(path)
                    ImageFilePath.getPath(this, imageuri!!)
                    Utils.printLog(
                        "camera",
                        "onActivityResult: " + ImageFilePath.getPath(this, imageuri!!)
                    )
                    img_upload_service.setImageURI(imageuri)
                }
            }
        }
    }

    fun setValidation(): Boolean {
        if (edt_service_name.text.toString().isEmpty()) {
            Utils.showToast(this, "Please Enter Service Name")
            return false
        } else if (edt_service_description.text.toString().isEmpty()) {
            Utils.showToast(this, "Please Enter Service Description")
            return false
        } else if (edt_service_average_time.text.toString().isEmpty()) {
            Utils.showToast(this, "Please Enter Average Time")
            return false
        } else if (imageuri == null) {
            Utils.showToast(this, "Please Select Service Image")
            return false
        }
        return true
    }

    override fun onAddServiceSuccess(response: SuccessResponse, message: String) {
        finish()
    }

    override fun onFailure(msg: String) {

    }
}