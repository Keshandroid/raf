package com.raf.employee.view.fragment

import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.R
import com.raf.customer.adapter.employee.EmployeeCurrentCompleteOrderAdapter
import com.raf.customer.controller.employee.EmployeeOrderController
import com.raf.customer.retrofit.response.GetCurrentBookingResponse
import com.raf.customer.retrofit.response.employee.EmployeeOrderResponse
import com.raf.customer.retrofit.response.employee.SuccessResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IEmployeeOrderView
import com.raf.employee.interfaces.EmployeeOrderListener
import kotlinx.android.synthetic.main.fragment_employee_order.*


class EmployeeCurrentOrderFragment : Fragment(R.layout.fragment_employee_order),
    IEmployeeOrderView, EmployeeOrderListener {

    var controller: EmployeeOrderController? = null;
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var uid = ""
    var utoken = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        preferences = requireActivity().getSharedPreferences("Raf", AppCompatActivity.MODE_PRIVATE)
        editor = preferences.edit()
        var appPreference = AppPreference(this.requireContext())
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)
        progress.visibility=View.VISIBLE
        controller = EmployeeOrderController(this.requireActivity(), this)
        controller!!.callGetCurrentOrderApi(uid, utoken)
    }

    override fun onResume() {
        super.onResume()

    }

    override fun onEmployeeCurrentOrderSuccess(response: EmployeeOrderResponse, message: String) {
        progress.visibility=View.GONE
        if (response.getStatus() == 0) {
            rc_order.visibility = View.GONE
            tv_not.visibility = View.VISIBLE
        } else if (response.getStatus() == 1) {
            if (response.result!!.size > 0) {
                rc_order.visibility = View.VISIBLE
                tv_not.visibility = View.GONE
                var adpt = EmployeeCurrentCompleteOrderAdapter(activity, response.result!!,"EmployeeCurrentOrder",this)
                rc_order.layoutManager = LinearLayoutManager(activity)
                rc_order.adapter = adpt
            } else {
                rc_order.visibility = View.GONE
                tv_not.visibility = View.VISIBLE
            }
        }

    }

    override fun onEmployeeCompleteOrderSuccess(
        response: EmployeeOrderResponse,
        message: String
    ) {
        progress.visibility=View.GONE

    }

    override fun onEmployeeOrderStatusSuccess(response: SuccessResponse, message: String) {
        progress.visibility=View.GONE

    }

    override fun onFailure(msg: String) {
        if (msg.equals("Employee_Current_Order_got_UnSuccess", true)) {
            progress.visibility=View.GONE
            rc_order.visibility = View.GONE
            tv_not.visibility = View.VISIBLE
        }
    }

    override fun showProgress() {
    }

    override fun dismissProgress() {
        progress.visibility=View.GONE

    }


    override fun itemClick() {
        controller = EmployeeOrderController(this.requireActivity(), this)
        controller!!.callGetCurrentOrderApi(uid, utoken)
    }
}