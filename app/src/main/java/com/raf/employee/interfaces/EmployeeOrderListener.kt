package com.raf.employee.interfaces

import com.raf.customer.retrofit.response.employee.EmployeeOrderResponse

interface EmployeeOrderListener {
    fun itemClick()
}