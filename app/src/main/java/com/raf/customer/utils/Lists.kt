package com.raf.customer.utils


import com.raf.customer.model.Booking
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.retrofit.response.employee.GetEmpServiceResponse
import com.raf.customer.retrofit.response.employee.GetEmpSubCategoryResponse

object Lists {
    var bookingList: ArrayList<Booking>? = null
    var result1: List<GetCategoryResponse.Result?>? = null
    var resultEmpSubCategoryActivity: List<GetEmpSubCategoryResponse.Result?>? = null
    var resultEmpService: List<GetEmpServiceResponse.Result?>? = null

    fun setCategoryList(result: List<GetCategoryResponse.Result?>) {
        result1 = result
    }

    fun getCategoryList(): List<GetCategoryResponse.Result?> {
        return result1!!
    }

    fun setEmpSubCategoryList(result: List<GetEmpSubCategoryResponse.Result?>) {
        resultEmpSubCategoryActivity = result
    }

    fun getEmpSubCategoryList(): List<GetEmpSubCategoryResponse.Result?> {
        return resultEmpSubCategoryActivity!!
    }
    fun setEmpServiceList(result: List<GetEmpServiceResponse.Result?>) {
        resultEmpService= result
    }

    fun getEmpServiceList(): List<GetEmpServiceResponse.Result?> {
        return resultEmpService!!
    }

    @JvmName("setBookingList1")
    fun setBookingList(list: ArrayList<Booking>) {
        bookingList = list
    }

    @JvmName("getBookingList1")
    fun getBookingList(): ArrayList<Booking> {
        return bookingList!!
    }
}
