package com.raf.customer.utils

object AppConstant {


    //Preference keys
    var USERID = "userid"

    // customer login
    var ISLOGIN = "islogin"
    var IS_SOCIAL_LOGIN = "is_social_login"
    var UID = "uid"
    var UTOKEN = "utoken"
    var FIRSNAME = "firsname"
    var LASTNAME = "lastname"
    var EMAIL = "email"
    var PHON_NO = "phon_no"
    var GENDER = "gender"
    var PIC = "pic"
    var USER_TYPE = "USER_TYPE"
    var EMPLOYEE_SERVICE_PRO_ID = "EMPLOYEE_SERVICE_PRO_ID"
    var EMPLOYEE = "EMPLOYEE"
    var CUSTOMER = "CUSTOMER"
    var PKey_SCANNED_QR_LIST = "scanned_qr_list"
    var PKey_LAST_SCANNED_ID = "last_scanned_id"




    //services key
    var SERVICE_PRO_ID="service_pro_id"
    var SELECTED_SERVICE_ID="selected_service_id"
    var CAT_ID="cat_id"
    var SUBCAT_ID="subcat_id"
    var SUBCAT_NAME="subcat_name"

}