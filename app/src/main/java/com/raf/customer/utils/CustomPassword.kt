package com.raf.customer.utils

import android.text.method.HideReturnsTransformationMethod
import android.text.method.PasswordTransformationMethod
import android.widget.EditText
import android.widget.ImageView
import com.raf.R

class CustomPassword(editText: EditText, imageView: ImageView) {
    var isSeen = false

    init {
        imageView.setOnClickListener {
            if(isSeen)
            {
                imageView.setImageResource(R.drawable.seen)
                editText.transformationMethod= PasswordTransformationMethod.getInstance()
                isSeen=false
            }
            else
            {
                imageView.setImageResource(R.drawable.hidden)
                editText.transformationMethod= HideReturnsTransformationMethod.getInstance()
                isSeen=true
            }
        }


    }

}