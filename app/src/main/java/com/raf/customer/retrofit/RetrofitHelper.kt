package com.julie.retrofit

import android.accounts.NetworkErrorException
import android.util.Log
//import com.errr.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.net.ConnectException
import java.net.UnknownHostException
import java.text.ParseException
import java.util.concurrent.TimeUnit
import java.util.concurrent.TimeoutException

object RetrofitHelper {

    private val SERVER_URL = "https://keshavinfotechdemo2.com/keshav/KG2/RAF/api/"
    private var gsonAPI: UserService? = null
//    private var connectionCallBack: ConnectionCallBack? = null

    fun getAPI(): UserService {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY
        val okHttpClient = OkHttpClient.Builder()
                .addInterceptor(logging)
                .readTimeout(60, TimeUnit.SECONDS)
                .connectTimeout(60, TimeUnit.SECONDS)
                .build()

        val gsonBuilder = GsonBuilder()
        gsonBuilder.setDateFormat("yyyy-MM-dd")
        gsonBuilder.disableHtmlEscaping()
        val retrofit = Retrofit.Builder()
                .client(okHttpClient)
                .baseUrl(SERVER_URL)
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .build()

        return retrofit.create(UserService::class.java)
    }

    fun getAPI(serverUrl: String): UserService { //        if (retrofit == null) {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BODY

        val okHttpClient = OkHttpClient.Builder()
        /* .addInterceptor(logging)
         .readTimeout(60, TimeUnit.SECONDS)
         .connectTimeout(60, TimeUnit.SECONDS)
         .build()*/

       /* if (BuildConfig.DEBUG)
            okHttpClient.addInterceptor(logging)*/

       /* val headerAuthorizationInterceptor: Interceptor = object : Interceptor {
            @Throws(IOException::class)
            override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
                var request: Request = chain.request()
                //val headers: okhttp3.Headers = request.headers.newBuilder().add("Authorization", stripeSecretKey).build()
                //request = request.newBuilder().headers(headers).build()
                return chain.proceed(request)
            }
        }*/
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setDateFormat("yyyy-MM-dd")
        gsonBuilder.disableHtmlEscaping()
        val TIMEOUT = 1 * 60 * 1000



        //okHttpClient.addInterceptor(headerAuthorizationInterceptor)
        val retrofit = Retrofit.Builder()
                .baseUrl(serverUrl)
                .client(okHttpClient.connectTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).readTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).writeTimeout(TIMEOUT.toLong(), TimeUnit.SECONDS).build())
                .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
                .build()

        return retrofit.create(UserService::class.java)
    }


    fun callApi(call: Call<ResponseBody?>?, callBack: ConnectionCallBack?) {

        call!!.enqueue(object : Callback<ResponseBody?> {
            override fun onResponse(call: Call<ResponseBody?>, response: Response<ResponseBody?>) {
                //Utils.appendLog(response.raw().request().url() + "  receive response: " + response.raw().receivedResponseAtMillis() + " send= " + response.raw().sentRequestAtMillis());
                //Utils.appendLog(response.raw().request().url() + "  diff= " + (response.raw().receivedResponseAtMillis() - response.raw().sentRequestAtMillis()));
                if (response.code() == 200) {
                    if (callBack != null) callBack!!.onSuccess(response)
                } else {
                    try {
                        var res = response.errorBody()!!.string()
                        if (response.code() == 400) {
                            res = " Bad request"
                        }

                        if (callBack != null) callBack!!.onError(response.code(), res)
                    } catch (e: IOException) {
                        Log.i("TAG", "onResponse: " + call.request().url)
                        e.printStackTrace()
                        if (callBack != null) callBack!!.onError(response.code(), e.message)
                    } catch (e: NullPointerException) {
                        Log.i("TAG", "onResponse: " + call.request().url)
                        e.printStackTrace()
                        if (callBack != null) callBack!!.onError(response.code(), e.message)
                    }
                }
            }

            override fun onFailure(call: Call<ResponseBody?>, error: Throwable) {
                var message: String? = null
                if (error is NetworkErrorException || error is ConnectException) {
                    message = "Please check your internet connection"
                } else if (error is ParseException) {
                    message = "Parsing error! Please try again after some time!!"
                } else if (error is TimeoutException) {
                    message = "Connection TimeOut! Please check your internet connection."
                } else if (error is UnknownHostException) {
                    message = "Please check your internet connection and try later"
                } else if (error is Exception) {
                    message = error.message
                }
                if (callBack != null) callBack!!.onError(-1, message)
            }
        })
    }

    interface ConnectionCallBack {
        fun onSuccess(body: Response<ResponseBody?>?)
        fun onError(code: Int, error: String?)
    }
}