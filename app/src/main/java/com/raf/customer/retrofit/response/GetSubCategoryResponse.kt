package com.raf.customer.retrofit.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetSubCategoryResponse {

    @SerializedName("status")
    @Expose
    var status:Int?=null

    @SerializedName("message")
    @Expose
    var message:String?=""

    @SerializedName("result")
    @Expose
    var subCategory: List<SubCategory?>?=null


    fun getStatus():Int
    {
        return status!!
    }
    @JvmName("getMessage1")
    fun getMessage():String
    {
        return message!!
    }

    @JvmName("getResult1")
    fun getSubCategoryList():List<SubCategory?>
    {
        return subCategory!!
    }


    class SubCategory
    {
        @SerializedName("subcategoryId")
        @Expose
        var subCategoryId:Int=0

        @SerializedName("subCategoryName")
        @Expose
        var subCategoryName:String=""

        @SerializedName("subcategoryImage")
        @Expose
        var subCategoryImage:String=""
        @SerializedName("subcategoryPrice")
        @Expose
        var subcategoryPrice:String=""

    }
}