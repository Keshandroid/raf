package com.raf.customer.retrofit.response

import com.google.gson.annotations.SerializedName

class GetBookingServiceListResponse {
    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""


    @SerializedName("result")
    var result: List<Result>? = null

    @JvmName("getStatus1")
    fun getStatus(): String {
        return status
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message
    }

    class Result {
        @SerializedName("serviceId")
        var serviceId: Int = -1

        @SerializedName("serviceName")
        var serviceName: String = ""

        @SerializedName("serviceDescription")
        var serviceDescription: String = ""

        @SerializedName("servicePrice")
        var servicePrice: String = ""

        @SerializedName("serviceImage")
        var serviceImage: String = ""

    }

   /* class ServiceData {
        @SerializedName("serviceId")
        var serviceId: Int = -1

        @SerializedName("serviceName")
        var serviceName: String = ""

        @SerializedName("serviceDescription")
        var serviceDescription: String = ""

        @SerializedName("servicePrice")
        var servicePrice: String = ""

        @SerializedName("serviceImage")
        var serviceImage: String = ""

    }
*/
}

