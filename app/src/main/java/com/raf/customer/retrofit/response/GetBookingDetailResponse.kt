package com.raf.customer.retrofit.response

import com.google.gson.annotations.SerializedName

class GetBookingDetailResponse {
    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

    @SerializedName("result")
    var result: List<Result>? = null

    @JvmName("getStatus1")
    fun getStatus():String
    {
        return status
    }

    @JvmName("getMessage1")
    fun getMessage():String
    {
        return message
    }
    class Result {
        @SerializedName("bookingId")
        var bookingIid: Int = -1

        @SerializedName("bookingDate")
        var bookingDate: String = ""

        @SerializedName("bookingTime")
        var bookingTime: String = ""

        @SerializedName("orderStatus")
        var orderStatus: Int = 0

        @SerializedName("serviceData")
        var serviceData: List<ServiceData>? = null

    }

    class ServiceData {
        @SerializedName("serviceId")
        var serviceId: Int = -1

        @SerializedName("serviceName")
        var serviceName: String = ""

        @SerializedName("serviceDescription")
        var serviceDescription: String = ""

        @SerializedName("servicePrice")
        var servicePrice: String = ""

        @SerializedName("serviceImage")
        var serviceImage: String = ""

    }
}



