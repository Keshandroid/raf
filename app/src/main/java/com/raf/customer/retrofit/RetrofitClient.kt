package com.raf.customer.retrofit

import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


object RetrofitClient {
    private var retrofit: Retrofit? = null
    private val SERVER_URL = "https://keshavinfotechdemo2.com/keshav/KG2/RAF/api/"
    private var gsonAPI: UserService? = null
    private var connectionCallBack: RetrofitHelper.ConnectionCallBack? = null

    fun getClient(): Retrofit? {
        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY
        val gsonBuilder = GsonBuilder()
        gsonBuilder.setDateFormat("yyyy-MM-dd")
        gsonBuilder.disableHtmlEscaping()
        val client =
            OkHttpClient.Builder().addInterceptor(interceptor).readTimeout(80, TimeUnit.SECONDS)
                .connectTimeout(80, TimeUnit.SECONDS).build()

        retrofit = Retrofit.Builder()
            .baseUrl(SERVER_URL)
            .addConverterFactory(GsonConverterFactory.create(gsonBuilder.create()))
            .client(client)
            .build()

        return retrofit
    }

}