package com.raf.customer.retrofit.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetTimeSlotsResponse {
    @SerializedName("status")
    @Expose
    var status:Int?=null

    @SerializedName("message")
    @Expose
    var message:String?=""

    @SerializedName("result")
    @Expose
    var timeSlots: List<String?>?=null


    fun getStatus():Int
    {
        return status!!
    }
    @JvmName("getMessage1")
    fun getMessage():String
    {
        return message!!
    }

    @JvmName("getResult1")
    fun getResult(): List<String?> {
        return timeSlots!!
    }


}