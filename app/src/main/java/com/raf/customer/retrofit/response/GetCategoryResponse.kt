package com.raf.customer.retrofit.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetCategoryResponse {

    @SerializedName("status")
    @Expose
    var status:Int?=null

    @SerializedName("message")
    @Expose
    var message:String?=""

    @SerializedName("result")
    @Expose
    var result: ArrayList<Result?>?=null


    fun getStatus():Int
    {
        return status!!
    }
    @JvmName("getMessage1")
    fun getMessage():String
    {
        return message!!
    }

    @JvmName("getResult1")
    fun getResult():List<Result?>
    {
            return result!!
    }


    class Result
    {
        @SerializedName("categoryId")
        @Expose
        var categoryId:Int=0

        @SerializedName("CategoryName")
        @Expose
        var categoryName:String=""

        @SerializedName("categoryImage")
        @Expose
        var categoryImage:String=""

    }
}