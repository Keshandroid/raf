package com.raf.customer.retrofit.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class StaticPageResponse {
    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = ""

    @SerializedName("result")
    @Expose
    var result: StaticPageResult? = null


    fun getStatus(): Int {
        return status!!
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message!!
    }

    fun getStaticPageResult(): StaticPageResult {
        return result!!
    }


    class StaticPageResult {

        @SerializedName("staticId")
        var sId: String = ""

        @SerializedName("staticTitle")
        var title:String=""

        @SerializedName("staticshortDescription")
        var shorDesc:String=""

        @SerializedName("staticDescription")
        var Desc:String=""

        @SerializedName("contactNumber")
        var contactNo:String=""

        @SerializedName("locaion")
        var location:String=""

    }
}