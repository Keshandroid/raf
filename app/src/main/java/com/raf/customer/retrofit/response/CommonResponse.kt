package com.raf.customer.retrofit.response

import com.google.gson.annotations.SerializedName

class CommonResponse {

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""


    @JvmName("getStatus1")
    fun getStatus():String
    {
        return status
    }

    @JvmName("getMessage1")
    fun getMessage():String
    {
        return message
    }


}