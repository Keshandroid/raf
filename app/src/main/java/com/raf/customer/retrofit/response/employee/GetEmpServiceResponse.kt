package com.raf.customer.retrofit.response.employee

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetEmpServiceResponse {

    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = ""

    @SerializedName("result")
    @Expose
    var result: ArrayList<Result?>? = null


    fun getStatus(): Int {
        return status!!
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message!!
    }

    @JvmName("getResult1")
    fun getResult(): List<Result?> {
        return result!!
    }

    class Result {

        @SerializedName("Avragetime")
        @Expose
        var avragetime: String = ""

        @SerializedName("Price")
        @Expose
        var price: String = ""

        @SerializedName("serviceId")
        @Expose
        var serviceId: String = ""

        @SerializedName("serviceName")
        @Expose
        var serviceName: String = ""


        @SerializedName("serviceImage")
        @Expose
        var serviceImage: String = ""

        @SerializedName("Description")
        @Expose
        var description: String = ""

    }
}