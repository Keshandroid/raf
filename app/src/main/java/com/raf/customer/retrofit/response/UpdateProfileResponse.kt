package com.raf.customer.retrofit.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class UpdateProfileResponse {


    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = ""

    @SerializedName("result")
    @Expose
    var result: Result? = null


    fun getStatus(): Int {
        return status!!
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message!!
    }

    @JvmName("getResult1")
    fun getResult(): Result {
        return result!!
    }

    class Result {
        @SerializedName("userId")
        @Expose
        val uid: Int = 0

        @SerializedName("userToken")
        @Expose
        var utoken = ""

        @SerializedName("firstName")
        @Expose
        var firstname: String = ""

        @SerializedName("lastName")
        @Expose
        var lastName: String = ""

        @SerializedName("email")
        @Expose
        var email: String = ""

        @SerializedName("contactNumber")
        @Expose
        var contact_no: String = ""

        @SerializedName("gender")
        @Expose
        val gender: String = ""

        @SerializedName("profilePhoto")
        @Expose
        val profile_pic: String = ""

    }
}
