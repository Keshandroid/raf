package com.raf.customer.retrofit.response.employee

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class SuccessResponse {

    @SerializedName("status")
    @Expose
    private var status:Int?=null


    @SerializedName("message")
    @Expose
    private var message:String?=null


    fun getStatus(): Int? {
        return status
    }


    fun getMessage(): String? {
        return message
    }


}