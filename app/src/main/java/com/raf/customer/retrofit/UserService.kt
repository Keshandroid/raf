package com.julie.retrofit

import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.json.JSONArray
import retrofit2.Call
import retrofit2.http.*

interface UserService {

    //customer APIs

    @POST("login")
    @FormUrlEncoded
    fun login(
        @Field(encoded = true, value = "email") email: String?,
        @Field(encoded = true, value = "password") password: String?
    ): Call<ResponseBody?>?

    @POST("sociallogin  ")
    @FormUrlEncoded
    fun socialLogin(
        @Field(encoded = true, value = "firstName") firstName: String?,
        @Field(encoded = true, value = "lastName") lastName: String?,
        @Field(encoded = true, value = "email") email: String?,
        @Field(encoded = true, value = "registration_type") registrationType: String?,
        @Field(encoded = true, value = "socialToken") socialToken: String?,
        @Field(encoded = true, value = "socialId") socialId: String?,
    ): Call<ResponseBody?>?


    @Multipart
    @POST("register")
    fun register(
        @Part("firstName") firstname: RequestBody,
        @Part("lastName") lastname: RequestBody,
        @Part("password") password: RequestBody,
        @Part("email") email: RequestBody,
        @Part("contactNumber") phoneno: RequestBody,
        @Part("gender") gender: RequestBody,
        @Part profilepic: MultipartBody.Part?
    ): Call<ResponseBody?>?


    @POST("changepassword")
    @FormUrlEncoded
    fun changepassword(
        @Field("userId") uid: String?,
        @Field("userToken") usertoken: String?,
        @Field("oldPassword") oldpassword: String?,
        @Field("newPassword") newpassword: String?
    ): Call<ResponseBody?>?


    @GET("getprofile")
    fun getprofile(
        @Query("userId") uid: String?,
        @Query("userToken") usertoken: String?
    ): Call<ResponseBody?>?

    @GET("getServiceTimeSlot")
    fun getServiceTimeSlot(
        @Query("userId") uid: String?,
        @Query("userToken") usertoken: String?,
        @Query("serviceProviderId") serviceProviderId: String?,
        @Query("bookingDate") bookingDate: String?
    ): Call<ResponseBody?>?

    @Multipart
    @POST("updateprofile")
    fun updateprofile(
        @Part("userId") uid: RequestBody,
        @Part("userToken") usertoken: RequestBody,
        @Part("firstName") firsname: RequestBody,
        @Part("lastName") lastname: RequestBody,
        @Part("email") email: RequestBody,
        @Part("contactNumber") contactno: RequestBody,
        @Part("gender") gender: RequestBody,
        @Part profileimg: MultipartBody.Part?
    ): Call<ResponseBody?>?


    @POST("forgotpassword")
    @FormUrlEncoded
    fun forgotpassword(
        @Field("email") email: String
    ): Call<ResponseBody?>?

    @POST("androidDeviceRegister")
    @FormUrlEncoded
    fun registerDevice(
        @Field("regId") regId: String,
        @Field("user_id") user_id: String,
        @Field("email") email: String,
        @Field("device_id") device_id: String



    ): Call<ResponseBody?>?

    @POST("verifyemailotp")
    @FormUrlEncoded
    fun verifyEmailOTP(
        @Field(encoded = true, value = "email") email: String,
        @Field(encoded = true, value = "otp") otp: String
    ): Call<ResponseBody?>?

    @Multipart
    @POST("addCategory")
    fun addCategory(
        @Part("userId") uid: RequestBody,
        @Part("userToken") usertoken: RequestBody,
        @Part("employeeId") employeeId: RequestBody,
        @Part("categoryName") categoryName: RequestBody,
        @Part categoryImage: MultipartBody.Part?
    ): Call<ResponseBody?>?

    @Multipart
    @POST("editCategory")
    fun editCategory(

        @Part("userId") uid: RequestBody,
        @Part("userToken") usertoken: RequestBody,
        @Part("categoryId") categoryId: RequestBody,
//        @Part("serviceProviderId") serviceProviderId: RequestBody,
        @Part("employeeId") serviceProviderId: RequestBody,
        @Part("categoryName") categoryName: RequestBody,
        @Part categoryImage: MultipartBody.Part?
    ): Call<ResponseBody?>?

    @Multipart
    @POST("addsubCategory")
    fun addSubCategory(
        @Part("userId") uid: RequestBody,
        @Part("userToken") usertoken: RequestBody,
        @Part("employeeId") employeeId: RequestBody,
        @Part("categoryId") categoryId: RequestBody,
        @Part("subcategoryName") subcategoryName: RequestBody,
        @Part subcategoryImage: MultipartBody.Part?,
        @Part("priceCurrency") priceCurrency: RequestBody,
        @Part("subcategoryPrice") subcategoryPrice: RequestBody,
    ): Call<ResponseBody?>?

    @Multipart
    @POST("addService")
    fun addService(
        @Part("userId") uid: RequestBody,
        @Part("userToken") usertoken: RequestBody,
        @Part("employeeId") employeeId: RequestBody,
        @Part("categoryId") categoryId: RequestBody,
        @Part("subcategoryId") subcategoryId: RequestBody,
        @Part("serviceName") serviceName: RequestBody,
        @Part serviceImage: MultipartBody.Part?,
        @Part("description") description: RequestBody,
        @Part("avrageTime") avrageTime: RequestBody,
        @Part("image_counter") image_counter: RequestBody,
    ): Call<ResponseBody?>?

    @Multipart
    @POST("editService")
    fun editService(
        @Part("userId") uid: RequestBody,
        @Part("userToken") usertoken: RequestBody,
        @Part("serviceId") serviceId: RequestBody,
        @Part("employeeId") employeeId: RequestBody,
        @Part("categoryId") categoryId: RequestBody,
        @Part("subcategoryId") subcategoryId: RequestBody,
        @Part("serviceName") serviceName: RequestBody,
        @Part serviceImage: MultipartBody.Part?,
        @Part("description") description: RequestBody,
        @Part("avrageTime") avrageTime: RequestBody,
        @Part("image_counter") image_counter: RequestBody
    ): Call<ResponseBody?>?

    @Multipart
    @POST("editsubCategory")
    fun editsubCategory(
        @Part("userId") uid: RequestBody,
        @Part("userToken") usertoken: RequestBody,
        @Part("subcategoryId") subcategoryId: RequestBody,
        @Part("employeeId") employeeId: RequestBody,
        @Part("categoryId") categoryId: RequestBody,
        @Part("subcategoryName") subcategoryName: RequestBody,
        @Part subcategoryImage: MultipartBody.Part?,
        @Part("priceCurrency") priceCurrency: RequestBody,
        @Part("subcategoryPrice") subcategoryPrice: RequestBody,
    ): Call<ResponseBody?>?

    @GET("getCategory")
    fun getcategory(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("serviceProviderId") service_id: String,
    ): Call<ResponseBody?>?

    @GET("deleteCategory")
    fun deleteCategory(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("serviceProviderId") service_id: String,
    ): Call<ResponseBody?>?

    @GET("getSubCategory")
    fun getSubCategory(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("serviceProviderId") service_id: String,
        @Query("categoryId") cat_id: String
    ): Call<ResponseBody?>?

    @POST("deletesubCategory")
    @FormUrlEncoded
    fun deleteSubCategory(
        @Field("userId") uid: String,
        @Field("userToken") utoken: String,
        @Field("subcategoryId") subcategoryId: String
    ): Call<ResponseBody?>?

    @GET("getService")
    fun getService(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("serviceProviderId") service_id: String,
        @Query("categoryId") cat_id: String,
        @Query("subcatId") subcat_id: String

    ): Call<ResponseBody?>?

    @POST("deleteService")
    @FormUrlEncoded
    fun getDeleteService(
        @Field("userId") uid: String,
        @Field("userToken") utoken: String,
        @Field("serviceId") serviceId: String

    ): Call<ResponseBody?>?

    @POST("addRating")
    fun addRating(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("serviceProviderId") service_id: String,
        @Query("rating") raing: Float,
        @Query("review") review: String

    ): Call<ResponseBody?>?

    @GET("getStaticPage")
    fun getStaticPage(
        @Query("staticId") sid: String
    ): Call<ResponseBody?>?

    @GET("getServiceProvider")
    fun getServiceProvider(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("serviceProviderId") service_id: String,

        ): Call<ResponseBody?>?

    @GET("get_currentBooking")
    fun getCurrentBooking(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("bookingStatus") bookingStatus: String
    ): Call<ResponseBody?>?

    @GET("get_completeBooking")
    fun getCompleteBooking(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("bookingStatus") bookingStatus: String
    ): Call<ResponseBody?>?

    @POST("addBooking")
    @FormUrlEncoded
    fun addBooking(
        @Field("userId") uid: String,
        @Field("userToken") utoken: String,
        @Field("serviceId") service_id: String,
        @Field("bookingDate") booking_date: String,
        @Field("bookingTime") booking_time: String,
        @Field("total_service_time") total_service_time: String

    ): Call<ResponseBody?>?

    @GET("get_BookingDetail")
    fun getBookingDetail(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("bookingId") bookingId: String
    ): Call<ResponseBody?>?

    @GET("get_BookingServiceList")
    fun getBookingServiceList(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("serviceId") serviceId: String
    ): Call<ResponseBody?>?


    //employee APIs

    @POST("empLogin")
    @FormUrlEncoded
    fun employeeLogin(
        @Field(encoded = true, value = "email") email: String?,
        @Field(encoded = true, value = "password") password: String?
    ): Call<ResponseBody?>?


    @POST("empforgotpassword")
    @FormUrlEncoded
    fun employeeForgotPassword(
        @Field("email") email: String
    ): Call<ResponseBody?>?

    @POST("changepassword")
    @FormUrlEncoded
    fun employeeChangePassword(
        @Field("userId") uid: String?,
        @Field("userToken") usertoken: String?,
        @Field("oldPassword") oldpassword: String?,
        @Field("newPassword") newpassword: String?
    ): Call<ResponseBody?>?

    @GET("getempprofile")
    fun getEmpProfile(
        @Query("userId") uid: String?,
        @Query("userToken") usertoken: String?
    ): Call<ResponseBody?>?

    @Multipart
    @POST("updateempprofile")
    fun updateEmpProfile(
        @Part("userId") uid: RequestBody,
        @Part("userToken") usertoken: RequestBody,
        @Part("firstName") firsname: RequestBody,
        @Part("lastName") lastname: RequestBody,
        @Part("email") email: RequestBody,
        @Part("contactNumber") contactno: RequestBody,
        @Part("gender") gender: RequestBody,
        @Part profileimg: MultipartBody.Part?
    ): Call<ResponseBody?>?

    @POST("cancelBooking")
    @FormUrlEncoded
    fun cancelBooking(  @Field("userId") uid: String?,
                        @Field("userToken") usertoken: String?,
                        @Field("bookingId") bookingId: String?)
            : Call<ResponseBody?>?


    @GET("get_empcompleteBooking")
    fun getEmpCompleteOrder(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String,
        @Query("bookingStatus") bookingStatus: String
    ): Call<ResponseBody?>?


    @POST("acceptRejectCompleteBooking")
    @FormUrlEncoded
    fun callOrderStatusApi(
        @Field("userId") uid: String,
        @Field("userToken") utoken: String,
        @Field("bookingId") bookingId: Int,
        @Field("bookingStatus") bookingStatus: String
    ): Call<ResponseBody?>?

    @GET("get_empcurrentBooking")
    fun getEmpCurrentOrder(
        @Query("userId") uid: String,
        @Query("userToken") utoken: String
    ): Call<ResponseBody?>?
}