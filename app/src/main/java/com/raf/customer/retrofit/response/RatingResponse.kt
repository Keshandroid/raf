package com.raf.customer.retrofit.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class RatingResponse {
    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = ""

    fun getStatus(): Int {
        return status!!
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message!!

    }
}