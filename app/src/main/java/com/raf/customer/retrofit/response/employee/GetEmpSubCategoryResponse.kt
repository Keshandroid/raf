package com.raf.customer.retrofit.response.employee

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetEmpSubCategoryResponse {

    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = ""

    @SerializedName("result")
    @Expose
    var result: ArrayList<Result?>? = null


    fun getStatus(): Int {
        return status!!
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message!!
    }

    @JvmName("getResult1")
    fun getResult(): List<Result?> {
        return result!!
    }

    class Result {
        @SerializedName("subcategoryId")
        @Expose
        var subcategoryId: Int = 0

        @SerializedName("subCategoryName")
        @Expose
        var subCategoryName: String = ""


        @SerializedName("subcategoryImage")
        @Expose
        var subcategoryImage: String = ""

        @SerializedName("subcategoryPrice")
        @Expose
        var subcategoryPrice: String = ""

    }
}