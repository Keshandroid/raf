package com.raf.customer.retrofit.response

import com.google.gson.annotations.SerializedName

class GetServiceProviderResponse {
    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

    @SerializedName("result")
    var result: List<Result>? = null


    @JvmName("getStatus1")
    fun getStatus():String
    {
        return status
    }

    @JvmName("getMessage1")
    fun getMessage():String
    {
        return message
    }

    @JvmName("getProviderResult1")
    fun getProviderResult():List<Result>
    {
        return result!!
    }

    class Result
    {
        @SerializedName("Id")
        var id:String=""

        @SerializedName("businessName")
        var businessName:String=""

        @SerializedName("firstName")
        var firstName:String=""

        @SerializedName("lastName")
        var lastName:String=""

        @SerializedName("email")
        var email:String=""

        @SerializedName("contactNumber")
        var contactNo:String=""

        @SerializedName("bio")
        var bio:String=""

        @SerializedName("location")
        var location:String=""

        @SerializedName("latitude")
        var latitude:String=""

        @SerializedName("longitude")
        var longitude:String=""

        @SerializedName("startTime")
        var startTime:String=""

        @SerializedName("endTime")
        var endTime:String=""

        @SerializedName("serviceProviderLogo")
        var providerLogo:String=""

        @SerializedName("serviceProviderImags")
        var providerImages:List<ServiceProviderImages>?=null

        @SerializedName("serviceProviderRatings")
        var providerRating:List<ServiceProviderRatings>?=null
    }


    class ServiceProviderImages
    {
        @SerializedName("serviceproviderImgId")
        var imgID:String=""

        @SerializedName("serviceproviderImgUrl")
        var providerImg:String=""
    }

    class ServiceProviderRatings
    {
        @SerializedName("reviewId")
        var reviewId:String=""

        @SerializedName("rating")
        var ratings:String=""

        @SerializedName("review")
        var review:String=""
    }
}
