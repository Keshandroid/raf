package com.raf.customer.retrofit.response

import com.google.gson.annotations.SerializedName

class GetCurrentBookingResponse {

    @SerializedName("status")
    var status: String = ""

    @SerializedName("message")
    var message: String = ""

    @SerializedName("result")
    var result: List<Result>? = null

    @JvmName("getStatus1")
    fun getStatus():String
    {
        return status
    }

    @JvmName("getMessage1")
    fun getMessage():String
    {
        return message
    }

    class Result {
        @SerializedName("bookingIid")
        var bookingIid: Int =-1

        @SerializedName("bookingDate")
        var bookingDate: String = ""

        @SerializedName("bookingTime")
        var bookingTime: String = ""

        @SerializedName("price")
        var price: String = ""

        @SerializedName("bookingStatus")
        var bookingStatus: String = ""

    }

}