package com.raf.customer.retrofit.response.employee

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class EmployeeOrderResponse {

    @SerializedName("status")
    @Expose
    var status:Int?=null

    @SerializedName("message")
    var message: String = ""

    @SerializedName("result")
    var result: List<Result>? = null

    fun getStatus(): Int {
        return status!!
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message!!
    }

    class Result {

        @SerializedName("bookingId")
        var bookingId: Int = 0

        @SerializedName("bookingDate")
        var bookingDate: String = ""

        @SerializedName("bookingTime")
        var bookingTime: String = ""

        @SerializedName("price")
        var price: String = ""

        @SerializedName("orderStatus")
        var orderStatus: Int = 0

    }

}