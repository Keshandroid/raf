package com.raf.customer.retrofit.response

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class GetServiceResponse {
    @SerializedName("status")
    @Expose
    var status: Int? = null

    @SerializedName("message")
    @Expose
    var message: String? = ""

    @SerializedName("result")
    @Expose
    var result: List<Result?>? = null


    fun getStatus(): Int {
        return status!!
    }

    @JvmName("getMessage1")
    fun getMessage(): String {
        return message!!
    }

    @JvmName("getResult1")
    fun getResult(): List<Result?> {
        return result!!
    }

    class Result {
        @SerializedName("serviceId")
        @Expose
        var serviceId: Int = 0

        @SerializedName("serviceName")
        @Expose
        var serviceName: String = ""

        @SerializedName("serviceImage")
        @Expose
        var serviceImage = ""

        @SerializedName("Description")
        @Expose
        var description: String = ""

        @SerializedName("Price")
        @Expose
        var price: String = ""

        @SerializedName("Avragetime")
        @Expose
        var avgTime = ""

        @SerializedName("serviceImages")
        @Expose
        var serviceImages: List<ServiceImages?>? = null


    }

    class ServiceImages {
        @SerializedName("serviceId")
        @Expose
        var serviceId: Int = 0

        @SerializedName("serviceImgUrl")
        @Expose
        var serviceImgUrl: String = ""
    }
}