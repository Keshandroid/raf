package com.raf.customer.controller.employee

import android.app.Activity
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IEmployeeLoginController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.employee.EmployeeLoginResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeLoginView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class EmployeeLoginController(var context: Activity, var iLoginView: IEmployeeLoginView) :
    IEmployeeLoginController {
    val TAG = "EmployeeLoginController"
    override fun callLoginApi(email: String, password: String) {
        iLoginView.showProgress()

        var apiInterface = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.employeeLogin(email, password)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iLoginView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: EmployeeLoginResponse =
                        builder.fromJson(reader, EmployeeLoginResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>Login_Success")
                        Utils.showToast(context, response.getMessage())
                        var appPreference = AppPreference(context)
                        appPreference.setBoolean(AppConstant.ISLOGIN, true)
                        appPreference.setString(AppConstant.UID, response.result!!.uid.toString())
                        appPreference.setString(AppConstant.UTOKEN, response.result!!.utoken)
                        appPreference.setString(AppConstant.FIRSNAME, response.result!!.firstname)
                        appPreference.setString(AppConstant.LASTNAME, response.result!!.lastName)
                        appPreference.setString(AppConstant.EMAIL, response.result!!.email)
                        appPreference.setString(AppConstant.PHON_NO, response.result!!.contact_no)
                        appPreference.setString(AppConstant.GENDER, response.result!!.gender)
                        appPreference.setString(AppConstant.PIC, response.result!!.profile_pic)
                        appPreference.setString(AppConstant.USER_TYPE, AppConstant.EMPLOYEE)
                        appPreference.setString(
                            AppConstant.EMPLOYEE_SERVICE_PRO_ID,
                            response.result!!.serviceproviderId
                        )

                        iLoginView.onLoginSuccess(response, "Login_Success")
                    } else if (response.getStatus() == 0) {
                        Utils.printLog(TAG, "onSuccess:=====>Login_UnSuccess")
                        Utils.showToast(context, response.getMessage())
                    }


                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e)
                    iLoginView.onFailure(e.message!!)
                }
            }

            override fun onError(code: Int, error: String?) {
                iLoginView.dismissProgress()
                Utils.showToast(context, "Something got wrong")
                iLoginView.onFailure(error!!)
            }
        })
    }
}