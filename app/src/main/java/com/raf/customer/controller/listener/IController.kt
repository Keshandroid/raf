package com.raf.customer.controller.listener

interface IChangePasswordController {
    fun callChangePasswordApi(uid: String, utoken: String, oldPass: String, newPass: String)
}

interface IforgotPasswordController {
    fun callForgotPasswordApi(email: String)
}

interface IregisterDeviceController {
    fun callRegisterDeviceApi(token: String,userId: String, email: String, deviceId: String)
}

interface IverifyEmailController {
    fun callVerifyEmailApi(email: String,otp: String)
}

interface ILoginController {
    fun callLoginApi(email: String, password: String)
    fun callSocialApi(
        firstName: String,
        lastName: String,
        email: String,
        registration_type: String,
        socialToken: String,
        socialId: String
    )
}

interface IEmployeeLoginController {
    fun callLoginApi(email: String, password: String)
}

interface IEmployeeChangePasswordController {
    fun callChangePasswordApi(uid: String, utoken: String, oldPass: String, newPass: String)
}

interface IEmployeeOrderController {
    fun callGetCurrentOrderApi(uid: String, utoken: String)
    fun callGetCompleteOrderApi(uid: String, utoken: String, bookingStatus: String)
    fun callOrderStatusApi(uid: String, utoken: String, bookingId: Int, bookingStatus: String)
}

interface IEmployeeAddCategoryController {
    fun callAddCategoryApi(
        id: String, utoken: String, employeeId: String,
        categoryName: String, categoryImage: String
    )

    fun callEditCategoryApi(
        id: String,
        utoken: String,
        category_id: String,
        employeeId: String,
        categoryName: String,
        categoryImage: String
    )
}

interface IEmployeeAddSubCategoryController {
    fun callAddSubCategoryApi(
        id: String, utoken: String, employeeId: String,
        categoryId: String, subcategoryName: String, subcategoryImage: String,
        priceCurrency: String, subcategoryPrice: String
    )

    fun callEditSubCategoryApi(
        id: String,
        utoken: String,
        subcategoryId: String,
        employeeId: String,
        categoryId: String,
        subcategoryName: String,
        subcategoryImage: String,
        priceCurrency: String,
        subcategoryPrice: String
    )
}

interface IEmployeeAddServiceController {
    fun callAddServiceApi(
        id: String, utoken: String, employeeId: String,
        categoryId: String, subcategoryId: String, serviceName: String,
        serviceImage: String, description: String, avrageTime: String, image_counter: String
    )

    fun callEditServiceApi(
        id: String, utoken: String, serviceId: String, employeeId: String,
        categoryId: String, subcategoryId: String, serviceName: String,
        serviceImage: String, description: String, avrageTime: String, image_counter: String

    )
}

interface IEmployeeGetProfileController {
    fun callGetProfileApi(uid: String, utkone: String)
}

interface IEmployeeUpdateProfileController {
    fun callUpdateProfileApi(
        id: String, utoken: String, firstname: String,
        lastname: String, email: String, phonno: String,
        gender: String, profile_pic: String
    )
}

interface IGetProfileController {
    fun calllGetProfileApi(uid: String, utkone: String)
}

interface IGetTimeSlotsController {
    fun calllGetTimeslotsApi(uid: String, utkone: String, service_pro_id: String, bookingDate: String)
}

interface IUpdateProfileController {
    fun callUpdateProfileApi(
        id: String, utoken: String, firstname: String,
        lastname: String, email: String, phonno: String,
        gender: String, profile_pic: String
    )
}

interface IRegisterController {
    fun callRegisterApi(
        firstname: String, lastname: String, password: String,
        email: String, phonno: String, gender: String, profile_pic: String
    )
}

interface IGetCategoryController {
    fun callGetcategotyApi(uid: String, utoken: String, serviceproviderid: String)
    fun callDeleteCategotyApi(uid: String, utoken: String, serviceproviderid: String)
}

interface IGetEmpSubCategoryController {
    fun callGetSubCategoryEmpController(
        uid: String,
        utoken: String,
        serviceproviderid: String,
        catId: String
    )
}

interface IGetEmpServiceController {
    fun callGetEmpServiceEmpController(
        uid: String,
        utoken: String,
        serviceproviderid: String,
        catId: String,
        subCatId: String
    )

    fun callDeleteServiceApi(
        uid: String,
        utoken: String,
        serviceId: String
    )
}

interface IGetSubCategoryController {
    fun callGetSubcategoryApi(
        uid: String,
        utoken: String,
        serviceproviderid: String,
        cat_id: String
    )

    fun callDeleteSubCategotyApi(
        uid: String,
        utoken: String,
        cat_id: String
    )
}

interface IGetServiceController {
    fun callGetServiceApi(
        uid: String,
        utoken: String,
        serviceproviderid: String,
        cat_id: Int,
        subcat_id: String
    )
}

interface IRatingController {
    fun callRatingApi(
        uid: String,
        utoken: String,
        serviceproviderid: String,
        rating: Float,
        review: String
    )
}

interface IStaticPageController {
    fun callStaticPageApi(static_id: String)
}

interface IGetServiceProviderController {
    fun callGetServiceProviderApi(uid: String, utoken: String, serviceproviderid: String)
}

interface IGetCurrentBookingController {
    fun callGetCurrentBookingApi(uid: String, utoken: String, bookingStatus: String)
    fun callCancelBookingAPI(uid: String, utoken: String, bookingId: String)
}

interface IGetCompletedBookingController {
    fun callGetCompletedBookingApi(uid: String, utoken: String, bookingStatus: String)
}

interface IGetBookingDetailController {
    fun callGetBookingDetailApi(uid: String, utoken: String, bookingId: String)
}

interface IGetBookingServiceListController {
    fun callGetBookingServiceListApi(uid: String, utoken: String, serviceId: String)
}

interface IAddBookingController {
    fun callAddBookingApi(
        userId: String,
        userToken: String,
        serviceId: String,
        bookingDate: String,
        bookingTime: String,
        total_service_time: String
    )
}