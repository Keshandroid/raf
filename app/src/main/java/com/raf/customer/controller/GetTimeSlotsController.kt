package com.raf.customer.controller

import android.app.Activity
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetProfileController
import com.raf.customer.controller.listener.IGetTimeSlotsController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetProfileResponse
import com.raf.customer.retrofit.response.GetTimeSlotsResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetProfileView
import com.raf.customer.view.viewInterface.IGetTimeSlotsView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class GetTimeSlotsController(var context: Activity, var iGetTimeSlotsView: IGetTimeSlotsView):
    IGetTimeSlotsController {
    val TAG ="GetTimeSlotsCtrl"
    override fun calllGetTimeslotsApi(uid: String, utkone: String, service_pro_id: String, bookingDate: String) {
        iGetTimeSlotsView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =apiInterface.getServiceTimeSlot(uid,utkone,service_pro_id,bookingDate)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetTimeSlotsView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: GetTimeSlotsResponse = builder.fromJson(reader, GetTimeSlotsResponse::class.java)

                    if (response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>Profile_got_Success")
                        Utils.showToast(context, response.getMessage()!!)
                        iGetTimeSlotsView.onGetTimeSlotSuccess(response, "Profile_got_Success")
                    }
                    else if(response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>Profile_got_UnSuccess")
                        Utils.showToast(context, response.getMessage()!!)
                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:"+e)
                    iGetTimeSlotsView.onFailure(e.message!!)
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetTimeSlotsView.dismissProgress()
                Utils.showToast(context,"Something got wrong")
                iGetTimeSlotsView.onFailure(error!!)
            }

        })
    }

}