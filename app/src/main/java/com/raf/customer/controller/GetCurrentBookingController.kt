package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetCurrentBookingController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.CommonResponse
import com.raf.customer.retrofit.response.GetCurrentBookingResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetCurrentBookingView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class GetCurrentBookingController(var context: Activity, var iGetCurrentBookingView: IGetCurrentBookingView) :
    IGetCurrentBookingController {
    var TAG="GetCurrentBookingController";
    override fun callGetCurrentBookingApi(uid: String, utoken: String, bookingStatus: String) {
        iGetCurrentBookingView.showProgress()

        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.getCurrentBooking(uid, utoken, bookingStatus)
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetCurrentBookingView.dismissProgress()
                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)
                    var builder = GsonBuilder().create()
                    var response: GetCurrentBookingResponse = builder.fromJson(
                        reader,
                        GetCurrentBookingResponse::class.java
                    )
                    if (response.getStatus().equals("1")) {
                        Utils.showToast(context, response.getMessage())
                        iGetCurrentBookingView.onIGetBookingSuccess(response, "Provider_Got")

                    } else if (response.getStatus().equals("0")) {
                        Utils.printLog(TAG, "onSuccess:===>Provider_got_failed")
                        Utils.showToast(context, response.getMessage())
                        iGetCurrentBookingView.onIGetBookingSuccess(
                            response,
                            "Provider_Got_failed"
                        )
                    }
                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess: test fail>>" + e.message)
                    iGetCurrentBookingView.onFailure(e.message ?: "")
                }

            }
            override fun onError(code: Int, error: String?) {
                iGetCurrentBookingView.dismissProgress()
                Utils.showToast(context, "Something Got Wrong")
                iGetCurrentBookingView.onFailure(error!!)
            }
        })
    }

    override fun callCancelBookingAPI(uid: String, utoken: String, bookingId: String) {
        iGetCurrentBookingView.showProgress()

        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.cancelBooking(uid, utoken, bookingId)
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetCurrentBookingView.dismissProgress()
                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)
                    var builder = GsonBuilder().create()
                    var response: CommonResponse = builder.fromJson(
                        reader,
                        CommonResponse::class.java
                    )
                    if (response.getStatus().equals("1")) {
                        iGetCurrentBookingView.onCacelBookingSuccess(response.getMessage())
                    } else  {
                        iGetCurrentBookingView.onCacelBookingFailed(response.getMessage())
                    }
                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess: test fail>>" + e.message)
                    iGetCurrentBookingView.onFailure(e.message ?: "")
                }

            }
            override fun onError(code: Int, error: String?) {
                iGetCurrentBookingView.dismissProgress()
                Utils.showToast(context, "Something Got Wrong")
                iGetCurrentBookingView.onFailure(error!!)
            }
        })
    }
}

