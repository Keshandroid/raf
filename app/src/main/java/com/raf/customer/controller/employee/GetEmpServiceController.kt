package com.raf.customer.controller.employee

import android.app.Activity
import android.content.Context
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetEmpServiceController
import com.raf.customer.controller.listener.IGetEmpSubCategoryController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.retrofit.response.employee.GetEmpServiceResponse
import com.raf.customer.retrofit.response.employee.GetEmpSubCategoryResponse
import com.raf.customer.utils.Lists
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetEmpServiceView
import com.raf.customer.view.viewInterface.IGetEmpSubCategoryView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class GetEmpServiceController(
    var context: Context,
    var iGetEmpServiceView: IGetEmpServiceView
) :
    IGetEmpServiceController {
    val TAG = "GetEmpServiceController"

    override fun callGetEmpServiceEmpController(
        uid: String,
        utoken: String,
        serviceproviderid: String,
        catId: String,
        subCatId: String
    ) {
        iGetEmpServiceView.showProgress()

        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =
            apiInterface.getService(uid, utoken, serviceproviderid, catId, subCatId)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetEmpServiceView.dismissProgress()
                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()

                    var response: GetEmpServiceResponse =
                        builder.fromJson(reader, GetEmpServiceResponse::class.java)
                    if (response.getStatus() == 1) {
                        Utils.showToast(context, response.getMessage())
                        iGetEmpServiceView.onGetEmpServiceSuccess(response, "Service_Got")
                        Utils.printLog(
                            TAG,
                            "onSuccess: " + response.getResult().get(0)!!.serviceName
                        )
                        Lists.setEmpServiceList(response.getResult())
                    } else if (response.getStatus() == 0) {
                        Utils.printLog(TAG, "onSuccess:===>Category_got_failed")
                        Utils.showToast(context, response.getMessage())
                        iGetEmpServiceView.onGetEmpServiceSuccess(
                            response,
                            "Sub_Category_Got_failed"
                        )

                    }
                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess: test fail>>" + e.message)
                    iGetEmpServiceView.onFailure(e.message ?: "")
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetEmpServiceView.dismissProgress()
                Utils.showToast(context, "Something Got Wrong")
                iGetEmpServiceView.onFailure(error!!)
            }
        })
    }

    override fun callDeleteServiceApi(uid: String,utoken: String,serviceId:String)
    {
        iGetEmpServiceView.showProgress()
        var apiInterface=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>?=apiInterface.getDeleteService(uid,utoken,serviceId)

        RetrofitHelper.callApi(call,object :RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetEmpServiceView.dismissProgress()
                try
                {
                    var res=body!!.body()!!.string()
                    var reader:Reader=StringReader(res)
                    var builder=GsonBuilder().create()
                    var response: GetEmpServiceResponse =builder.fromJson(reader, GetEmpServiceResponse::class.java)
                    if(response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:=====service_got")
                        Utils.showToast(context,response.getMessage())
                        iGetEmpServiceView.onGetEmpServiceSuccess(response,"sevice_got")
                    }
                    else if(response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:=====service_got_failed")
                        Utils.showToast(context,response.getMessage())
                    }
                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: "+e.message)
                    iGetEmpServiceView.onFailure(e.message!!)
                }
            }

            override fun onError(code: Int, error: String?) {
                iGetEmpServiceView.dismissProgress()
                Utils.showToast(context,"Something got wrong")
                iGetEmpServiceView.onFailure(error!!)
            }
        })
    }


}