package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IChangePasswordController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.ChangePasswordResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IChangePasswordView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader


class ChangePasswordController(var context: Activity, var iChangePasswordView: IChangePasswordView) : IChangePasswordController {
    val TAG = "ChangePassController"


    override fun callChangePasswordApi(
        uid: String,
        utoken: String,
        oldPass: String,
        newPass: String
    ) {
        val apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.changepassword(uid, utoken, oldPass, newPass)

        iChangePasswordView.showProgress()
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iChangePasswordView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: ChangePasswordResponse =
                        builder.fromJson(reader, ChangePasswordResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>PasswordChanged_Success")
                        Utils.showToast(context, response.getMessage()!!)
                        iChangePasswordView.onChangePasswordSuccess(response, "PassChanged_Success")
                    } else if (response.getStatus() == 0) {
                        Utils.printLog(TAG, "onSuccess:=====>PassChanged_UnSuccess")
                        Utils.showToast(context, response.getMessage()!!)
                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:" + e)
                    iChangePasswordView.onFailure(e.message!!)
                }

            }

            override fun onError(code: Int, error: String?) {
                iChangePasswordView.dismissProgress()
                Utils.showToast(context, "Something got wrong")
                iChangePasswordView.onFailure(error!!)
            }

        })
    }

}