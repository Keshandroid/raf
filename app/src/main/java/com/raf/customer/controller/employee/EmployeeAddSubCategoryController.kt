package com.raf.customer.controller.employee

import android.app.Activity
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IEmployeeAddCategoryController
import com.raf.customer.controller.listener.IEmployeeAddSubCategoryController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.ForgotPasswordResponse
import com.raf.customer.retrofit.response.employee.SuccessResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeAddCategoryView
import com.raf.customer.view.viewInterface.IEmployeeSubAddCategoryView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class EmployeeAddSubCategoryController(
    var context: Activity,
    var iAddSubCategoryView: IEmployeeSubAddCategoryView
) :
    IEmployeeAddSubCategoryController {
    val TAG = "EmployeeAddSubCategoryController"
    var pic: MultipartBody.Part? = null
    override fun callAddSubCategoryApi(
        id: String, utoken: String, employeeId: String,
        categoryId: String, subcategoryName: String, subcategoryImage: String,
        priceCurrency: String, subcategoryPrice: String

    ) {
        var imgpath = subcategoryImage
        Utils.printLog(TAG, "callAddApi: " + imgpath)
        val file = File(imgpath)
        if (file.exists()) {
            val reqimg = RequestBody.create(
                context.contentResolver.getType(Utils.getImageContentUri(context, file)!!)!!
                    .toMediaTypeOrNull(), file
            )
            pic = MultipartBody.Part.createFormData("subcategoryImage", file.name, reqimg)

        }
        val uid: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, id)
        val token: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, utoken)
        val strEmployeeId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, employeeId)
        val strCategoryId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, categoryId)
        val strSubCategoryName: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, subcategoryName)
        val strPriceCurrency: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, priceCurrency)
        val strSubcategoryPrice: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, subcategoryPrice)

        iAddSubCategoryView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =
            apiInterface.addSubCategory(
                uid, token, strEmployeeId, strCategoryId, strSubCategoryName,
                pic, strPriceCurrency, strSubcategoryPrice
            )
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iAddSubCategoryView.dismissProgress()

                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: SuccessResponse =
                        builder.fromJson(reader, SuccessResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>AddSubCategorySuccess")
                        Utils.showToast(context, response.getMessage()!!)
                        iAddSubCategoryView.onSubCategoryAddSuccess(
                            response,
                            "AddSubCategorySuccess"
                        )
                    } else {
                        Utils.showToast(context, response.getMessage()!!)
                    }

                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e.message)
                }
            }

            override fun onError(code: Int, error: String?) {
                iAddSubCategoryView.dismissProgress()
                Utils.printLog(TAG, "onError() called with: code = $code, error = $error")
                Utils.showToast(context, "Something got Wrong")
                iAddSubCategoryView.onFailure(error!!)
            }
        })
    }


    override fun callEditSubCategoryApi(

        id: String, utoken: String, subcategoryId: String, employeeId: String,
        categoryId: String, subcategoryName: String, subcategoryImage: String,
        priceCurrency: String, subcategoryPrice: String

    ) {
        var imgpath = subcategoryImage
        Utils.printLog(TAG, "callAddApi: " + imgpath)
        val file = File(imgpath)
        if (file.exists()) {
            val reqimg = RequestBody.create(
                context.contentResolver.getType(Utils.getImageContentUri(context, file)!!)!!
                    .toMediaTypeOrNull(), file
            )
            pic = MultipartBody.Part.createFormData("subcategoryImage", file.name, reqimg)

        }
        val uid: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, id)
        val token: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, utoken)
        val strSubCategoryId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, subcategoryId)
        val strEmployeeId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, employeeId)
        val strCategoryId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, categoryId)
        val strSubCategoryName: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, subcategoryName)
        val strPriceCurrency: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, priceCurrency)
        val strSubcategoryPrice: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, subcategoryPrice)

        iAddSubCategoryView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =
            apiInterface.editsubCategory(
                uid, token, strSubCategoryId, strEmployeeId, strCategoryId, strSubCategoryName,
                pic, strPriceCurrency, strSubcategoryPrice
            )
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iAddSubCategoryView.dismissProgress()

                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: SuccessResponse =
                        builder.fromJson(reader, SuccessResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>EditSubCategorySuccess")
                        Utils.showToast(context, response.getMessage()!!)
                        iAddSubCategoryView.onSubCategoryAddSuccess(
                            response,
                            "EditSubCategorySuccess"
                        )
                    } else {
                        Utils.showToast(context, response.getMessage()!!)
                    }

                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e.message)
                }
            }

            override fun onError(code: Int, error: String?) {
                iAddSubCategoryView.dismissProgress()
                Utils.printLog(TAG, "onError() called with: code = $code, error = $error")
                Utils.showToast(context, "Something got Wrong")
                iAddSubCategoryView.onFailure(error!!)
            }
        })
    }


}