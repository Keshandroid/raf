package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IAddBookingController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.AddBookingResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IAddBookingView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class AddBookingController(var context: Activity, var iAddBookingView: IAddBookingView) :
    IAddBookingController {

    val TAG="AddBookingController"

    override fun callAddBookingApi(userId: String, userToken: String,
                                   serviceId: String, bookingDate: String,
                                   bookingTime: String,total_service_time: String)
    {
        iAddBookingView.showProgress()

        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>?=apiInterface.addBooking(userId,userToken,serviceId,bookingDate,bookingTime,total_service_time)

        RetrofitHelper.callApi(call,object : RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iAddBookingView.dismissProgress()
                try {
                    var res=body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder= GsonBuilder().create()

                    var response: AddBookingResponse =builder.fromJson(reader,
                        AddBookingResponse::class.java)
                    if(response.getStatus().equals("1"))
                    {
                        Utils.printLog(TAG, "onSuccess:====>Booking_Success")
                        Utils.showToast(context,response.getMessage())
                        iAddBookingView.onIAddBookingSuccess(response,"Booking_Success")
                    }
                    else if (response.getStatus().equals("0"))
                    {
                        Utils.printLog(TAG, "onSuccess:===>Booking_failed")
                        Utils.showToast(context,response.getMessage())
                        iAddBookingView.onIAddBookingSuccess(response,"Booking_failed")

                    }
                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: test fail>>"+e.message)
                    iAddBookingView.onFailure(e.message?:"")
                }

            }

            override fun onError(code: Int, error: String?) {
                iAddBookingView.dismissProgress()
                Utils.showToast(context,"Something Got Wrong")
                iAddBookingView.onFailure(error!!)
            }
        })
    }
}