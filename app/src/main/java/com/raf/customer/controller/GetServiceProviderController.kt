package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetServiceProviderController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetServiceProviderResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetServiceProviderView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class GetServiceProviderController(var context:Activity,var iGetServiceProviderView: IGetServiceProviderView):IGetServiceProviderController {

    val TAG="GetServiceProviderController"

    override fun callGetServiceProviderApi(uid: String, utoken: String, serviceproviderid: String) {
        iGetServiceProviderView.showProgress()

        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>?=apiInterface.getServiceProvider(uid,utoken,serviceproviderid)

        RetrofitHelper.callApi(call,object : RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetServiceProviderView.dismissProgress()
                try {
                    var res=body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder= GsonBuilder().create()

                    var response: GetServiceProviderResponse =builder.fromJson(reader,
                        GetServiceProviderResponse::class.java)
                    if(response.getStatus().equals("1"))
                    {
                        Utils.showToast(context,response.getMessage())
                        iGetServiceProviderView.onIGetServiceSuccess(response,"Provider_Got")
                    }
                    else if (response.getStatus().equals("0"))
                    {
                        Utils.printLog(TAG, "onSuccess:===>Provider_got_failed")
                        Utils.showToast(context,response.getMessage())
                        iGetServiceProviderView.onIGetServiceSuccess(response,"Provider_Got_failed")

                    }
                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: test fail>>"+e.message)
                    iGetServiceProviderView.onFailure(e.message?:"")
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetServiceProviderView.dismissProgress()
                Utils.showToast(context,"Something Got Wrong")
                iGetServiceProviderView.onFailure(error!!)
            }
        })
    }
}