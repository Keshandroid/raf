package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetProfileController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetProfileResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetProfileView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class GetProfileController(var context: Activity, var iGetProfileView:IGetProfileView):IGetProfileController {
    val TAG ="GetProfileController"
    override fun calllGetProfileApi(uid: String, utkone: String) {
        iGetProfileView.showProgress()
        var apiInterface:UserService=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =apiInterface.getprofile(uid,utkone)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetProfileView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: GetProfileResponse = builder.fromJson(reader, GetProfileResponse::class.java)

                    if (response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>Profile_got_Success")
                        Utils.showToast(context, response.getMessage()!!)
                            iGetProfileView.onGetProfileSuccess(response, "Profile_got_Success")
                    }
                    else if(response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>Profile_got_UnSuccess")
                        Utils.showToast(context, response.getMessage()!!)
                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:"+e)
                    iGetProfileView.onFailure(e.message!!)
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetProfileView.dismissProgress()
                Utils.showToast(context,"Something got wrong")
                iGetProfileView.onFailure(error!!)
            }

        })
    }

}
