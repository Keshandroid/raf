package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IUpdateProfileController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.UpdateProfileResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IUpdateProfileView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class UpdateProfileController(var context: Activity, var iUpdateProfileView: IUpdateProfileView):IUpdateProfileController
{
    val TAG = "UpdateProfileController"
    var pic: MultipartBody.Part? = null
    override fun callUpdateProfileApi(id: String, utoken: String, firstname: String, lastname: String,
        email: String, phonno: String, gender: String, profile_pic: String) {
        var imgpath = profile_pic
        Utils.printLog(TAG, "callUpdateApi: "+imgpath)
        val file = File(imgpath)
        if (file.exists()) {
            val reqimg = RequestBody.create(
                context.contentResolver.getType(Utils.getImageContentUri(context, file)!!)!!
                    .toMediaTypeOrNull(), file
            )
            pic = MultipartBody.Part.createFormData("profilePhoto", file.name, reqimg)

        }
        val uid: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, id)
        val token: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, utoken)
        val fnm: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, firstname)
        val lnm: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, lastname)
        val mail: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, email)
        val cont_no: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, phonno)
        val g: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, gender)


            iUpdateProfileView.showProgress()
        var apiInterface:UserService=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =apiInterface.updateprofile(uid,token,fnm,lnm,mail,cont_no,g,pic)
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iUpdateProfileView.dismissProgress()

                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response:UpdateProfileResponse =
                        builder.fromJson(reader, UpdateProfileResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>Profile Update Success")
                        Utils.showToast(context, response.getMessage())
                        iUpdateProfileView.onUpdateProfileSucsess(response, "Profile_updates")
                    }

                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e.message)
                }
            }

            override fun onError(code: Int, error: String?) {
                iUpdateProfileView.dismissProgress()
                Utils.printLog(TAG, "onError() called with: code = $code, error = $error")
                Utils.showToast(context, "Something got Wrong")
                iUpdateProfileView.onFailure(error!!)
            }
        })
    }


}