package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IStaticPageController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.StaticPageResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IStaticPageView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class StaticPageController(var context: Activity, var iStaticPageView: IStaticPageView): IStaticPageController {
    val TAG="StaticPageController"

    override fun callStaticPageApi(static_id: String) {

        iStaticPageView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>?=apiInterface.getStaticPage(static_id)

        RetrofitHelper.callApi(call,object : RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iStaticPageView.dismissProgress()
                try {
                    var res=body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder= GsonBuilder().create()

                    var response: StaticPageResponse =builder.fromJson(reader,
                        StaticPageResponse::class.java)
                    if(response.getStatus()==1)
                    {
                        Utils.showToast(context,response.getMessage())
                        iStaticPageView.onIStaticPageSuccess(response,"Static_page_Got")
                    }
                    else if (response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:===>Static_page_got_failed")
                        Utils.showToast(context,response.getMessage())
                        iStaticPageView.onIStaticPageSuccess(response,"Static_page_got_failed")
                    }
                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: test fail>>"+e.message)
                    iStaticPageView.onFailure(e.message?:"")
                }

            }

            override fun onError(code: Int, error: String?) {
                iStaticPageView.dismissProgress()
                Utils.showToast(context,"Something Got Wrong")
                iStaticPageView.onFailure(error!!)
            }

        })

    }

}