package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.ILoginController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.LoginResponse
import com.raf.customer.retrofit.response.SocialLoginResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.ILoginView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class LoginController(var context: Activity, var iLoginView: ILoginView ):ILoginController {
    val TAG = "LoginController"
    override fun callLoginApi(email: String, password: String) {
        iLoginView.showProgress()

        var apiInterface=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.login(email, password)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
               iLoginView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: LoginResponse = builder.fromJson(reader, LoginResponse::class.java)

                    if (response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>Login_Success")
                        Utils.showToast(context,response.getMessage())
                        var appPreference = AppPreference(context)
                        appPreference.setBoolean(AppConstant.ISLOGIN,true)
                        appPreference.setString(AppConstant.UID,response.result!!.uid.toString())
                        appPreference.setString(AppConstant.UTOKEN, response.result!!.utoken)
                        appPreference.setString(AppConstant.FIRSNAME,response.result!!.firstname)
                        appPreference.setString(AppConstant.LASTNAME,  response.result!!.lastName)
                        appPreference.setString(AppConstant.EMAIL,  response.result!!.email)
                        appPreference.setString(AppConstant.PHON_NO,  response.result!!.contact_no)
                        appPreference.setString(AppConstant.GENDER,  response.result!!.gender)
                        appPreference.setString(AppConstant.PIC,   response.result!!.profile_pic)
                        appPreference.setString(AppConstant.USER_TYPE, AppConstant.CUSTOMER)

                        iLoginView.onLoginSuccess(response, "Login_Success")
                    }
                    else if(response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>Login_UnSuccess")
                        Utils.showToast(context,response.getMessage())
                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:"+e)
                    iLoginView.onFailure(e.message!!)
                }
            }
            override fun onError(code: Int, error: String?) {
            iLoginView.dismissProgress()
                Utils.showToast(context,"Something got wrong")
                iLoginView.onFailure(error!!)
            }

        })

    }

    override fun callSocialApi(
        firstName: String,
        lastName: String,
        email: String,
        registrationType: String,
        socialToken: String,
        socialId: String ) {
        iLoginView.showProgress()

        var apiInterface=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.socialLogin(firstName, lastName, email, registrationType, socialToken,socialId)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iLoginView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: SocialLoginResponse = builder.fromJson(reader, SocialLoginResponse::class.java)

                    if (response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>Social Login Success")
                        Utils.showToast(context,response.getMessage())
                        var appPreference = AppPreference(context)
                        appPreference.setBoolean(AppConstant.ISLOGIN,true)
                        appPreference.setBoolean(AppConstant.IS_SOCIAL_LOGIN,true)
                        appPreference.setString(AppConstant.UID,response.result!!.uid.toString())
                        appPreference.setString(AppConstant.UTOKEN, response.result!!.utoken)
                        appPreference.setString(AppConstant.FIRSNAME,response.result!!.firstname)
                        appPreference.setString(AppConstant.LASTNAME,  response.result!!.lastName)
                        appPreference.setString(AppConstant.EMAIL,  response.result!!.email)
//                        appPreference.setString(AppConstant.PHON_NO,  response.result!!.contact_no)
//                        appPreference.setString(AppConstant.GENDER,  response.result!!.gender)
//                        appPreference.setString(AppConstant.PIC,   response.result!!.profile_pic)
                        appPreference.setString(AppConstant.USER_TYPE, AppConstant.CUSTOMER)

                        iLoginView.onSocialLoginSuccess(response, "Login_Success")
                    }
                    else if(response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>Login_UnSuccess")
                        Utils.showToast(context,response.getMessage())
                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:"+e)
                    iLoginView.onFailure(e.message!!)
                }
            }
            override fun onError(code: Int, error: String?) {
                iLoginView.dismissProgress()
                Utils.showToast(context,"Something got wrong")
                iLoginView.onFailure(error!!)
            }

        })
    }

}