package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IRatingController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.RatingResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IRatingView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class RatingController(var context:Activity,var iRatingView: IRatingView):IRatingController {
    val TAG="RatingController"
    override fun callRatingApi(uid: String, utoken: String, serviceproviderid: String,
        rating: Float, review: String)
    {
        iRatingView.showProgress()

        var apiInterface:UserService=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call:Call<ResponseBody?>?=apiInterface.addRating(uid,utoken,serviceproviderid,rating,review)

        RetrofitHelper.callApi(call,object :RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iRatingView.dismissProgress()
                try
                {
                    var res=body?.body()?.string()
                    var reader:Reader=StringReader(res)

                    var builder=GsonBuilder().create()
                    var response:RatingResponse=builder.fromJson(reader,RatingResponse::class.java)

                    if(response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:===>Add review Success")
                        Utils.showToast(context,response.getMessage())
                        iRatingView.onRatingSuccess(response,"Add_review_success")
                    }

                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: "+e.message)
                    iRatingView.onFailure(e.message!!)
                }
            }

            override fun onError(code: Int, error: String?) {
                iRatingView.dismissProgress()
                Utils.showToast(context,"Something Got Wrong")
                iRatingView.onFailure(error!!)
            }
        })
    }
}