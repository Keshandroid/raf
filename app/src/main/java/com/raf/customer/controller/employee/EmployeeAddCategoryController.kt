package com.raf.customer.controller.employee

import android.app.Activity
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IEmployeeAddCategoryController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.ForgotPasswordResponse
import com.raf.customer.retrofit.response.employee.SuccessResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeAddCategoryView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class EmployeeAddCategoryController(
    var context: Activity,
    var iAddCategoryView: IEmployeeAddCategoryView
) :
    IEmployeeAddCategoryController {
    val TAG = "EmployeeAddCategoryController"
    var pic: MultipartBody.Part? = null
    var editPic: MultipartBody.Part? = null
    override fun callAddCategoryApi(
        id: String, utoken: String, employeeId: String,
        categoryName: String, categoryImage: String
    ) {
        var imgpath = categoryImage
        Utils.printLog(TAG, "callAddApi: " + imgpath)
        val file = File(imgpath)
        if (file.exists()) {
            val reqimg = RequestBody.create(
                context.contentResolver.getType(Utils.getImageContentUri(context, file)!!)!!
                    .toMediaTypeOrNull(), file
            )
            pic = MultipartBody.Part.createFormData("categoryImage", file.name, reqimg)

        }
        val uid: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, id)
        val token: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, utoken)
        val strEmployeeId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, employeeId)
        val strCategoryName: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, categoryName)

        iAddCategoryView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =
            apiInterface.addCategory(uid, token, strEmployeeId, strCategoryName, pic)
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iAddCategoryView.dismissProgress()

                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: ForgotPasswordResponse =
                        builder.fromJson(reader, ForgotPasswordResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>AddCategorySuccess")
                        Utils.showToast(context, response.getMessage()!!)
                        iAddCategoryView.onCategoryAddSuccess(response, "AddCategorySuccess")
                    } else {
                        Utils.showToast(context, response.getMessage()!!)
                    }

                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e.message)
                }
            }

            override fun onError(code: Int, error: String?) {
                iAddCategoryView.dismissProgress()
                Utils.printLog(TAG, "onError() called with: code = $code, error = $error")
                Utils.showToast(context, "Something got Wrong")
                iAddCategoryView.onFailure(error!!)
            }
        })
    }

    override fun callEditCategoryApi(
        id: String, utoken: String, category_id: String, employeeId: String,
        categoryName: String, categoryImage: String
    ) {
        var imgpath = categoryImage
        Utils.printLog(TAG, "callAddApi: " + imgpath)
        val file = File(imgpath)
        if (file.exists()) {
            val reqimg = RequestBody.create(
                context.contentResolver.getType(Utils.getImageContentUri(context, file)!!)!!
                    .toMediaTypeOrNull(), file
            )
            editPic = MultipartBody.Part.createFormData("categoryImage", file.name, reqimg)

        }
        val uid: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, id)
        val token: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, utoken)
        val categoryId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, category_id)
        val serviceProviderId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, employeeId)
        val strCategoryName: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, categoryName)

        iAddCategoryView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =
            apiInterface.editCategory(
                uid,
                token,
                categoryId,
                serviceProviderId,
                strCategoryName,
                editPic
            )
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iAddCategoryView.dismissProgress()

                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: SuccessResponse =
                        builder.fromJson(reader, SuccessResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>EditCategorySuccess")
                        iAddCategoryView.onCategoryEditSuccess(response, "EditCategorySuccess")
                    } else {
                        Utils.showToast(context, response.getMessage()!!)
                    }

                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e.message)
                }
            }

            override fun onError(code: Int, error: String?) {
                iAddCategoryView.dismissProgress()
                Utils.printLog(TAG, "onError() called with: code = $code, error = $error")
                Utils.showToast(context, "Something got Wrong")
                iAddCategoryView.onFailure(error!!)
            }
        })
    }


}