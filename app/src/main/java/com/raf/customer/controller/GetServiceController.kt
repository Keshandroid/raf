package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetServiceController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetServiceResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetServiceView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader


class GetServiceController(var context:Activity,var iGetServiceView: IGetServiceView):IGetServiceController{
    val TAG="GetServiceController"
    override fun callGetServiceApi(uid: String,utoken: String,serviceproviderid: String,cat_id:Int,subcat_id:String)
    {
        iGetServiceView.showProgress()
        var apiInterface=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>?=apiInterface.getService(uid,utoken,serviceproviderid,
            cat_id.toString(), subcat_id.toString())

        RetrofitHelper.callApi(call,object :RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
            iGetServiceView.dismissProgress()
                try
                {
                    var res=body!!.body()!!.string()
                    var reader:Reader=StringReader(res)
                    var builder=GsonBuilder().create()
                    var response:GetServiceResponse=builder.fromJson(reader,GetServiceResponse::class.java)
                    if(response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:=====service_got")
//                        Utils.showToast(context,response.getMessage())
                        iGetServiceView.onGetServiceSuccess(response,"sevice_got")
                    }
                    else if(response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:=====service_got_failed")
                        Utils.showToast(context,response.getMessage())
                    }
                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: "+e.message)
                    iGetServiceView.onFailure(e.message!!)
                }
            }

            override fun onError(code: Int, error: String?) {
                iGetServiceView.dismissProgress()
               Utils.showToast(context,"Something got wrong")
                iGetServiceView.onFailure(error!!)
            }
        })
    }
}