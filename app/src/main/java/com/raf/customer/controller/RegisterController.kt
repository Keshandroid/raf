package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IRegisterController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.RegisterResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IRegisterView
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class RegisterController(var context: Activity, var iRegisterView: IRegisterView) :
    IRegisterController {
    val TAG = "RegisterController"
    var pic: MultipartBody.Part? = null

    override
    fun callRegisterApi(
        firstname: String, lastname: String, password: String, email: String,
        phonno: String, gender: String, profile_pic: String
    ) {
        var imgpath = profile_pic
        val file = File(imgpath)
        if (file.exists()) {
            val reqimg = RequestBody.create(
                context.contentResolver.getType(Utils.getImageContentUri(context, file)!!)!!
                    .toMediaTypeOrNull(), file
            )
            pic = MultipartBody.Part.createFormData("profilePhoto", file.name, reqimg)
            Utils.printLog(TAG, "callRegisterApi:" + pic)
        }
        val fnm: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, firstname)
        val lnm: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, lastname)
        val pass: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, password)
        val mail: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, email)
        val cont_no: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, phonno)
        val g: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, gender)


        iRegisterView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =
            apiInterface.register(fnm, lnm, pass, mail, cont_no, g, pic)
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iRegisterView.dismissProgress()

                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: RegisterResponse =
                        builder.fromJson(reader, RegisterResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>Register Success")
                        Utils.showToast(context, response.getMessage())

//                        var appPreference = AppPreference(context)
//                        appPreference.setBoolean(AppConstant.ISLOGIN, true)
//                        appPreference.setString(AppConstant.UID, response.result!!.uid.toString())
//                        appPreference.setString(AppConstant.UTOKEN, ""+response.result!!.utoken)
//                        appPreference.setString(AppConstant.FIRSNAME, response.result!!.firstname)
//                        appPreference.setString(AppConstant.LASTNAME, response.result!!.lastName)
//                        appPreference.setString(AppConstant.EMAIL, response.result!!.email)
//                        appPreference.setString(AppConstant.PHON_NO, response.result!!.contact_no)
//                        appPreference.setString(AppConstant.GENDER, response.result!!.gender)
//                        appPreference.setString(AppConstant.PIC, response.result!!.profile_pic)
//                        appPreference.setString(AppConstant.USER_TYPE, AppConstant.CUSTOMER)

                        iRegisterView.onRegisterSuccess(response, "Register_Success")
                    } else {
                        Utils.showToast(context, response.getMessage())
                    }

                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e.message)
                }
            }

            override fun onError(code: Int, error: String?) {
                iRegisterView.dismissProgress()
                Utils.printLog(TAG, "onError() called with: code = $code, error = $error")
                Utils.showToast(context, "Something got Wrong")
                iRegisterView.onFailure(error!!)
            }
        })
    }


}