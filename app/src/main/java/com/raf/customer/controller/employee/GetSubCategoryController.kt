package com.raf.customer.controller.employee

import android.app.Activity
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetEmpSubCategoryController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.retrofit.response.employee.GetEmpSubCategoryResponse
import com.raf.customer.utils.Lists
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetEmpSubCategoryView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class GetSubCategoryEmpController(var context:Activity, var iGetEmpSubCategoryView: IGetEmpSubCategoryView):
    IGetEmpSubCategoryController {
    val TAG="GetSubCategoryEmpController"

    override fun callGetSubCategoryEmpController(uid: String, utoken: String, serviceproviderid: String,catId: String) {
        iGetEmpSubCategoryView.showProgress()

        var apiInterface:UserService=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call:Call<ResponseBody?>?=apiInterface.getSubCategory(uid,utoken,serviceproviderid,catId)

        RetrofitHelper.callApi(call,object :RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetEmpSubCategoryView.dismissProgress()
                try {
                    var res=body!!.body()!!.string()
                    var reader:Reader=StringReader(res)

                    var builder=GsonBuilder().create()

                    var response:GetEmpSubCategoryResponse=builder.fromJson(reader,GetEmpSubCategoryResponse::class.java)
                    if(response.getStatus()==1)
                    {
                        Utils.showToast(context,response.getMessage())
                        iGetEmpSubCategoryView.onGetEmpCategorySuccess(response,"Sub_Category_Got")
                        Lists.setEmpSubCategoryList(response.getResult())
                    }
                    else if (response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:===>Category_got_failed")
                        Utils.showToast(context,response.getMessage())
                        iGetEmpSubCategoryView.onGetEmpCategorySuccess(response,"Sub_Category_Got_failed")

                    }
                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: test fail>>"+e.message)
                    iGetEmpSubCategoryView.onFailure(e.message?:"")
                }

            }

            override fun onError(code: Int, error: String?) {
            iGetEmpSubCategoryView.dismissProgress()
                Utils.showToast(context,"Something Got Wrong")
                iGetEmpSubCategoryView.onFailure(error!!)
            }
        })
    }

}