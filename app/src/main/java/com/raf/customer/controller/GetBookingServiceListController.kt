package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetBookingServiceListController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetBookingServiceListResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetBookingServiceListView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class GetBookingServiceListController(var context: Activity, var iGetBookingServiceListView: IGetBookingServiceListView):
    IGetBookingServiceListController {
    var TAG = "GetBookingServiceListController";
    override fun callGetBookingServiceListApi(uid: String, utoken: String, serviceId: String) {
        iGetBookingServiceListView.showProgress()

        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.getBookingServiceList(uid, utoken, serviceId)
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetBookingServiceListView.dismissProgress()
                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)
                    var builder = GsonBuilder().create()

                    var response: GetBookingServiceListResponse = builder.fromJson(
                        reader,
                        GetBookingServiceListResponse::class.java
                    )
                    if (response.getStatus().equals("1")) {
                        Utils.showToast(context, response.getMessage())
                        iGetBookingServiceListView.onIGetBookingServiceListSuccess(response, "Provider_Got")

                    } else if (response.getStatus().equals("0")) {
                        Utils.printLog(TAG, "onSuccess:===>Provider_got_failed")
                        Utils.showToast(context, response.getMessage())
                        iGetBookingServiceListView.onIGetBookingServiceListSuccess(
                            response,
                            "Provider_Got_failed"
                        )

                    }
                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess: test fail>>" + e.message)
                    iGetBookingServiceListView.onFailure(e.message ?: "")
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetBookingServiceListView.dismissProgress()
                Utils.showToast(context, "Something Got Wrong")
                iGetBookingServiceListView.onFailure(error!!)
            }
        })
    }

}