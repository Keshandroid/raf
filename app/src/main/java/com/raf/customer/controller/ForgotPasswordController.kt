package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IforgotPasswordController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.ForgotPasswordResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IForgotPassWordView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class ForgotPasswordController(var context: Activity, var iForgotPassWordView: IForgotPassWordView):IforgotPasswordController {
    val TAG = "ForgotPassdController"
    override fun callForgotPasswordApi(email: String) {
        iForgotPassWordView.showProgress()
        var apiInterface:UserService=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.forgotpassword(email)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iForgotPassWordView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response:ForgotPasswordResponse = builder.fromJson(reader, ForgotPasswordResponse::class.java)

                    if (response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>PasswordForgot_Success")
                        Utils.showToast(context, response.getMessage()!!)
                        iForgotPassWordView.onForgotPasswordSuccess(response, "Forgot_Success")
                    }
                    else if(response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>PasswordForgot_UnSuccess")
                        Utils.showToast(context, response.getMessage()!!)
                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:"+e)
                    iForgotPassWordView.onFailure(e.message!!)
                }

            }

            override fun onError(code: Int, error: String?) {
                Utils.dismissProgress()
                Utils.showToast(context,"Something got wrong")
                iForgotPassWordView.onFailure(error!!)
            }

        })
    }

}