package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IforgotPasswordController
import com.raf.customer.controller.listener.IregisterDeviceController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.ForgotPasswordResponse
import com.raf.customer.retrofit.response.RegisterDeviceResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IForgotPassWordView
import com.raf.customer.view.viewInterface.IRegisterDeviceView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class RegisterDeviceController(var context: Activity, var iRegisterDeviceView: IRegisterDeviceView):
    IregisterDeviceController {
    val TAG = "RegisterDeviceCtrl"
    override fun callRegisterDeviceApi(token: String,userId: String, email: String, deviceId: String) {
        //iRegisterDeviceView.showProgress()
        var apiInterface:UserService=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.registerDevice(token,userId,email,deviceId)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                //iRegisterDeviceView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response:RegisterDeviceResponse = builder.fromJson(reader, RegisterDeviceResponse::class.java)

                    if (response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>register device success")
                        Utils.showToast(context, response.getMessage()!!)
                        iRegisterDeviceView.onRegisterDeviceSuccess(response, "register_device")
                    }
                    else if(response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onFailed:=====>Register device failed")
                        Utils.showToast(context, response.getMessage()!!)
                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:"+e)
                    iRegisterDeviceView.onFailure(e.message!!)
                }

            }

            override fun onError(code: Int, error: String?) {
                //iRegisterDeviceView.dismissProgress()
                Utils.showToast(context,"Something got wrong")
                iRegisterDeviceView.onFailure(error!!)
            }

        })
    }

}