package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetBookingDetailController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetBookingDetailResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetBookingDetailView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class GetBookingDetailController(var context: Activity, var iGetBookingDerailView: IGetBookingDetailView):
    IGetBookingDetailController {
    var TAG = "GetBookingDetailController";
    override fun callGetBookingDetailApi(uid: String, utoken: String, bookingId: String) {
        iGetBookingDerailView.showProgress()

        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.getBookingDetail(uid, utoken, bookingId)
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetBookingDerailView.dismissProgress()
                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)
                    var builder = GsonBuilder().create()

                    var response: GetBookingDetailResponse = builder.fromJson(
                        reader,
                        GetBookingDetailResponse::class.java
                    )
                    if (response.getStatus().equals("1")) {
                        Utils.showToast(context, response.getMessage())
                        iGetBookingDerailView.onIGetBookingDetailSuccess(response, "Provider_Got")

                    } else if (response.getStatus().equals("0")) {
                        Utils.printLog(TAG, "onSuccess:===>Provider_got_failed")
                        Utils.showToast(context, response.getMessage())
                        iGetBookingDerailView.onIGetBookingDetailSuccess(
                            response,
                            "Provider_Got_failed"
                        )

                    }
                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess: test fail>>" + e.message)
                    iGetBookingDerailView.onFailure(e.message ?: "")
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetBookingDerailView.dismissProgress()
                Utils.showToast(context, "Something Got Wrong")
                iGetBookingDerailView.onFailure(error!!)
            }
        })
    }

}