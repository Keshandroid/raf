package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetCategoryController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.utils.Lists
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetCategoryView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class GetCategoryController(var context:Activity,var iGetCategoryView: IGetCategoryView):IGetCategoryController{
    val TAG="GetCategoryController"
    override fun callGetcategotyApi(uid: String, utoken: String, serviceproviderid: String) {
        iGetCategoryView.showProgress()

        var apiInterface:UserService=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call:Call<ResponseBody?>?=apiInterface.getcategory(uid,utoken,serviceproviderid)

        RetrofitHelper.callApi(call,object :RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetCategoryView.dismissProgress()
                try {
                    var res=body!!.body()!!.string()
                    var reader:Reader=StringReader(res)

                    var builder=GsonBuilder().create()

                    var response:GetCategoryResponse=builder.fromJson(reader,GetCategoryResponse::class.java)
                    if(response.getStatus()==1)
                    {
                        Utils.showToast(context,response.getMessage())
                        Lists.setCategoryList(response.getResult())
                        iGetCategoryView.onGetCategorySuccess(response,"Category_Got")
                        Utils.printLog(TAG, "onSuccess: "+response.getResult().get(0)!!.categoryName)
                    }
                    else if (response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:===>Category_got_failed")
                        Utils.showToast(context,response.getMessage())
                        iGetCategoryView.onGetCategorySuccess(response,"Category_Got_failed")

                    }
                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: test fail>>"+e.message)
                    iGetCategoryView.onFailure(e.message?:"")
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetCategoryView.dismissProgress()
                Utils.showToast(context,"Something Got Wrong")
                iGetCategoryView.onFailure(error!!)
            }
        })
    }

    override fun callDeleteCategotyApi(uid: String, utoken: String, serviceproviderid: String) {
        iGetCategoryView.showProgress()

        var apiInterface:UserService=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call:Call<ResponseBody?>?=apiInterface.deleteCategory(uid,utoken,serviceproviderid)

        RetrofitHelper.callApi(call,object :RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetCategoryView.dismissProgress()
                try {
                    var res=body!!.body()!!.string()
                    var reader:Reader=StringReader(res)

                    var builder=GsonBuilder().create()

                    var response:GetCategoryResponse=builder.fromJson(reader,GetCategoryResponse::class.java)
                    if(response.getStatus()==1)
                    {
                        Utils.showToast(context,response.getMessage())
                        iGetCategoryView.onGetCategorySuccess(response,"Category_Got")
                        Utils.printLog(TAG, "onSuccess: "+response.getResult().get(0)!!.categoryName)
                        Lists.setCategoryList(response.getResult())
                    }
                    else if (response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:===>Category_got_failed")
                        Utils.showToast(context,response.getMessage())
                        iGetCategoryView.onGetCategorySuccess(response,"Category_Got_failed")

                    }
                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: test fail>>"+e.message)
                    iGetCategoryView.onFailure(e.message?:"")
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetCategoryView.dismissProgress()
                Utils.showToast(context,"Something Got Wrong")
                iGetCategoryView.onFailure(error!!)
            }
        })
    }
}