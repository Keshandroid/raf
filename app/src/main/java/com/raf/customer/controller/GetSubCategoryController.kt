package com.raf.customer.controller

import android.app.Activity
import android.content.Context
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IGetSubCategoryController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetSubCategoryResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetSubCategoryView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader

class GetSubCategoryController(var context: Context, var iGetSubCategoryView: IGetSubCategoryView):
    IGetSubCategoryController {
    val TAG="GetSubCategoryController"
    override fun callGetSubcategoryApi(uid: String, utoken: String, serviceproviderid: String, cat_id: String) {
        iGetSubCategoryView.showProgress()

        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>?=apiInterface.getSubCategory(uid,utoken,serviceproviderid,cat_id)

        RetrofitHelper.callApi(call,object : RetrofitHelper.ConnectionCallBack{
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetSubCategoryView.dismissProgress()
                try {
                    var res=body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder= GsonBuilder().create()

                    var response: GetSubCategoryResponse =builder.fromJson(reader,
                        GetSubCategoryResponse::class.java)
                    if(response.getStatus()==1)
                    {
//                        Utils.showToast(context,response.getMessage())
                        iGetSubCategoryView.onGetSubCategorySuccess(response,"SubCategory_Got")
                        Utils.printLog(TAG, "onSuccess: "+response.getSubCategoryList().get(0)!!.subCategoryName)
                    }
                    else if (response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:===>Category_got_failed")
                        Utils.showToast(context,response.getMessage())
                        iGetSubCategoryView.onGetSubCategorySuccess(response,"SubCategory_Got_failed")

                    }
                }
                catch (e:Exception)
                {
                    Utils.printLog(TAG, "onSuccess: test fail>>"+e.message)
                    iGetSubCategoryView.onFailure(e.message?:"")
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetSubCategoryView.dismissProgress()
                Utils.showToast(context,"Something Got Wrong")
                iGetSubCategoryView.onFailure(error!!)
            }
        })
    }

    override fun callDeleteSubCategotyApi(
        uid: String,
        utoken: String,
        subcategoryId: String
    ) {
        iGetSubCategoryView.showProgress()

        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =
            apiInterface.deleteSubCategory(uid, utoken, subcategoryId)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iGetSubCategoryView.dismissProgress()
                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()

                    var response: GetSubCategoryResponse = builder.fromJson(
                        reader,
                        GetSubCategoryResponse::class.java
                    )
                    if (response.getStatus() == 1) {
                        Utils.showToast(context, response.getMessage())
                        iGetSubCategoryView.onGetSubCategorySuccess(response, "SubCategory_Got")

                    } else if (response.getStatus() == 0) {
                        Utils.printLog(TAG, "onSuccess:===>Category_got_failed")
                        Utils.showToast(context, response.getMessage())
                        iGetSubCategoryView.onGetSubCategorySuccess(
                            response,
                            "SubCategory_Got_failed"
                        )

                    }
                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess: test fail>>" + e.message)
                    iGetSubCategoryView.onFailure(e.message ?: "")
                }

            }

            override fun onError(code: Int, error: String?) {
                iGetSubCategoryView.dismissProgress()
                Utils.showToast(context, "Something Got Wrong")
                iGetSubCategoryView.onFailure(error!!)
            }
        })
    }


}