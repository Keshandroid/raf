package com.raf.customer.controller.employee

import android.app.Activity
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IEmployeeOrderController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.GetProfileResponse
import com.raf.customer.retrofit.response.employee.EmployeeOrderResponse
import com.raf.customer.retrofit.response.employee.SuccessResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeOrderView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class EmployeeOrderController(
    var context: Activity,
    var iEmployeeOrderView: IEmployeeOrderView
) :
    IEmployeeOrderController {
    val TAG = "EmployeeOrderController"
    override fun callGetCurrentOrderApi(
        uid: String,
        utoken: String
    ) {
        iEmployeeOrderView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.getEmpCurrentOrder(uid, utoken)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iEmployeeOrderView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: EmployeeOrderResponse =
                        builder.fromJson(reader, EmployeeOrderResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>Employee_Current_Order_got_Success")
                        //Utils.showToast(context, response.getMessage()!!)
                        iEmployeeOrderView.onEmployeeCurrentOrderSuccess(response, "Employee_Current_Order_got_Success")
                    } else if (response.getStatus() == 0) {
                        Utils.printLog(TAG, "onSuccess:=====>Employee_Current_Order_got_UnSuccess")
                        iEmployeeOrderView.onFailure( "Employee_Current_Order_got_Success")
//                        Utils.showToast(context, response.getMessage()!!)
                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:" + e)
                    iEmployeeOrderView.onFailure(e.message!!)
                }

            }

            override fun onError(code: Int, error: String?) {
                iEmployeeOrderView.dismissProgress()
                Utils.printLog(TAG+"error", "error:" + error)
//                Utils.showToast(context, "Something got wrong")
                iEmployeeOrderView.onFailure(error!!)
            }

        })
    }

    override fun callGetCompleteOrderApi(
        uid: String,
        utoken: String,
        bookingStatus: String
    ) {
        iEmployeeOrderView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.getEmpCompleteOrder(uid, utoken,bookingStatus)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iEmployeeOrderView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: EmployeeOrderResponse =
                        builder.fromJson(reader, EmployeeOrderResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>Employee_Complete_Order_got_Success")
                        //Utils.showToast(context, response.getMessage()!!)
                        iEmployeeOrderView.onEmployeeCompleteOrderSuccess(response, "Employee_Complete_Order_got_Success")
                    } else if (response.getStatus() == 0) {
                        Utils.printLog(TAG, "onSuccess:=====>Employee_Complete_Order_got_UnSuccess")
//                        Utils.showToast(context, response.getMessage()!!)
                        iEmployeeOrderView.onFailure( "Employee_Complete_Order_got_UnSuccess")

                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:" + e)
                    iEmployeeOrderView.onFailure(e.message!!)
                }

            }

            override fun onError(code: Int, error: String?) {
                iEmployeeOrderView.dismissProgress()
//                Utils.showToast(context, "Something got wrong")
                iEmployeeOrderView.onFailure(error!!)
            }

        })
    }


    override fun callOrderStatusApi(
        uid: String,
        utoken: String,
        bookingId: Int,
        bookingStatus: String
    ) {
        iEmployeeOrderView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.callOrderStatusApi(uid, utoken,bookingId,bookingStatus)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iEmployeeOrderView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: SuccessResponse =
                        builder.fromJson(reader, SuccessResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>Employee_Order_Status_Success")
                        iEmployeeOrderView.onEmployeeOrderStatusSuccess(response, "Employee_Order_Status_Success")
                    } else if (response.getStatus() == 0) {
                        Utils.printLog(TAG, "onSuccess:=====>Employee_Order_Status_UnSuccess")
                        iEmployeeOrderView.onFailure( "Employee_Order_Status_UnSuccess")
                    }
                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e)
                    iEmployeeOrderView.onFailure(e.message!!)
                }

            }

            override fun onError(code: Int, error: String?) {
                iEmployeeOrderView.dismissProgress()
                iEmployeeOrderView.onFailure(error!!)
            }

        })
    }


}