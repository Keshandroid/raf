package com.raf.customer.controller.employee

import android.app.Activity
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IEmployeeAddServiceController
import com.raf.customer.controller.listener.IEmployeeAddSubCategoryController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.employee.SuccessResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IEmployeeAddServiceView
import com.ramo.utils.ImageFilePath
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.MultipartBody
import okhttp3.RequestBody
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.File
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class EmployeeAddServiceController(
    var context: Activity,
    var iAddServiceView: IEmployeeAddServiceView
) :
    IEmployeeAddServiceController {
    val TAG = "EmployeeAddServiceController"
    var pic: MultipartBody.Part? = null
    override fun callAddServiceApi(
        id: String, utoken: String, employeeId: String,
        categoryId: String, subcategoryId: String, serviceName: String,
        serviceImage: String, description: String, avrageTime: String, image_counter: String

    ) {

        var imgpath = serviceImage
        Utils.printLog(TAG, "callAddApi: " + imgpath)
        val file = File(imgpath)
        if (file.exists()) {
            val reqimg = RequestBody.create(
                context.contentResolver.getType(Utils.getImageContentUri(context, file)!!)!!
                    .toMediaTypeOrNull(), file
            )
            pic = MultipartBody.Part.createFormData("serviceImage", file.name, reqimg)

        }
        val uid: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, id)
        val token: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, utoken)
        val strEmployeeId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, employeeId)
        val strCategoryId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, categoryId)
        val strSubCategoryId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, subcategoryId)
        val strServiceName: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, serviceName)
        val strDescription: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, description)
        val strAvrageTime: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, avrageTime)
        val strImageCounter: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, image_counter)

        iAddServiceView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =
            apiInterface.addService(
                uid, token, strEmployeeId, strCategoryId, strSubCategoryId, strServiceName,
                pic, strDescription, strAvrageTime, strImageCounter
            )
        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iAddServiceView.dismissProgress()

                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: SuccessResponse =
                        builder.fromJson(reader, SuccessResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>AddServiceSuccess")
                        Utils.showToast(context, response.getMessage()!!)
                        iAddServiceView.onAddServiceSuccess(
                            response,
                            "AddServiceSuccess"
                        )
                    } else {
                        Utils.showToast(context, response.getMessage()!!)
                    }

                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e.message)
                }
            }

            override fun onError(code: Int, error: String?) {
                iAddServiceView.dismissProgress()
                Utils.printLog(TAG, "onError() called with: code = $code, error = $error")
                Utils.showToast(context, "Something got Wrong")
                iAddServiceView.onFailure(error!!)
            }
        })
    }

    override fun callEditServiceApi(
        id: String, utoken: String, serviceId: String, employeeId: String,
        categoryId: String, subcategoryId: String, serviceName: String,
        serviceImage: String, description: String, avrageTime: String, image_counter: String

    ) {

        var imgpath = serviceImage
        Utils.printLog(TAG, "callAddApi: " + imgpath)
        val file = File(imgpath)
        if (file.exists()) {
            val reqimg = RequestBody.create(
                context.contentResolver.getType(Utils.getImageContentUri(context, file)!!)!!
                    .toMediaTypeOrNull(), file
            )
            pic = MultipartBody.Part.createFormData("serviceImage", file.name, reqimg)

        }
        val uid: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, id)
        val token: RequestBody = RequestBody.create("text/plain".toMediaTypeOrNull()!!, utoken)
        val strServiceId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, serviceId)
        val strEmployeeId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, employeeId)
        val strCategoryId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, categoryId)
        val strSubCategoryId: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, subcategoryId)
        val strServiceName: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, serviceName)
        val strDescription: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, description)
        val strAvrageTime: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, avrageTime)
        val strImageCounter: RequestBody =
            RequestBody.create("text/plain".toMediaTypeOrNull()!!, image_counter)

        iAddServiceView.showProgress()
        var apiInterface: UserService = RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? =
            apiInterface.editService(
                uid,
                token,
                strServiceId,
                strEmployeeId,
                strCategoryId,
                strSubCategoryId,
                strServiceName,
                pic,
                strDescription,
                strAvrageTime,
                strImageCounter
            )

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iAddServiceView.dismissProgress()

                try {
                    var res = body!!.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response: SuccessResponse =
                        builder.fromJson(reader, SuccessResponse::class.java)

                    if (response.getStatus() == 1) {
                        Utils.printLog(TAG, "onSuccess:=====>EditServiceSuccess")
                        Utils.showToast(context, response.getMessage()!!)
                        iAddServiceView.onAddServiceSuccess(
                            response,
                            "EditServiceSuccess"
                        )
                    } else {
                        Utils.showToast(context, response.getMessage()!!)
                    }

                } catch (e: Exception) {
                    Utils.printLog(TAG, "onSuccess:" + e.message)
                }
            }

            override fun onError(code: Int, error: String?) {
                iAddServiceView.dismissProgress()
                Utils.printLog(TAG, "onError() called with: code = $code, error = $error")
                Utils.showToast(context, "Something got Wrong")
                iAddServiceView.onFailure(error!!)
            }
        })
    }

}