package com.raf.customer.controller

import android.app.Activity
import android.util.Log
import com.google.gson.GsonBuilder
import com.julie.retrofit.RetrofitHelper
import com.julie.retrofit.UserService
import com.raf.customer.controller.listener.IforgotPasswordController
import com.raf.customer.controller.listener.IverifyEmailController
import com.raf.customer.retrofit.RetrofitClient
import com.raf.customer.retrofit.response.ForgotPasswordResponse
import com.raf.customer.retrofit.response.VerifyEmailResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IForgotPassWordView
import com.raf.customer.view.viewInterface.IVerifyEmailView
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Response
import java.io.Reader
import java.io.StringReader
import java.lang.Exception

class VerifyEmailController(var context: Activity, var iVerifyEmailView: IVerifyEmailView):
    IverifyEmailController {
    val TAG = "VerifyEmailCtrl"
    override fun callVerifyEmailApi(email: String,otp: String) {
        iVerifyEmailView.showProgress()
        var apiInterface:UserService=RetrofitClient.getClient()!!.create(UserService::class.java)
        var call: Call<ResponseBody?>? = apiInterface.verifyEmailOTP(email,otp)

        RetrofitHelper.callApi(call, object : RetrofitHelper.ConnectionCallBack {
            override fun onSuccess(body: Response<ResponseBody?>?) {
                iVerifyEmailView.dismissProgress()
                try {
                    var res = body?.body()!!.string()
                    var reader: Reader = StringReader(res)

                    var builder = GsonBuilder().create()
                    var response:VerifyEmailResponse = builder.fromJson(reader, VerifyEmailResponse::class.java)

                    if (response.getStatus()==1)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>PasswordForgot_Success")
                        Utils.showToast(context, response.getMessage()!!)
                        iVerifyEmailView.onVerifyEmailSuccess(response, "verify_email_success")
                    }
                    else if(response.getStatus()==0)
                    {
                        Utils.printLog(TAG, "onSuccess:=====>verify email failed")
                        Utils.showToast(context, response.getMessage()!!)
                    }


                } catch (e: Exception) {

                    Utils.printLog(TAG, "onSuccess:"+e)
                    iVerifyEmailView.onFailure(e.message!!)
                }

            }

            override fun onError(code: Int, error: String?) {
                iVerifyEmailView.dismissProgress()
                Utils.showToast(context,"Something got wrong")
                iVerifyEmailView.onFailure(error!!)
            }

        })
    }

}