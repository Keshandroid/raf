package com.raf.customer.application

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import androidx.lifecycle.LifecycleObserver
import com.google.firebase.FirebaseApp

import com.raf.R


class MyApplication : Application(), Application.ActivityLifecycleCallbacks,
    LifecycleObserver{

    val TAG = MyApplication::class.java.getSimpleName()
    lateinit var MY_APPLICATION: MyApplication
    internal var isautho = false
    private var mInstance: MyApplication? = null
    private var mContext: Context? = null
    private var appName: String? = null


    init {
        instance = this
    }

    companion object {
        private var instance: MyApplication? = null

        var onAppForegrounded = false


        fun applicationContext() : Context {
            return instance!!.applicationContext
        }

        fun onAppForground() : Boolean {
            return onAppForegrounded
        }

    }

    @Synchronized
    fun getInstance(): MyApplication? {
        return mInstance
    }



    fun getContext(): Context {
        return getInstance()!!.getApplicationContext()
    }


    fun getAppContext(): Context? {
        return mContext
    }

    override fun onCreate() {
        super.onCreate()

        mInstance = this
        appName = resources.getString(R.string.app_name)

        MY_APPLICATION = this
        mContext = this.applicationContext


        FirebaseApp.initializeApp(applicationContext)

    }



    fun setAPValueByKey(sharedPreferenceKey: String, value: String) {
        val sharedPreferences = getSharedPreferences("Kaushlesh", Context.MODE_PRIVATE)
        val prefsEditor = sharedPreferences.edit()
        prefsEditor.putString(sharedPreferenceKey, value)
        prefsEditor.apply()
    }





    override fun onActivityCreated(p0: Activity, p1: Bundle?) {
        onAppForegrounded = true
    }

    override fun onActivityStarted(p0: Activity) {
        onAppForegrounded = true
    }

    override fun onActivityResumed(p0: Activity) {
        onAppForegrounded = true
    }

    override fun onActivityPaused(p0: Activity) {
        onAppForegrounded = false
    }

    override fun onActivityStopped(p0: Activity) {
        onAppForegrounded = false
    }

    override fun onTrimMemory(level: Int) {
        if (level == TRIM_MEMORY_UI_HIDDEN) {
            onAppForegrounded = false
        }
        super.onTrimMemory(level)
    }

    override fun onActivitySaveInstanceState(p0: Activity, p1: Bundle) {
        onAppForegrounded = true
    }

    override fun onActivityDestroyed(p0: Activity) {
        onAppForegrounded = false

    }


}