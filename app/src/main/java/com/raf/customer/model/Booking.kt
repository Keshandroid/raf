package com.raf.customer.model

class Booking {

    private var serviceid: String = ""
    private var service_nm: String = ""
    private var price: String = ""
    private var Desc: String = ""
    private var serviceImage: String = ""


    @JvmName("getServiceid1")
    fun getServiceid(): String {
        return serviceid
    }

    @JvmName("getService_nm1")
    fun getServiceName(): String {
        return service_nm
    }

    fun setServiceName(nm: String) {
        service_nm = nm
    }

    @JvmName("setServiceid1")
    fun setServiceid(serviceid: String) {
        this.serviceid = serviceid
    }

    @JvmName("getPrice1")
    fun getPrice(): String {
        return price
    }

    @JvmName("setPrice1")
    fun setPrice(price: String) {
        this.price = price
    }

    @JvmName("getDesc1")
    fun getDesc(): String {
        return Desc
    }

    @JvmName("setDesc1")
    fun setDesc(desc: String) {
        Desc = desc
    }

    @JvmName("getServiceImage1")
    fun getServiceImage(): String {
        return serviceImage
    }

    @JvmName("setServiceImage1")
    fun setServiceImage(serviceImage: String) {
        this.serviceImage = serviceImage
    }

}