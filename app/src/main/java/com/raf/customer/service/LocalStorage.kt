package com.raf.customer.service

import android.content.Context
import android.content.SharedPreferences
import com.raf.customer.application.MyApplication

class LocalStorage(context: Context) {

    var KEY_FIREBASE = "keyFirebase"
    var KEY_TOKEN_FIREBASE = "fireToken"


    fun setToken(token: String?) {
        val sharedPreferences: SharedPreferences = MyApplication.applicationContext().getSharedPreferences(KEY_FIREBASE,
                Context.MODE_PRIVATE)
        val editor = sharedPreferences.edit()
        editor.putString(KEY_TOKEN_FIREBASE, token)
        editor.apply()
    }

    fun getToken(): String? {
        val sharedPreferences: SharedPreferences = MyApplication.applicationContext().getSharedPreferences(KEY_FIREBASE,
                Context.MODE_PRIVATE)
        return sharedPreferences.getString(KEY_TOKEN_FIREBASE, "")
    }

}