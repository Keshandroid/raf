package com.raf.customer.adapter

import android.content.ContentValues.TAG
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.raf.R
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.utils.Utils

class CategoryListAdapter(var activity: FragmentActivity?, var result1: List<GetCategoryResponse.Result?>?) :RecyclerView.Adapter<CategoryListAdapter.ViewHolder>() {

    private lateinit var iClickListener : onItemMyClickListener

     class ViewHolder(itemview: View):RecyclerView.ViewHolder(itemview) {
        var cat_img=itemview.findViewById<ImageView>(R.id.category_img)
        var btn_category=itemview.findViewById<TextView>(R.id.btn_category)
        var card_hair_holder = itemview.findViewById<CardView>(R.id.card_hair_holder)

    }

    fun setOnCategoryClickListener(iClickLIstener : onItemMyClickListener){
        this.iClickListener = iClickLIstener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryListAdapter.ViewHolder {
    var view=LayoutInflater.from(activity).inflate(R.layout.category_adapter,parent,false)
    return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CategoryListAdapter.ViewHolder, position: Int) {
        Utils.printLog(TAG, "onBindViewHolder:"+result1!!.get(position)!!.categoryName)
        holder.btn_category.setText(result1!!.get(position)!!.categoryName)
        Glide.with(activity!!).load(result1!!.get(position)!!.categoryImage).into(holder.cat_img)

        holder.btn_category.setOnClickListener(View.OnClickListener {
            iClickListener.onClick(result1!!.get(position)!!)
        })

        holder.card_hair_holder.setOnClickListener(View.OnClickListener {
            iClickListener.onClick(result1!!.get(position)!!)
            /* var intent=Intent(activity,SubServiceActivity::class.java)
             intent.putExtra(AppConstant.CAT_ID,result1!!.get(position)!!.categoryId)
             intent.putExtra("cat_nm",result1!!.get(position)!!.categoryName)
             activity!!.startActivity(intent)*/
        })


    }

    override fun getItemCount(): Int {
        return result1!!.size
    }


}

interface onItemMyClickListener
{
    fun onClick(data : GetCategoryResponse.Result)
}