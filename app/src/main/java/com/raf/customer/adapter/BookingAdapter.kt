package com.raf.customer.adapter

import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.raf.R

import com.raf.customer.retrofit.response.GetBookingServiceListResponse
import com.raf.customer.view.activity.BookingActivity

class BookingAdapter(
    var context: BookingActivity,
    var result1: GetBookingServiceListResponse?
) :
    RecyclerView.Adapter<BookingAdapter.MyViewHolder>() {
    private lateinit var iClickListener: onItemMyClickListener

    class MyViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var del = itemview.findViewById<ImageView>(R.id.img_remove)

        var tv_bsNm = itemview.findViewById<TextView>(R.id.tv_bsNm)
        var tv_bio = itemview.findViewById<TextView>(R.id.tv_bio)
        var tv_price = itemview.findViewById<TextView>(R.id.tv_price)
        var iv_img = itemview.findViewById<ImageView>(R.id.iv_img)
        var rl = itemview.findViewById<RelativeLayout>(R.id.rl)

    }

    public fun setOnCategoryClickListener(iClickLIstener: onItemMyClickListener) {
        this.iClickListener = iClickLIstener
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.booking_adapter, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.tv_bsNm.setText(result1!!.result!!.get(position).serviceName)
//        holder.tv_bio.setText(result1!!.result!!.get(position)!!.serviceDescription)
        var html = if(result1?.result!!.get(position)?.serviceDescription.isNullOrEmpty()) "No Details Available" else result1?.result!!.get(position)?.serviceDescription
        holder.tv_bio.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(html)
        }

        holder.tv_price.setText(result1!!.result!!.get(position)!!.servicePrice)
        Glide.with(context!!).load(result1!!.result!!.get(position)!!.serviceImage).into(holder.iv_img)
        holder.del.setOnClickListener(View.OnClickListener {
            notifyItemRemoved(position)
        })

        holder.del.setOnClickListener(View.OnClickListener {
            if (iClickListener != null)
            iClickListener.onDeleteService(result1!!.result!!.get(position).serviceId.toString()) })
    }

    override fun getItemCount(): Int {
        return result1!!.result!!.size
    }

    public interface onItemMyClickListener {
        fun onDeleteService(serviceId: String)
    }

}