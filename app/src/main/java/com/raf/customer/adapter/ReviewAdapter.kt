package com.raf.customer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.raf.R
import com.raf.customer.view.activity.HistoryDetailsActivity

class ReviewAdapter(var context:HistoryDetailsActivity):RecyclerView.Adapter<ReviewAdapter.ViewHolder>()
{
    class ViewHolder(itemview:View):RecyclerView.ViewHolder(itemview) {

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ReviewAdapter.ViewHolder {
        val view=LayoutInflater.from(context).inflate(R.layout.review_adapter,parent,false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
      return 3
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

    }
}