package com.raf.customer.adapter.employee

import android.app.AlertDialog
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.raf.R
import com.raf.customer.controller.employee.EmployeeOrderController
import com.raf.customer.retrofit.response.employee.EmployeeOrderResponse
import com.raf.customer.retrofit.response.employee.SuccessResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IEmployeeOrderView
import com.raf.employee.interfaces.EmployeeOrderListener

class EmployeeCurrentCompleteOrderAdapter(
    var activity: FragmentActivity?,
    var listData: List<EmployeeOrderResponse.Result>,
    var from: String,
    var listener: EmployeeOrderListener
) : RecyclerView.Adapter<EmployeeCurrentCompleteOrderAdapter.ViewHolder>(), IEmployeeOrderView {

    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var txt_order_no = itemview.findViewById<TextView>(R.id.txt_order_no)
        var ll_btn = itemview.findViewById<LinearLayout>(R.id.ll_btn)
        var btn_accept = itemview.findViewById<TextView>(R.id.btn_accept)
        var btn_reject = itemview.findViewById<TextView>(R.id.btn_reject)
        var btn_complete = itemview.findViewById<TextView>(R.id.btn_complete)
        var txt_date = itemview.findViewById<TextView>(R.id.txt_date)
        var txt_time = itemview.findViewById<TextView>(R.id.txt_time)
        var tv_price = itemview.findViewById<TextView>(R.id.tv_price)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        var view =
            LayoutInflater.from(activity).inflate(R.layout.item_current_order, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var strOrderIDStatus =
            activity!!.getString(R.string.order_id) + " : " + listData.get(position).bookingId.toString()
        if (listData.get(position).orderStatus == 0) {
            strOrderIDStatus += "\n(Booked)"
        } else if (listData.get(position).orderStatus == 1) {
            strOrderIDStatus += "\n(Accepted)"
        } else if (listData.get(position).orderStatus == 2) {
            strOrderIDStatus += "\n(Rejected)"
        } else if (listData.get(position).orderStatus == 3) {
            strOrderIDStatus += "\n(Completed)"
        }
        holder.txt_order_no.text = strOrderIDStatus
        holder.txt_date.text = listData.get(position).bookingDate
        holder.txt_time.text = listData.get(position).bookingTime
        holder.tv_price.text = listData.get(position).price

        if (from.equals("EmployeeCurrentOrder", true)) {
            if (listData.get(position).orderStatus == 0) {
                holder.ll_btn.visibility = View.VISIBLE
                holder.btn_accept.visibility = View.VISIBLE
                holder.btn_reject.visibility = View.VISIBLE
                holder.btn_complete.visibility = View.GONE
            } else if (listData.get(position).orderStatus == 1) {
                holder.ll_btn.visibility = View.VISIBLE
                holder.btn_accept.visibility = View.GONE
                holder.btn_reject.visibility = View.GONE
                holder.btn_complete.visibility = View.VISIBLE
            } else {
                holder.ll_btn.visibility = View.GONE
            }
        } else {
            holder.ll_btn.visibility = View.GONE
        }
        holder.btn_accept.setOnClickListener {

            val builder = AlertDialog.Builder(activity)

            builder.setMessage("Do you want to Accept Order?")
            builder.setIcon(android.R.drawable.ic_dialog_alert)

            builder.setPositiveButton("Yes") { dialogInterface, which ->
                dialogInterface.dismiss()
                var appPreference = AppPreference(activity!!)
                var uid = appPreference.getString(AppConstant.UID)
                var utoken = appPreference.getString(AppConstant.UTOKEN)
                var controller = EmployeeOrderController(activity!!, this)
                controller!!.callOrderStatusApi(uid, utoken, listData.get(position).bookingId, "1")
            }

            builder.setNegativeButton("No") { dialogInterface, which ->
                dialogInterface.dismiss()
            }
            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()

        }
        holder.btn_reject.setOnClickListener {

            val builder = AlertDialog.Builder(activity)

            builder.setMessage("Do you want to Reject Order?")
            builder.setIcon(android.R.drawable.ic_dialog_alert)

            builder.setPositiveButton("Yes") { dialogInterface, which ->
                dialogInterface.dismiss()
                var appPreference = AppPreference(activity!!)
                var uid = appPreference.getString(AppConstant.UID)
                var utoken = appPreference.getString(AppConstant.UTOKEN)
                var controller = EmployeeOrderController(activity!!, this)
                controller!!.callOrderStatusApi(uid, utoken, listData.get(position).bookingId, "3")
            }

            builder.setNegativeButton("No") { dialogInterface, which ->
                dialogInterface.dismiss()
            }
            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()

        }
        holder.btn_complete.setOnClickListener {

            val builder = AlertDialog.Builder(activity)

            builder.setMessage("Do you want to Complete Order?")
            builder.setIcon(android.R.drawable.ic_dialog_alert)

            builder.setPositiveButton("Yes") { dialogInterface, which ->
                dialogInterface.dismiss()
                var appPreference = AppPreference(activity!!)
                var uid = appPreference.getString(AppConstant.UID)
                var utoken = appPreference.getString(AppConstant.UTOKEN)
                var controller = EmployeeOrderController(activity!!, this)
                controller!!.callOrderStatusApi(uid, utoken, listData.get(position).bookingId, "2")
            }

            builder.setNegativeButton("No") { dialogInterface, which ->
                dialogInterface.dismiss()
            }
            val alertDialog: AlertDialog = builder.create()
            alertDialog.setCancelable(false)
            alertDialog.show()
        }
    }

    override fun getItemCount(): Int {
        return listData.size
    }

    override fun onEmployeeCurrentOrderSuccess(response: EmployeeOrderResponse, message: String) {
    }

    override fun onEmployeeCompleteOrderSuccess(response: EmployeeOrderResponse, message: String) {
    }

    override fun onEmployeeOrderStatusSuccess(response: SuccessResponse, message: String) {
        listener.itemClick()
    }

    override fun onFailure(msg: String) {

    }

    override fun showProgress() {
    }

    override fun dismissProgress() {
    }
}