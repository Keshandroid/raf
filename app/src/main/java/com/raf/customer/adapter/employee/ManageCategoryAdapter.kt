package com.raf.customer.adapter.employee

import android.app.AlertDialog
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.raf.R
import com.raf.customer.controller.GetCategoryController
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IGetCategoryView
import com.raf.employee.view.activity.EmployeeEditCategoryActivity
import com.raf.employee.view.activity.EmployeeManageCategoryActivity
import com.raf.employee.view.activity.EmployeeManageSubCategoryActivity

class ManageCategoryAdapter(
    var context: EmployeeManageCategoryActivity,
    var result: ArrayList<GetCategoryResponse.Result?>
) :
    RecyclerView.Adapter<ManageCategoryAdapter.ViewHolder>(), IGetCategoryView {
    var pos: Int = 0

    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var txt_category = itemview.findViewById<TextView>(R.id.txt_category)
        var img_category = itemview.findViewById<ImageView>(R.id.img_category)
        var img_delete = itemview.findViewById<ImageView>(R.id.img_delete)
        var img_edit = itemview.findViewById<ImageView>(R.id.img_edit)
        var txt_manage_sub_category = itemview.findViewById<TextView>(R.id.txt_manage_sub_category)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ManageCategoryAdapter.ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_manage_category, parent, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return result.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var data = result.get(position) as GetCategoryResponse.Result
        pos = position
        if (data.categoryName != null && data.categoryName.length > 0)
            holder.txt_category.text = data.categoryName

        if (data.categoryImage != null && data.categoryImage.length > 0) {
            Glide.with(context).load(data.categoryImage).into(holder.img_category)
        }

        holder.img_delete.setOnClickListener(View.OnClickListener {
            displayAlterDialog(data)

        })

        holder.img_edit.setOnClickListener(View.OnClickListener {
            var intent = Intent(context, EmployeeEditCategoryActivity::class.java)
            intent.putExtra("categoryId", data.categoryId.toString())
            intent.putExtra("categoryName", data.categoryName)
            intent.putExtra("categoryImage", data.categoryImage)
            context.startActivity(intent)

        })

        holder.txt_manage_sub_category.setOnClickListener(View.OnClickListener {

            val intent = Intent(context, EmployeeManageSubCategoryActivity::class.java)
            intent.putExtra("cat_id", "" + data.categoryId)
            context.startActivity(intent)

        })

    }

    private fun displayAlterDialog(data: GetCategoryResponse.Result) {
        val builder = AlertDialog.Builder(context)
        //set title for alert dialog
        builder.setTitle("Warning Dialog")
        //set message for alert dialog
        builder.setMessage("Are you want to delete this category?")
        builder.setIcon(R.drawable.delete)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            apiCalling(data)
        }
        //performing negative action
        builder.setNegativeButton("No") { dialogInterface, which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun apiCalling(data: GetCategoryResponse.Result) {
        lateinit var iGetCategoryController: GetCategoryController
        iGetCategoryController = GetCategoryController(context, context)
        var appPreference = AppPreference(context)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
        iGetCategoryController.callDeleteCategotyApi(uid, utoken, data.categoryId.toString())
        result.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun onGetCategorySuccess(response: GetCategoryResponse, message: String) {
        result.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun onFailure(msg: String) {

    }

    override fun showProgress() {

    }

    override fun dismissProgress() {

    }

}