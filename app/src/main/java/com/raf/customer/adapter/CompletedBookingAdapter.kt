package com.raf.customer.adapter

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.raf.R
import com.raf.customer.retrofit.response.GetCurrentBookingResponse
import com.raf.customer.view.activity.BookingDetailsActivity

class CompletedBookingAdapter(
    var activity: FragmentActivity?,
    var response: GetCurrentBookingResponse
) : RecyclerView.Adapter<CompletedBookingAdapter.ViewHolder>() {

    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var txt_booking_no = itemview.findViewById<TextView>(R.id.txt_booking_no)
        var txt_date = itemview.findViewById<TextView>(R.id.txt_date)
        var txt_time = itemview.findViewById<TextView>(R.id.txt_time)
        var tv_price = itemview.findViewById<TextView>(R.id.tv_price)
        var tvCancel = itemview.findViewById<TextView>(R.id.tvCancel)
        var txtOrderStatus = itemview.findViewById<TextView>(R.id.txtOrderStatus)

    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): CompletedBookingAdapter.ViewHolder {
        var view =
            LayoutInflater.from(activity).inflate(R.layout.current_booking_adpater, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: CompletedBookingAdapter.ViewHolder, position: Int) {
        holder.txt_booking_no.text=response.result!!.get(position).bookingIid.toString()
        holder.txt_date.text=response.result!!.get(position).bookingDate
        holder.txt_time.text=response.result!!.get(position).bookingTime
        holder.tv_price.text=response.result!!.get(position).price


        if(response.result!!.get(position).bookingStatus.toString() == "0"){
            holder.tvCancel.visibility = View.VISIBLE
            holder.txtOrderStatus.text = "Pending"
            holder.txtOrderStatus.backgroundTintList  = activity!!.getResources().getColorStateList(R.color.grey, activity!!.theme)
        }else if(response.result!!.get(position).bookingStatus.toString() == "1"){
            holder.tvCancel.visibility = View.GONE
            holder.txtOrderStatus.text = "Accepted"
            holder.txtOrderStatus.backgroundTintList  = activity!!.getResources().getColorStateList(R.color.light_green, activity!!.theme)
        }else if (response.result!!.get(position).bookingStatus.toString() == "2"){
            holder.tvCancel.visibility = View.GONE
            holder.txtOrderStatus.text = "Completed"
            holder.txtOrderStatus.backgroundTintList  = activity!!.getResources().getColorStateList(R.color.dark_green, activity!!.theme)
        }else if (response.result!!.get(position).bookingStatus.toString() == "3"){
            holder.tvCancel.visibility = View.GONE
            holder.txtOrderStatus.text = "Rejected"
            holder.txtOrderStatus.backgroundTintList  = activity!!.getResources().getColorStateList(R.color.red, activity!!.theme)
        }else if (response.result!!.get(position).bookingStatus.toString() == "4"){
            holder.tvCancel.visibility = View.GONE
            holder.txtOrderStatus.text = "Expired"
            holder.txtOrderStatus.backgroundTintList  = activity!!.getResources().getColorStateList(R.color.orange, activity!!.theme)
        }

        holder.itemView.setOnClickListener(View.OnClickListener {
            activity!!.startActivity(Intent(activity, BookingDetailsActivity::class.java).putExtra("bookingIid",""+response.result!!.get(position).bookingIid))
        })

        holder.tvCancel.setOnClickListener {

        }
    }

    override fun getItemCount(): Int {
        return response.result!!.size
    }

    public interface IClickListener{
        public fun onDeleteClicked(bookingId : String)
    }
}