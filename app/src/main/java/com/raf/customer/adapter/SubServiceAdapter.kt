package com.raf.customer.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.raf.R
import com.raf.customer.retrofit.response.GetSubCategoryResponse
import com.raf.customer.view.activity.SubServiceActivity

class SubServiceAdapter(
    var context: SubServiceActivity, var subCat_list: List<GetSubCategoryResponse.SubCategory?>,
) : RecyclerView.Adapter<SubServiceAdapter.ViewHolder>() {
    private lateinit var onsubitemClick: onSubItemClickListener

    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var service_nm = itemview.findViewById<TextView>(R.id.service_name)
        var chargers = itemview.findViewById<TextView>(R.id.chargers)
        var img1 = itemview.findViewById<ImageView>(R.id.img1)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int,

        ): SubServiceAdapter.ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.sub_service_adapter, parent, false)
        return ViewHolder(view)

    }

    fun setOnClickListener(onsubitemClickListener: onSubItemClickListener) {
        onsubitemClick = onsubitemClickListener
    }

    override fun onBindViewHolder(holder: SubServiceAdapter.ViewHolder, position: Int) {

        holder.service_nm.setText(subCat_list.get(position)!!.subCategoryName)
        holder.chargers.setText(subCat_list.get(position)!!.subcategoryPrice)

        holder.itemView.setOnClickListener(View.OnClickListener {

            onsubitemClick.onClick(subCat_list.get(position)!!)
            /*context.startActivity(
                Intent(context, ServiceDetailsActivity::class.java)
                    .putExtra(AppConstant.SUBCAT_ID, subCat_list.get(position)?.subCategoryId)
            )*/
        })

        Glide.with(holder.img1.context)
            .load(subCat_list.get(position)!!.subCategoryImage)
            .error(R.drawable.placeholder)
            .into(holder.img1)
    }

    override fun getItemCount(): Int {
        return subCat_list.size
    }

    interface onSubItemClickListener {
        fun onClick(data: GetSubCategoryResponse.SubCategory)

    }
}