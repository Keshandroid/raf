package com.raf.customer.adapter

import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.raf.R
import com.raf.customer.retrofit.response.GetBookingDetailResponse
import com.raf.customer.view.activity.BookingDetailsActivity

class BookingDetailsAdapter(
    var context: BookingDetailsActivity,
    var serviceData: List<GetBookingDetailResponse.ServiceData>?,
) : RecyclerView.Adapter<BookingDetailsAdapter.ViewHolder>() {

    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var iv_simg = itemview.findViewById<ImageView>(R.id.iv_simg)
        var tv_sNM = itemview.findViewById<TextView>(R.id.tv_sNM)
        var tv_sDis = itemview.findViewById<TextView>(R.id.tv_sDis)
        var tv_price = itemview.findViewById<TextView>(R.id.tv_price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.booking_details_adapter, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.tv_sNM.text = serviceData!!.get(position).servicePrice
        var html = serviceData!!.get(position).serviceDescription
        if (html != null)
            holder.tv_sDis.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
            } else {
                Html.fromHtml(html)
            }
        holder.tv_price.text = serviceData!!.get(position).servicePrice
        Glide.with(context!!).load(serviceData!!.get(position)!!.serviceImage).error(R.drawable.no_image).into(holder.iv_simg)

    }

    override fun getItemCount(): Int {
        return serviceData!!.size
    }
}