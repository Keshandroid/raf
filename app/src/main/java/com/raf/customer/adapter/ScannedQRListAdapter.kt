package com.raf.customer.adapter

import android.app.Activity
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

import com.raf.databinding.ItemScanHistoryItemBinding

class ScannedQRListAdapter(
    var context: Activity,
    var dataList: ArrayList<String>, var iClickListener: IClickLisetener) :
    RecyclerView.Adapter<ScannedQRListAdapter.MyViewHolder>() {

    class MyViewHolder(val binding: ItemScanHistoryItemBinding) : RecyclerView.ViewHolder(binding.root) {
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
//        val view = LayoutInflater.from(context).inflate(R.layout.booking_adapter, parent, false)
        val binding = ItemScanHistoryItemBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(binding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        holder.binding.tvId.text = dataList.get(position)
        holder.itemView.setOnClickListener {
            iClickListener.onIdClicked(dataList.get(position))
        }
    }

    override fun getItemCount(): Int {
        return dataList.size
    }

    public interface IClickLisetener {
        fun onIdClicked(qrId: String)
    }

}