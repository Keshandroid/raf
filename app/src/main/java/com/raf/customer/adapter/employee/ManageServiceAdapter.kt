package com.raf.customer.adapter.employee

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.raf.R
import com.raf.customer.controller.employee.GetEmpServiceController
import com.raf.customer.retrofit.response.employee.GetEmpServiceResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IGetEmpServiceView
import com.raf.employee.view.activity.EmployeeEditCategoryActivity
import com.raf.employee.view.activity.EmployeeEditServiceActivity

class ManageServiceAdapter(
    var context: Context,
    var result: ArrayList<GetEmpServiceResponse.Result?>,
    var cat_id: String,
    var sub_cat_id: String
) :
    RecyclerView.Adapter<ManageServiceAdapter.ViewHolder>(), IGetEmpServiceView {
    var pos: Int = 0

    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var txt_service_name = itemview.findViewById<TextView>(R.id.txt_service_name)
        var txt_service_description = itemview.findViewById<TextView>(R.id.txt_service_description)
        var txt_service_price = itemview.findViewById<TextView>(R.id.txt_service_price)
        var txt_service_time = itemview.findViewById<TextView>(R.id.txt_service_time)
        var img_service = itemview.findViewById<ImageView>(R.id.img_service)
        var img_delete = itemview.findViewById<ImageView>(R.id.img_delete)
        var img_edit = itemview.findViewById<ImageView>(R.id.img_edit)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_manage_service, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int {
        return result.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var data = result.get(position) as GetEmpServiceResponse.Result
        pos = position
        if (data.serviceName != null && data.serviceName.length > 0)
            holder.txt_service_name.text = data.serviceName

        if (data.description != null && data.description.length > 0) {
            holder.txt_service_description.text =
                HtmlCompat.fromHtml(data.description, HtmlCompat.FROM_HTML_MODE_LEGACY);
        }

        if (data.price != null && data.price.length > 0)
            holder.txt_service_price.text = data.price

        if (data.avragetime != null && data.avragetime.length > 0)
            holder.txt_service_time.text =
                context.resources.getString(R.string.average_time) + " : " + data.avragetime

        if (data.serviceImage != null && data.serviceImage.length > 0) {
            Glide.with(context).load(data.serviceImage).into(holder.img_service)
        }

        holder.img_edit.setOnClickListener(View.OnClickListener {
            var intent = Intent(context, EmployeeEditServiceActivity::class.java)
            intent.putExtra("cat_id", cat_id)
            intent.putExtra("sub_cat_id", sub_cat_id)
            intent.putExtra("serviceId", data.serviceId)
            intent.putExtra("serviceName", data.serviceName)
            intent.putExtra("serviceImage", data.serviceImage)
            intent.putExtra("description", data.description)
            intent.putExtra("avragetime", data.avragetime)
            context.startActivity(intent)
        })
        holder.img_delete.setOnClickListener(View.OnClickListener {
            displayAlterDialog(data)
        })
    }

    private fun displayAlterDialog(data: GetEmpServiceResponse.Result) {
        val builder = AlertDialog.Builder(context)
        //set title for alert dialog
        builder.setTitle("Warning Dialog")
        //set message for alert dialog
        builder.setMessage("Are you want to delete this Service?")
        builder.setIcon(R.drawable.delete)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            apiCalling(data)
        }
        //performing negative action
        builder.setNegativeButton("No") { dialogInterface, which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun apiCalling(data: GetEmpServiceResponse.Result) {
        lateinit var getServiceController: GetEmpServiceController
        getServiceController = GetEmpServiceController(context, this)
        var appPreference = AppPreference(context)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
        getServiceController.callDeleteServiceApi(uid, utoken, data.serviceId.toString())
        result.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun onGetEmpServiceSuccess(response: GetEmpServiceResponse, message: String) {
        result.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun onFailure(msg: String) {

    }

    override fun showProgress() {

    }

    override fun dismissProgress() {

    }

}