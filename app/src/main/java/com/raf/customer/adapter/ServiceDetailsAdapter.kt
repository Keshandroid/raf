package com.raf.customer.adapter

import android.content.Context
import android.os.Build
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.material.tabs.TabLayout
import com.raf.R
import com.raf.customer.retrofit.response.GetServiceResponse

class ServiceDetailsAdapter(
    var context: Context,
    var result1: GetServiceResponse?,
    var clickListener: onItemMyClickListener,
) :
    RecyclerView.Adapter<ServiceDetailsAdapter.MyViewHolder>() {

    class MyViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var service_viewpager = itemview.findViewById<ViewPager>(R.id.service_viewpager)
        var dot_tablayout = itemview.findViewById<TabLayout>(R.id.dot_tablayout)
        var txt_service_nm = itemview.findViewById<TextView>(R.id.txt_service_nm)
        var txt_price = itemview.findViewById<TextView>(R.id.txt_price)
        var txt_avg_time = itemview.findViewById<TextView>(R.id.txt_avg_time)
        var txt_service_details = itemview.findViewById<TextView>(R.id.txt_service_details)
        var ivNoImage = itemview.findViewById<ImageView>(R.id.ivNoImage)


    }

    /* public fun setOnCategoryClickListener(iClickLIstener: onItemMyClickListener) {
         this.iClickListener = iClickLIstener
     }*/

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(context).inflate(R.layout.service_detail_item, parent, false)
        return MyViewHolder(view)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        /*  holder.tv_bsNm.setText(result1!!.result!!.get(position)!!.serviceName)
         // holder.tv_bio.setText(result1!!.result!!.get(position)!!.serviceDescription)

          //holder.tv_price.setText(result1!!.result!!.get(position)!!.servicePrice)
          Glide.with(context!!).load(result1!!.result!!.get(position)!!.serviceImage).into(holder.iv_img)
          holder.del.setOnClickListener(View.OnClickListener {
              notifyItemRemoved(position)
          })*/
        // holder.txt_heading.setText(res?.get(0)?.serviceName)
        holder.txt_service_nm.setText(result1?.result!!.get(position)?.serviceName)
        holder.txt_avg_time.setText(result1?.result!!.get(position)?.avgTime + "min")
        holder.txt_price.setText(result1?.result!!.get(position)?.price)
//        holder.txt_service_details.setText(result1?.result!!.get(position)?.description)
        var html = if(result1?.result!!.get(position)?.description.isNullOrEmpty()) "No Details Available" else result1?.result!!.get(position)?.description
        holder.txt_service_details.text = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(html, Html.FROM_HTML_MODE_COMPACT)
        } else {
            Html.fromHtml(html)
        }
        var imgarray= ArrayList<String>()
        if (result1!!.result!!.get(position)!!.serviceImages.isNullOrEmpty()) {
            holder.ivNoImage.visibility = View.VISIBLE
        } else {
            for (i in 0..result1!!.result!!.get(position)!!.serviceImages!!.size - 1) {
                imgarray.add(result1!!.result!!.get(position)!!.serviceImages!!.get(i)!!.serviceImgUrl)
               /* imgarray =
                    listOf(result1!!.result!!.get(position)!!.serviceImages!!.get(i)!!.serviceImgUrl)!!*/
            }
            holder.service_viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(
                holder.dot_tablayout))
            val adpt = ServicePagerAdapter(context, imgarray)
            holder.service_viewpager.adapter = adpt
            holder.dot_tablayout.setupWithViewPager(holder.service_viewpager)
        }

        for (i in 0 until holder.dot_tablayout.tabCount) {
            val tab = (holder.dot_tablayout.getChildAt(0) as ViewGroup).getChildAt(i)
            val p = tab.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(0, 0, 10, 0)
            tab.requestLayout()
        }

        holder.txt_price.setOnClickListener {
            clickListener.onClick(result1!!.result!!.get(position)!!)
        }
    }

    override fun getItemCount(): Int {
        return result1!!.result!!.size
    }

    public interface onItemMyClickListener {
        fun onClick(data: GetServiceResponse.Result)
    }


}