package com.raf.customer.adapter.employee

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.raf.R
import com.raf.customer.controller.GetSubCategoryController
import com.raf.customer.retrofit.response.GetSubCategoryResponse
import com.raf.customer.retrofit.response.employee.GetEmpSubCategoryResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IGetCategoryView
import com.raf.customer.view.viewInterface.IGetSubCategoryView
import com.raf.employee.view.activity.EmployeeEditSubCategoryActivity
import com.raf.employee.view.activity.EmployeeManageServiceActivity
import com.raf.employee.view.activity.EmployeeManageSubCategoryActivity
import java.io.Serializable

class ManageSubCategoryAdapter(
    var context: Context,
    var result: ArrayList<GetEmpSubCategoryResponse.Result?>,
    var cat_id: String
) :
    RecyclerView.Adapter<ManageSubCategoryAdapter.ViewHolder>(), IGetSubCategoryView {
    var pos: Int = 0

    class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        var txt_sub_category = itemview.findViewById<TextView>(R.id.txt_sub_category)
        var txt_manage_service = itemview.findViewById<TextView>(R.id.txt_manage_service)
        var img_category = itemview.findViewById<ImageView>(R.id.img_category)
        var img_delete = itemview.findViewById<ImageView>(R.id.img_delete)
        var img_edit = itemview.findViewById<ImageView>(R.id.img_edit)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ViewHolder {
        val view =
            LayoutInflater.from(context).inflate(R.layout.item_manage_sub_category, parent, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return result.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        var data = result.get(position) as GetEmpSubCategoryResponse.Result
        pos = position
        var strSubCatNamePrice = ""
        if (data.subCategoryName != null && data.subCategoryName.length > 0)
            strSubCatNamePrice = data.subCategoryName

        if (data.subcategoryPrice != null && data.subcategoryPrice.length > 0)
            strSubCatNamePrice += "(" + data.subcategoryPrice + ")"

        holder.txt_sub_category.text = strSubCatNamePrice

        if (data.subcategoryImage != null && data.subcategoryImage.length > 0) {
            Glide.with(context).load(data.subcategoryImage).into(holder.img_category)
        }

        holder.img_delete.setOnClickListener(View.OnClickListener {
            displayAlterDialog(data)
        })
        holder.img_edit.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, EmployeeEditSubCategoryActivity::class.java)
            intent.putExtra("cat_id", "" + cat_id)
            intent.putExtra("sub_cat_id", "" + data.subcategoryId)
            intent.putExtra("subCategoryName", "" + data.subCategoryName)
            intent.putExtra("subcategoryPrice", "" + data.subcategoryPrice)
            intent.putExtra("subcategoryImage", "" + data.subcategoryImage)
            context.startActivity(intent)
        })
        holder.txt_manage_service.setOnClickListener(View.OnClickListener {
            val intent = Intent(context, EmployeeManageServiceActivity::class.java)
            intent.putExtra("cat_id", "" + cat_id)
            intent.putExtra("sub_cat_id", "" + data.subcategoryId)
            context.startActivity(intent)
        })
    }

    private fun displayAlterDialog(data: GetEmpSubCategoryResponse.Result) {
        val builder = AlertDialog.Builder(context)
        //set title for alert dialog
        builder.setTitle("Warning Dialog")
        //set message for alert dialog
        builder.setMessage("Are you want to delete this Service?")
        builder.setIcon(R.drawable.delete)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            apiCalling(data)
        }
        //performing negative action
        builder.setNegativeButton("No") { dialogInterface, which ->

        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    private fun apiCalling(data: GetEmpSubCategoryResponse.Result) {
        lateinit var iGetSubCategoryController: GetSubCategoryController
        iGetSubCategoryController = GetSubCategoryController(context, this)
        var appPreference = AppPreference(context)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
        iGetSubCategoryController.callDeleteSubCategotyApi(
            uid,
            utoken,
            data.subcategoryId.toString()
        )
        result.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun onGetSubCategorySuccess(response: GetSubCategoryResponse, message: String) {
        result.removeAt(pos)
        notifyDataSetChanged()
    }

    override fun onFailure(msg: String) {

    }

    override fun showProgress() {

    }

    override fun dismissProgress() {

    }
}