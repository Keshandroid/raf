package com.raf.customer.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.viewpager.widget.PagerAdapter
import com.bumptech.glide.Glide
import com.raf.R
import kotlinx.android.synthetic.main.image_item.view.*
import java.util.*

class ServicePagerAdapter(var context: Context, var imgarray: List<String>?) :PagerAdapter() {
    override fun getCount(): Int {
        return imgarray!!.size
    }

    override fun isViewFromObject(view: View, object1: Any): Boolean {

        return view== object1 as View
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        var view= LayoutInflater.from(context).inflate(R.layout.image_item,container,false)
       // view.item_img.setImageResource(imgarray.get(position))
        Glide.with(context!!)
            .load(imgarray!!.get(position))
            .error(R.drawable.no_image)
            .into( view.item_img)

        Objects.requireNonNull(container).addView(view)
        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, object1: Any) {
//        super.destroyItem(container, position, object1)
        container.removeView(object1 as View?)
    }
}
