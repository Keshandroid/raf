package com.raf.customer.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.raf.R
import com.raf.employee.view.activity.EmployeeLoginActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        btn_cust.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@MainActivity,CustomerLoginActivity::class.java))
            finish()
        })

        btn_employ.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@MainActivity, EmployeeLoginActivity::class.java))
            finish()
        })
    }
}