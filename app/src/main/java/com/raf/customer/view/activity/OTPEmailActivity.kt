package com.raf.customer.view.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.ForgotPasswordController
import com.raf.customer.controller.VerifyEmailController
import com.raf.customer.controller.listener.IforgotPasswordController
import com.raf.customer.controller.listener.IverifyEmailController
import com.raf.customer.retrofit.response.VerifyEmailResponse
import com.raf.customer.view.viewInterface.IVerifyEmailView
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_otp_verification.*

class OTPEmailActivity : BaseActivity(),IVerifyEmailView {

    internal lateinit var txt_heading: TextView
    internal lateinit var btnback: ImageView
    internal lateinit var tvtoolbartitle: TextView
    internal lateinit var btn_submit_otp: TextView

    lateinit var  iverifyEmailController: IverifyEmailController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp_verification)

        val emailOTP = intent.getStringExtra("emailOTP")
        val email = intent.getStringExtra("email")


        txt_heading = findViewById(R.id.txt_heading)
        btn_submit_otp = findViewById(R.id.btn_submit_otp)

        btnback = findViewById(R.id.back)



        txt_heading.text = "OTP"


        btnback.setOnClickListener {
            onBackPressed()
        }

        iverifyEmailController= VerifyEmailController(this,this)


        btn_submit_otp.setOnClickListener{

            Log.d("email111",""+email)

            if(et_one.text.toString() != ""){

                iverifyEmailController.callVerifyEmailApi(email!!,et_one.text.toString())


            }else{
                Toast.makeText(this@OTPEmailActivity,"Invalid OTP",Toast.LENGTH_SHORT).show()
            }
        }

    }

    override fun onVerifyEmailSuccess(response: VerifyEmailResponse, message: String) {
        val intent = Intent(applicationContext, CustomerLoginActivity::class.java)
        startActivity(intent)
        finishAffinity()
    }

    override fun onFailure(msg: String) {
        Toast.makeText(this@OTPEmailActivity,"Verification Failed",Toast.LENGTH_SHORT).show()

    }

    override fun showProgress() {
        super.showProgress()
    }

    override fun dismissProgress() {
        super.dismissProgress()
    }
}
