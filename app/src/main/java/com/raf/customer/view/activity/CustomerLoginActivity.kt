package com.raf.customer.view.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.activity.result.contract.ActivityResultContracts.StartActivityForResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthProvider
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.LoginController
import com.raf.customer.controller.listener.ILoginController
import com.raf.customer.retrofit.response.LoginResponse
import com.raf.customer.retrofit.response.SocialLoginResponse
import com.raf.customer.utils.CustomPassword
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.ILoginView
import kotlinx.android.synthetic.main.activity_customer_login.*
import kotlinx.android.synthetic.main.login_top.*

class CustomerLoginActivity : BaseActivity(), ILoginView {

    private lateinit var firebaseAuth: FirebaseAuth
    private lateinit var iLoginController : ILoginController

    //    lateinit var sharedPreferences: SharedPreferences
//    lateinit var editor: SharedPreferences.Editor
    lateinit var signInClient: GoogleSignInClient
    lateinit var signInOptions: GoogleSignInOptions

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_customer_login)

        /* sharedPreferences = getSharedPreferences("Raf", MODE_PRIVATE)
         editor = sharedPreferences.edit()


         var b = sharedPreferences.getBoolean("islogin", false)*/

        /* if(b)
         {
             startActivity(Intent(this@CustomerLoginActivity,GetCategoryActivity::class.java))
             finish()
         }
 */
        iLoginController = LoginController(this, this)


        cust_login_back.setOnClickListener(View.OnClickListener {
            finish()
        })

        txt_forgotpasword.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@CustomerLoginActivity, ForgotPasswordActivity::class.java))
        })
        go_to_register.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@CustomerLoginActivity, RegisterActivtiy::class.java))
        })

        google_lgn.setOnClickListener {
            val signInIntent: Intent = signInClient.getSignInIntent()
            launchGoogleLogin.launch(signInIntent)
        }

        CustomPassword(et_lgn_password, img_seen)
        btn_login.setOnClickListener(View.OnClickListener {


            if (et_lgn_email.text.toString() == "" && et_lgn_password.text.toString() == "") {
                Utils.showToast(this, "Please Enter Login Details")
            } else {
                var email = et_lgn_email.text.toString()
                var password = et_lgn_password.text.toString()
                iLoginController.callLoginApi(email, password)

            }

        })

        setupGoogleLogin()

    }



    var launchGoogleLogin = registerForActivityResult(
        StartActivityForResult()) { result ->

        Log.d("handleSign","${result.resultCode}")


        if (result.resultCode == Activity.RESULT_OK) {

            Log.d("handleSign","222")


            // There are no request codes
            val data = result.data
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

    private fun setupGoogleLogin() {
        FirebaseApp.initializeApp(this)
        signInOptions = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.google_client_ID))
            .requestEmail()
            .build()
        signInClient = GoogleSignIn.getClient(this, signInOptions)
        firebaseAuth = FirebaseAuth.getInstance()
    }


    private fun handleSignInResult(completedTask: Task<GoogleSignInAccount>) {

        Log.d("handleSign","111")

        try {
            val account = completedTask.getResult(ApiException::class.java)
            if (account != null) {
                val credential = GoogleAuthProvider.getCredential(account.idToken, null)
                firebaseAuth.signInWithCredential(credential).addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val token = account.idToken
                        val id = account.id
                        val email = account.email
                        val fullName = account.displayName
                        var firstName = account.displayName

                        var lastName = ""
                        if (fullName!!.contains(" "))
                        {
                            firstName = fullName.split(" ")[0]
                            lastName = fullName.split(" ")[1]
                        }
                        iLoginController.callSocialApi(firstName!!,lastName,email!!,"gmail", token!!, id!!)
                    }
                }

            }
        } catch (e: ApiException) {
            // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Utils.printLog(CustomerLoginActivity::class.java.simpleName, "signInResult:failed code=" + e.statusCode)
            //            updateUI(null);
        }
    }
    override fun onLoginSuccess(response: LoginResponse, message: String) {
        if (message == "Login_Success") {

            /*    var appPreference = AppPreference(this)
                appPreference.setString(AppConstant.USERID, response.result!!.uid.toString())
                editor.putBoolean("islogin", true)
                editor.putString("uid", response.result!!.uid.toString())
                editor.putString("utoken", response.result!!.utoken)
                editor.putString("firsname", response.result!!.firstname)
                editor.putString("lastname", response.result!!.lastName)
                editor.putString("email", response.result!!.email)
                editor.putString("phon_no", response.result!!.contact_no)
                editor.putString("gender", response.result!!.gender)
                editor.putString("pic", response.result!!.profile_pic)
                editor.commit()*/
            /*  startActivity(
                  Intent(this@CustomerLoginActivity, HomeActivity::class.java)
              )
  */
            startActivity(
                Intent(this@CustomerLoginActivity, HomeActivity::class.java)
            )

            /*  startActivity(Intent(this@CustomerLoginActivity, GetCategoryActivity::class.java))*/
            finish()
        }
    }

    override fun onSocialLoginSuccess(response: SocialLoginResponse, message: String) {
        if (message == "Login_Success") {
            startActivity(
                Intent(this@CustomerLoginActivity, HomeActivity::class.java)
            )
            finish()
        }
    }

    override fun onFailure(msg: String) {
        Utils.printLog("LoginActivity", "onFailure:=====>" + msg)

    }


}