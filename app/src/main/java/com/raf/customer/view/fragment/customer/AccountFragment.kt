package com.raf.customer.view.fragment.customer

import android.app.AlertDialog
import android.content.ContentValues
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth
import com.google.zxing.integration.android.IntentIntegrator
import com.raf.R
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.activity.*
import kotlinx.android.synthetic.main.fragment_account.*
import kotlinx.android.synthetic.main.topbar_view2.*


class AccountFragment() : Fragment(R.layout.fragment_account) {
    // declare the GoogleSignInClient
    lateinit var mGoogleSignInClient: GoogleSignInClient

    private val auth by lazy {
        FirebaseAuth.getInstance()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        txt_heading2.setText("Account")

        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestIdToken(getString(R.string.google_client_ID))
            .requestEmail()
            .build()
        mGoogleSignInClient = GoogleSignIn.getClient(requireActivity(), gso)


        linear_contactus.setOnClickListener(View.OnClickListener {
            startActivity(Intent(activity, ContactUsActivity::class.java))
        })
        linear_logout.setOnClickListener(View.OnClickListener {
            showLogoutConfirmationDialog()
        })
        linear_terms.setOnClickListener(View.OnClickListener {
            startActivity(Intent(activity, TermsConditionActivity::class.java))
        })
        linear_privacy.setOnClickListener(View.OnClickListener {
            startActivity(Intent(activity, PrivacyPolicyActivity::class.java))
        })
        linear_profile.setOnClickListener(View.OnClickListener {
            startActivity(Intent(activity, UpdateProfileActivity::class.java))
        })
        linear_changepasword.setOnClickListener(View.OnClickListener {
            startActivity(Intent(activity, ChangePasswordActivity::class.java))

        })
        linear_rating.setOnClickListener(View.OnClickListener {
            startActivity(Intent(activity, RatingActivity::class.java))

        })
        ll_scan.setOnClickListener(View.OnClickListener {
            val intentIntegrator = IntentIntegrator(this.requireActivity())
            intentIntegrator.setCameraId(0)
            intentIntegrator.setOrientationLocked(true)
            intentIntegrator.setPrompt("Scan Qr code")
            intentIntegrator.initiateScan()
        })

        linScannedQRHistory.setOnClickListener {
            startActivity(Intent(activity, ScannedQRHistoryActivity::class.java))
        }

    }

    fun showLogoutConfirmationDialog(){
        val builder = AlertDialog.Builder(activity)

        //set message for alert dialog
        builder.setMessage("Are you sure to Logout?")
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            dialogInterface.dismiss()
            mGoogleSignInClient.signOut().addOnCompleteListener {
                var appPreference = AppPreference(requireContext())
                appPreference.clearData(requireContext())
                appPreference.setBoolean(AppConstant.ISLOGIN, false)
                startActivity(Intent(activity, CustomerLoginActivity::class.java))
                requireActivity().finish()
            }
        }

        //performing negative action
        builder.setNegativeButton("No") { dialogInterface, which ->
            dialogInterface.dismiss()
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (result != null) {
            /*//sercivePro,1
              val str=""
            str.replace("sercivePro,","")
            */
            //Utils.printLog(TAG, "onActivityResult:"+result.contents)
            Utils.printLog(ContentValues.TAG, "onActivityResult: " + result.contents.substringAfter(","))


            /*  var id = result.contents.substringAfter(",")
              iGetCategoryController.callGetcategotyApi(uid, utoken, id)
              editor.putString(AppConstant.SERVICE_PRO_ID, id)
              editor.commit()*/

        } else {
            Utils.printLog(ContentValues.TAG, "nullll ")
        }

    }

}