package com.raf.customer.view.activity

import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.R
import com.raf.customer.adapter.BookingDetailsAdapter
import com.raf.customer.controller.GetBookingDetailController
import com.raf.customer.controller.listener.IGetBookingDetailController
import com.raf.customer.retrofit.response.GetBookingDetailResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IGetBookingDetailView
import com.raf.databinding.ActivityBookingDetailsBinding
import kotlinx.android.synthetic.main.topbar_view.*

class BookingDetailsActivity : AppCompatActivity(), IGetBookingDetailView {

    private lateinit var binding : ActivityBookingDetailsBinding
    var bookingIid: String = ""
    var controller: IGetBookingDetailController? = null
    var uid = ""
    var utoken = ""
    var serviceData: List<GetBookingDetailResponse.ServiceData>? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityBookingDetailsBinding.inflate(layoutInflater)
        setContentView(binding.root)

        window.statusBarColor = getColor(R.color.app_header)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText("Booking Detail")
        var appPreference = AppPreference(this)
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)
        bookingIid = intent!!.getStringExtra("bookingIid")!!
        controller = GetBookingDetailController(this, this)
        controller!!.callGetBookingDetailApi(uid, utoken, bookingIid)

    }

    override fun onIGetBookingDetailSuccess(response: GetBookingDetailResponse, message: String) {
        binding.txtDate.text=response.result!!.get(0).bookingDate
        binding.txtTime.text=response.result!!.get(0).bookingTime
        binding.txtDuration.text=response.result!!.get(0).bookingTime

        // 0-Pending/1-Accept/2-Complete
        var orderStatus = ""
        var apiOrderStatus = response.result!!.get(0).orderStatus
        if (apiOrderStatus==0){
            orderStatus = "Pending"
            binding.tvOrderStatus.backgroundTintList  = getResources().getColorStateList(R.color.grey,theme)
        } else if (apiOrderStatus==1){
            orderStatus = "Accepted"
            binding.tvOrderStatus.backgroundTintList  = getResources().getColorStateList(R.color.light_green,theme)
        } else if (apiOrderStatus==2){
            orderStatus = "Completed"
            binding.tvOrderStatus.backgroundTintList  = getResources().getColorStateList(R.color.dark_green,theme)
        }else if (apiOrderStatus==3){
            orderStatus = "Rejected"
            binding.tvOrderStatus.backgroundTintList  = getResources().getColorStateList(R.color.red,theme)
        }else if (apiOrderStatus==4){
            orderStatus = "Expired"
            binding.tvOrderStatus.backgroundTintList  = getResources().getColorStateList(R.color.orange,theme)
        }

        binding.tvOrderStatus.text= orderStatus

        binding.txtBookingNo.text="Booking Id : "+response.result!!.get(0).bookingIid
        binding.tvPrice.text="$"+response.result!!.get(0).serviceData!!.get(0).servicePrice
        serviceData=response!!.result!!.get(0).serviceData
        var adpt = BookingDetailsAdapter(this@BookingDetailsActivity,serviceData)
        binding.rcBookingdetails.layoutManager = LinearLayoutManager(this)
        binding.rcBookingdetails.adapter = adpt
    }

    override fun onFailure(msg: String) {

    }

    override fun showProgress() {

    }

    override fun dismissProgress() {

    }
}