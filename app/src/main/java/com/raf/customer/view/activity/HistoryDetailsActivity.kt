package com.raf.customer.view.activity

import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.R
import com.raf.customer.adapter.HistoryDetailsAdapter
import com.raf.customer.adapter.ReviewAdapter
import kotlinx.android.synthetic.main.activity_history_details.*
import kotlinx.android.synthetic.main.topbar_view.*

class HistoryDetailsActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_history_details)
        window.statusBarColor = getColor(R.color.app_header)
        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText("History Detail")
        var adpt=HistoryDetailsAdapter(this@HistoryDetailsActivity)
        rc_historydetails.layoutManager=LinearLayoutManager(this)
        rc_historydetails.adapter=adpt

        var adpt1=ReviewAdapter(this)
        rc_reviewdetails.layoutManager=LinearLayoutManager(this)
        rc_reviewdetails.adapter=adpt1
    }
}