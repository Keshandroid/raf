package com.raf.customer.view.activity

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import com.raf.R

class FirstPageActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_first_page)
        val btn_barber = findViewById<TextView>(R.id.btn_barber)
        val btn_restaurant = findViewById<TextView>(R.id.btn_restaurant)
        val btn_beautician = findViewById<TextView>(R.id.btn_beautician)
        btn_barber.setOnClickListener(View.OnClickListener {
            startActivity(
                Intent(this@FirstPageActivity, HomeActivity::class.java)
            )
        })
        btn_restaurant.setOnClickListener(View.OnClickListener {
            startActivity(
                Intent(this@FirstPageActivity, ComingSoonActivity::class.java)
            )
        })
        btn_beautician.setOnClickListener(View.OnClickListener {
            startActivity(
                Intent(this@FirstPageActivity, ComingSoonActivity::class.java)
            )
        })
    }
}