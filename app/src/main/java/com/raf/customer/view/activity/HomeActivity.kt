package com.raf.customer.view.activity

import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.provider.Settings
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.google.zxing.integration.android.IntentIntegrator
import com.raf.R
import com.raf.customer.controller.ForgotPasswordController
import com.raf.customer.controller.GetCategoryController
import com.raf.customer.controller.RegisterDeviceController
import com.raf.customer.controller.listener.IforgotPasswordController
import com.raf.customer.controller.listener.IregisterDeviceController
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.retrofit.response.RegisterDeviceResponse
import com.raf.customer.service.LocalStorage
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.fragment.customer.AccountFragment
import com.raf.customer.view.fragment.customer.BookingFragment
import com.raf.customer.view.fragment.customer.HomeFragment
import com.raf.customer.view.viewInterface.IGetCategoryView
import com.raf.customer.view.viewInterface.IRegisterDeviceView
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_home.*


class HomeActivity : AppCompatActivity(), IGetCategoryView,IRegisterDeviceView {
    private lateinit var appPreference: AppPreference
    lateinit var iGetCategoryController: GetCategoryController

    var uid = ""
    var utoken = ""
    var SERVICE_PRO_ID = ""
    var userEmail = ""

    lateinit var  iregisterDeviceController: IregisterDeviceController
    lateinit var localStorage: LocalStorage


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)

        /*  preferences = getSharedPreferences("Raf", MODE_PRIVATE)
          editor = preferences.edit()
          uid = preferences.getString("uid", "")!!
          utoken = preferences.getString("utoken", "")!!*/
        appPreference = AppPreference(this)
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)
        userEmail = appPreference.getString(AppConstant.EMAIL)

        // SERVICE_PRO_ID = appPreference.getString(AppConstant.SERVICE_PRO_ID)


        val idStr = intent.getStringExtra("id") ?: ""
        val fromStr = intent.getStringExtra("from") ?: ""
        iGetCategoryController = GetCategoryController(this, this)

        /*   if (!SERVICE_PRO_ID.isEmpty()) {
               iGetCategoryController.callGetcategotyApi(uid, utoken, SERVICE_PRO_ID)
           }
   */
        val adpt = ViewPagerAdapter(supportFragmentManager, tab_layout.tabCount, idStr)
        viewpager.adapter = adpt
        viewpager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tab_layout))
        tab_layout.tabRippleColor = null
        tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    if (tab!!.position == 1 || tab!!.position == 2) {
                        window.statusBarColor = getColor(R.color.app_header)
                    } else {
                        window.statusBarColor = Color.parseColor("#B9B9BA")

                    }
                }

                viewpager.currentItem = tab!!.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }
        })

        adpt

        //Register Android Device
        registerDevice();

    }

    private fun registerDevice(){
        localStorage = LocalStorage(this)

        var deviceId = Settings.Secure.getString(this.getContentResolver(), Settings.Secure.ANDROID_ID)

        iregisterDeviceController = RegisterDeviceController(this,this)
        iregisterDeviceController.callRegisterDeviceApi(localStorage.getToken().toString(),uid,userEmail,deviceId)
    }

    override fun onResume() {
        super.onResume()
//        var appPreference = AppPreference(this)
//        appPreference.setString(AppConstant.SUBCAT_ID, "")
    }

    class ViewPagerAdapter(fm: FragmentManager, var tabCount: Int, val idStr: String) :
        FragmentPagerAdapter(fm) {
        override fun getCount(): Int {

            return tabCount
        }

        override fun getItem(position: Int): Fragment {
            return when (position) {
                0 -> {
                    HomeFragment().getInstance(idStr)
                }
                1 -> {
                    BookingFragment()
                }
                2 -> {
                    AccountFragment()
                }
                else -> {
                    getItem(position)
                }
            }
        }
    }

    override fun onGetCategorySuccess(response: GetCategoryResponse, message: String) {
        if (message == "Category_Got") {
            startActivity(
                Intent(this@HomeActivity, HomeActivity::class.java)
            )
            finish()

        }

    }

    override fun onRegisterDeviceSuccess(response: RegisterDeviceResponse, message: String) {

    }


    override fun onFailure(msg: String) {

    }

    override fun showProgress() {

    }

    override fun dismissProgress() {

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (result != null && resultCode == RESULT_OK) {
            /*//sercivePro,1
              val str=""
            str.replace("sercivePro,","")
            */
            //Utils.printLog(TAG, "onActivityResult:"+result.contents)
            Utils.printLog(HomeActivity::class.java.simpleName,
                "onActivityResult: " + result.contents.substringAfter(","))

            if (result.contents != null) {
                var id = result.contents.substringAfter(",")
                appPreference.setString(AppConstant.PKey_LAST_SCANNED_ID,id)
                iGetCategoryController.callGetcategotyApi(uid, utoken, id)
                var appPreference = AppPreference(this)
                appPreference.setString(AppConstant.SERVICE_PRO_ID, id).toString()

                var list = ArrayList<String>()
                //get saved list from preference
                list.addAll(appPreference.getStringList(AppConstant.PKey_SCANNED_QR_LIST))
                //if list is empty or no scanned id exist then add i
                if (list.isNullOrEmpty() || !list.contains(id)) {
                    list.add(id)
                    appPreference.setStringList(AppConstant.PKey_SCANNED_QR_LIST, list)
                }

                /* editor.putString(AppConstant.SERVICE_PRO_ID, id)
                 editor.commit()*/
            }
        }
    }

}