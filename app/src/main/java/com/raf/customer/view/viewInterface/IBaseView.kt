package com.raf.customer.view.viewInterface

interface IBaseView {
    fun onFailure(msg:String)
    fun showProgress()
    fun dismissProgress()
}