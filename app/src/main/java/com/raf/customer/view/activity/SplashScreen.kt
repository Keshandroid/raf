package com.raf.customer.view.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.raf.R
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.employee.view.activity.EmployeeHomeActivity

class SplashScreen : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)


        Handler().postDelayed(
            Runnable {
                var appPreference = AppPreference(this)
                var isLogin: Boolean = appPreference.getBoolean(AppConstant.ISLOGIN)
                if (isLogin) {
                    var strUserType = appPreference.getString(AppConstant.USER_TYPE)
                    if (strUserType.equals(AppConstant.CUSTOMER)) {
                        startActivity(Intent(this, HomeActivity::class.java))
                    } else {
                        startActivity(Intent(this, EmployeeHomeActivity::class.java))
                    }
                    finish()
                } else {
                    startActivity(Intent(this@SplashScreen, MainActivity::class.java))
                    finish()
                }


            }, 3000
        )
    }

    fun restartActivity(cls: Class<*>?) {
        finish()
        val intent = Intent(this, cls)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK)
        startActivity(intent)
    }
}