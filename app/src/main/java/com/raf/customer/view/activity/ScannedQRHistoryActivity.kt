package com.raf.customer.view.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import com.raf.BaseActivity
import com.raf.customer.adapter.ScannedQRListAdapter
import com.raf.customer.controller.GetCategoryController
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IGetCategoryView
import com.raf.databinding.ActivityScannedQrhistoryBinding

class ScannedQRHistoryActivity : BaseActivity(), ScannedQRListAdapter.IClickLisetener,
    IGetCategoryView {

    private lateinit var binding: ActivityScannedQrhistoryBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityScannedQrhistoryBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.toolbar.txtHeading.text = " Scanned QR History"
        binding.toolbar.back.setOnClickListener { finish() }

        var appPreference = AppPreference(this)

        var list = ArrayList<String>()
        //get saved QR list from preference
        if (!appPreference.getStringList(AppConstant.PKey_SCANNED_QR_LIST).isNullOrEmpty())
            list.addAll(appPreference.getStringList(AppConstant.PKey_SCANNED_QR_LIST))

        var adapter = ScannedQRListAdapter(this, list, this)
        binding.rcvScannedQR.adapter = adapter

        if (list.isNullOrEmpty()) {
            binding.tvEmptyView.visibility = View.VISIBLE
            binding.rcvScannedQR.visibility = View.GONE
        } else {
            binding.tvEmptyView.visibility = View.GONE
            binding.rcvScannedQR.visibility = View.VISIBLE
        }
    }

    override fun onIdClicked(qrId: String) {

        var appPreference = AppPreference(this)
        var uid = appPreference.getString(AppConstant.UID)
        var utoken = appPreference.getString(AppConstant.UTOKEN)
        appPreference.setString(AppConstant.PKey_LAST_SCANNED_ID,qrId)
        var controller = GetCategoryController(this@ScannedQRHistoryActivity, this)
        controller.callGetcategotyApi(uid, utoken, qrId)
        appPreference.setString(AppConstant.SERVICE_PRO_ID, qrId).toString()
    }

    override fun onGetCategorySuccess(response: GetCategoryResponse, message: String) {
        if (message.equals("Category_Got")) {
            var intent = Intent(this@ScannedQRHistoryActivity, HomeActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
            startActivity(intent)
            finish()
        }

    }

    override fun onFailure(msg: String) {
        showToast(msg)
    }
}