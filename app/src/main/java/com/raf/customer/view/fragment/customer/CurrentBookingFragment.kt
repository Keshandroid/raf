package com.raf.customer.view.fragment.customer

import android.app.AlertDialog
import android.app.Dialog
import android.content.SharedPreferences
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.R
import com.raf.customer.adapter.CurrentBookingAdapter
import com.raf.customer.controller.GetCurrentBookingController
import com.raf.customer.controller.listener.IGetCurrentBookingController
import com.raf.customer.retrofit.response.GetCurrentBookingResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetCurrentBookingView
import kotlinx.android.synthetic.main.current_booking_fragment.*

class CurrentBookingFragment() : Fragment(R.layout.current_booking_fragment),
    IGetCurrentBookingView, CurrentBookingAdapter.IClickListener {

    var controller: IGetCurrentBookingController? = null;
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var uid = ""
    var utoken = ""
    private  var progress: Dialog?=null


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        /*preferences = requireActivity().getSharedPreferences("Raf", AppCompatActivity.MODE_PRIVATE)
        editor = preferences.edit()
        uid = preferences.getString("uid", "")!!
        utoken = preferences.getString("utoken", "")!!*/
        var appPreference = AppPreference(this.requireContext())
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)

        controller = GetCurrentBookingController(this.requireActivity(), this)
        controller!!.callGetCurrentBookingApi(uid, utoken, "0")

    }

    private fun showCancelConfirmationDialog(bookingId: String){
        val builder = AlertDialog.Builder(activity)

        //set message for alert dialog
        builder.setMessage("Are you sure to cancel booking?")
        builder.setIcon(android.R.drawable.ic_dialog_alert)

        //performing positive action
        builder.setPositiveButton("Yes") { dialogInterface, which ->
            dialogInterface.dismiss()
            controller!!.callCancelBookingAPI(uid, utoken, bookingId)
        }

        //performing negative action
        builder.setNegativeButton("No") { dialogInterface, which ->
            dialogInterface.dismiss()
        }
        // Create the AlertDialog
        val alertDialog: AlertDialog = builder.create()
        // Set other dialog properties
        alertDialog.setCancelable(false)
        alertDialog.show()
    }

    override fun onIGetBookingSuccess(response: GetCurrentBookingResponse, message: String) {

        if(message == "Provider_Got"){
            if (response.result!!.size > 0) {
                rc_currentbooking.visibility = View.VISIBLE
                tv_not.visibility = View.GONE
                var adpt = CurrentBookingAdapter(activity, response,this)
                rc_currentbooking.layoutManager = LinearLayoutManager(activity)
                rc_currentbooking.adapter = adpt
            }else{
                rc_currentbooking.visibility = View.GONE
                tv_not.visibility = View.VISIBLE
            }
        }else{
            rc_currentbooking.visibility = View.GONE
            tv_not.visibility = View.VISIBLE
        }


    }

    override fun onCacelBookingSuccess(message: String) {
        Utils.showToast(requireActivity(),message)
        controller!!.callGetCurrentBookingApi(uid, utoken, "0")
    }

    override fun onCacelBookingFailed(message: String) {
        Utils.showToast(requireActivity(),message)
    }

    override fun onFailure(msg: String) {
        Log.d("onFailure"," current booking : " +msg)
    }

    override fun showProgress() {
        progress = Dialog(requireActivity())
        progress!!.setContentView(R.layout.dialog_layout)
        progress!!.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        progress!!.setCancelable(false)
        progress!!.show()
    }

    override fun dismissProgress() {
        progress!!.dismiss()
    }

    override fun onCancelClicked(bookingId: String) {
        showCancelConfirmationDialog(bookingId)
    }

}