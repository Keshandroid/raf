package com.raf.customer.view.fragment.customer

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.google.android.material.tabs.TabLayout
import com.raf.R
import kotlinx.android.synthetic.main.fragment_booking.*
import kotlinx.android.synthetic.main.topbar_view2.*

class BookingFragment():Fragment(R.layout.fragment_booking) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        txt_heading2.setText("Area Prenotazioni")
        viewpager_booking.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(booking_tablayout))
        var adpt= ViewpagerAdapter(childFragmentManager,booking_tablayout.tabCount)
        booking_tablayout.tabRippleColor=null
        booking_tablayout.addOnTabSelectedListener(object :TabLayout.OnTabSelectedListener{
            override fun onTabSelected(tab: TabLayout.Tab?) {
                    viewpager_booking.currentItem=tab!!.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabReselected(tab: TabLayout.Tab?) {

            }
        })
        viewpager_booking.adapter=adpt

    }

    class ViewpagerAdapter(fm: FragmentManager, var tabCount: Int):FragmentPagerAdapter(fm) {
        override fun getCount(): Int {
            return tabCount
        }

        override fun getItem(position: Int): Fragment {
            return when(position)
            {
                0->{
                    CurrentBookingFragment()
                }
                1->{
                    CompletedBookingFragment()
                }
                else->{
                    getItem(position)
                }
            }
        }
    }
}