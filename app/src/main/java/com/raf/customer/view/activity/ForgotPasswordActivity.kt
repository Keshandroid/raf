package com.raf.customer.view.activity

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.ForgotPasswordController
import com.raf.customer.controller.listener.IforgotPasswordController
import com.raf.customer.retrofit.response.ForgotPasswordResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IForgotPassWordView
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.topbar_view.*

class ForgotPasswordActivity :BaseActivity(),IForgotPassWordView {
    lateinit var  iforgotPasswordController: IforgotPasswordController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        window.statusBarColor = getColor(R.color.app_header)
        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText(getString(R.string.forgot_password))

        iforgotPasswordController=ForgotPasswordController(this,this)
        btn_forgot.setOnClickListener(View.OnClickListener {
            if(et_forgot_email.text.toString()=="")
            {
                Utils.showToast(this,"Enter Email")
            }
            else
            {

                    iforgotPasswordController.callForgotPasswordApi(et_forgot_email.text.toString())
            }
        })

    }


    override fun onForgotPasswordSuccess(response: ForgotPasswordResponse, message: String) {

    }

    override fun onFailure(msg: String) {

    }

    override fun showProgress() {
        super.showProgress()

    }

    override fun dismissProgress() {
        super.dismissProgress()
    }
}