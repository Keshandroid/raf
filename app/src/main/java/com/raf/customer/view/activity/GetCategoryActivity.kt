package com.raf.customer.view.activity

import android.content.ContentValues.TAG
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import com.google.zxing.integration.android.IntentIntegrator
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.GetCategoryController
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetCategoryView
import kotlinx.android.synthetic.main.activity_get_category.*

class GetCategoryActivity : BaseActivity(), IGetCategoryView {
    private lateinit var appPreference: AppPreference
    lateinit var iGetCategoryController: GetCategoryController

    var uid = ""
    var utoken = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_get_category)
        /*   preferences = getSharedPreferences("Raf", MODE_PRIVATE)
           editor = preferences.edit()
           uid = preferences.getString("uid", "")!!
           utoken = preferences.getString("utoken", "")!!*/
        appPreference = AppPreference(this)
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)

        iGetCategoryController = GetCategoryController(this, this)
        bt_scan.setOnClickListener(View.OnClickListener {
            val intentIntegrator = IntentIntegrator(this)
            intentIntegrator.setCameraId(0)
            intentIntegrator.setOrientationLocked(true)
            intentIntegrator.setPrompt("Scan Qr code")
            intentIntegrator.initiateScan()

            /*iGetCategoryController.callGetcategotyApi(uid, utoken, "1")
            editor.putString(AppConstant.SERVICE_PRO_ID, "1")
            editor.commit()*/
        })

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        if (result != null) {
            /*//sercivePro,1
              val str=""
            str.replace("sercivePro,","")
            */
            //Utils.printLog(TAG, "onActivityResult:"+result.contents)
            Utils.printLog(TAG, "onActivityResult: " + result.contents.substringAfter(","))

            var id = result.contents.substringAfter(",")
            appPreference.setString(AppConstant.PKey_LAST_SCANNED_ID,id)
            iGetCategoryController.callGetcategotyApi(uid, utoken, id)
            var appPreference = AppPreference(this)
            appPreference.setString(AppConstant.SERVICE_PRO_ID, id).toString()

            /*
             editor.putString(AppConstant.SERVICE_PRO_ID, id)
             editor.commit()*/

        }

    }

    override fun onGetCategorySuccess(response: GetCategoryResponse, message: String) {
        if (message == "Category_Got") {
            startActivity(
                Intent(this@GetCategoryActivity, HomeActivity::class.java)
            )

        }

    }

    override fun onFailure(msg: String) {

    }

}