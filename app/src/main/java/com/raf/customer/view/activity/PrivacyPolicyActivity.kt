package com.raf.customer.view.activity

import android.graphics.Color
import android.os.Build
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import com.raf.R
import kotlinx.android.synthetic.main.topbar_view.*

class PrivacyPolicyActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_privacy_policy)

        window.statusBarColor = getColor(R.color.app_header)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText("Privacy Policy")
    }
}