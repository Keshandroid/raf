package com.raf.customer.view.viewInterface

import com.raf.customer.retrofit.response.*
import com.raf.customer.retrofit.response.employee.*

interface IChangePasswordView : IBaseView {
    fun onChangePasswordSuccess(response: ChangePasswordResponse, message: String)
}

interface IForgotPassWordView : IBaseView {
    fun onForgotPasswordSuccess(response: ForgotPasswordResponse, message: String)
}

interface IRegisterDeviceView : IBaseView {
    fun onRegisterDeviceSuccess(response: RegisterDeviceResponse, message: String)
}

interface IVerifyEmailView : IBaseView {
    fun onVerifyEmailSuccess(response: VerifyEmailResponse, message: String)
}

interface ILoginView : IBaseView {
    fun onLoginSuccess(response: LoginResponse, message: String)
    fun onSocialLoginSuccess(response: SocialLoginResponse, message: String)
}

interface IRegisterView : IBaseView {
    fun onRegisterSuccess(response: RegisterResponse, message: String)
}

interface IGetProfileView :  IBaseView {
    fun onGetProfileSuccess(response: GetProfileResponse, message: String)
}

interface IGetTimeSlotsView :  IBaseView {
    fun onGetTimeSlotSuccess(response: GetTimeSlotsResponse, message: String)
}

interface IUpdateProfileView : IBaseView {
    fun onUpdateProfileSucsess(response: UpdateProfileResponse, message: String)
}

interface IGetCategoryView : IBaseView {
    fun onGetCategorySuccess(response: GetCategoryResponse, message: String)
}

interface IGetEmpServiceView : IBaseView {
    fun onGetEmpServiceSuccess(response: GetEmpServiceResponse, message: String)
}

interface IGetEmpSubCategoryView : IBaseView {
    fun onGetEmpCategorySuccess(response: GetEmpSubCategoryResponse, message: String)
}

interface IGetSubCategoryView : IBaseView {
    fun onGetSubCategorySuccess(response: GetSubCategoryResponse, message: String)
}

interface IGetServiceView : IBaseView {
    fun onGetServiceSuccess(response: GetServiceResponse, message: String)
}

interface IRatingView : IBaseView {
    fun onRatingSuccess(response: RatingResponse, message: String)
}

interface IStaticPageView : IBaseView {
    fun onIStaticPageSuccess(response: StaticPageResponse, message: String)
}

interface IGetServiceProviderView : IBaseView {
    fun onIGetServiceSuccess(response: GetServiceProviderResponse, message: String)
}

interface IGetCurrentBookingView : IBaseView {
    fun onIGetBookingSuccess(response: GetCurrentBookingResponse, message: String)
    fun onCacelBookingSuccess(message: String)
    fun onCacelBookingFailed(message: String)
}

interface IGetCompletedBookingView : IBaseView {
    fun onIGetBookingSuccess(response: GetCurrentBookingResponse, message: String)
}

interface IGetBookingDetailView : IBaseView {
    fun onIGetBookingDetailSuccess(response: GetBookingDetailResponse, message: String)
}

interface IGetBookingServiceListView : IBaseView {
    fun onIGetBookingServiceListSuccess(response: GetBookingServiceListResponse?, message: String)

}

interface IAddBookingView : IBaseView {
    fun onIAddBookingSuccess(response: AddBookingResponse, message: String)
}


// EMPLOYEE Views

interface IEmployeeLoginView : IBaseView {
    fun onLoginSuccess(response: EmployeeLoginResponse, message: String)
}

interface IEmployeeForgotPassWordView : IBaseView {
    fun onForgotPasswordSuccess(response: ForgotPasswordResponse, message: String)
}

interface IEmployeeChangePasswordView : IBaseView {
    fun onChangePasswordSuccess(response: ChangePasswordResponse, message: String)
}
interface IEmployeeGetProfileView :  IBaseView {
    fun onGetProfileSuccess(response: GetProfileResponse, message: String)
}
interface IEmployeeUpdateProfileView : IBaseView {
    fun onUpdateProfileSucsess(response: UpdateProfileResponse, message: String)
}
interface IEmployeeAddCategoryView : IBaseView {
    fun onCategoryAddSuccess(response: ForgotPasswordResponse, message: String)
    fun onCategoryEditSuccess(response: SuccessResponse, message: String)
}
interface IEmployeeOrderView : IBaseView {
    fun onEmployeeCurrentOrderSuccess(response: EmployeeOrderResponse, message: String)
    fun onEmployeeCompleteOrderSuccess(response: EmployeeOrderResponse, message: String)
    fun onEmployeeOrderStatusSuccess(response: SuccessResponse, message: String)
}
interface IEmployeeSubAddCategoryView : IBaseView {
    fun onSubCategoryAddSuccess(response: SuccessResponse, message: String)
}
interface IEmployeeAddServiceView : IBaseView {
    fun onAddServiceSuccess(response: SuccessResponse, message: String)
}