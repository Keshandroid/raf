package com.raf.customer.view.activity

import android.content.ContentValues.TAG
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RatingBar
import android.widget.Toast
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.RatingController
import com.raf.customer.controller.listener.IRatingController
import com.raf.customer.retrofit.response.RatingResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IRatingView
import kotlinx.android.synthetic.main.activity_rating.*
import kotlinx.android.synthetic.main.topbar_view.*

class RatingActivity : BaseActivity(), IRatingView, RatingBar.OnRatingBarChangeListener {
    lateinit var iRatingController: IRatingController

    private var uid = ""
    private var utoken = ""
    private var service_id = ""
    private var review = ""
    var rating = 0.0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rating)
        /* preferences = getSharedPreferences("Raf", MODE_PRIVATE)
         uid = preferences.getString("uid", "")!!
         utoken = preferences.getString("utoken", "")!!
         service_id = preferences.getString(AppConstant.SERVICE_PRO_ID, "")!!*/
        var appPreference = AppPreference(this)
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)
        service_id = appPreference.getString(AppConstant.SERVICE_PRO_ID)

        window.statusBarColor = getColor(R.color.app_header)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText("Rating")
        val ratings = findViewById<RatingBar>(R.id.ratings)
        ratings.onRatingBarChangeListener = this


        btn_submit.setOnClickListener(View.OnClickListener {

            review = et_comment.text.toString()

            Utils.printLog(TAG, "onCreate:rate" + rating)
            Utils.printLog(TAG, "onCreate:review" + review)
            if (rating != 0.0f && review != "") {
                iRatingController = RatingController(this, this)
                iRatingController.callRatingApi(uid, utoken, service_id, rating, review)
            } else if (rating == 0.0f) {
                Toast.makeText(applicationContext, "Please add rating", Toast.LENGTH_SHORT).show()
            } else if (review == "") {
                Toast.makeText(applicationContext, "Please write comment", Toast.LENGTH_SHORT)
                    .show()
            }

        })
    }

    override fun onRatingChanged(p0: RatingBar?, p1: Float, p2: Boolean) {

        rating = p1
    }

    override fun onRatingSuccess(response: RatingResponse, message: String) {

        if (message == "Add_review_success") {
            startActivity(Intent(this, ThankYouActivity::class.java))
        }
    }

    override fun onFailure(msg: String) {

    }
}