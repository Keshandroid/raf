package com.raf.customer.view.activity

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Color
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.util.Patterns
import android.view.View
import android.widget.LinearLayout
import android.widget.RadioButton
import androidx.core.app.ActivityCompat
import com.google.gson.GsonBuilder
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.RegisterController
import com.raf.customer.controller.listener.IRegisterController
import com.raf.customer.retrofit.response.RegisterResponse
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IRegisterView
import com.ramo.utils.ImageFilePath
import kotlinx.android.synthetic.main.activity_register_activtiy.*
import kotlinx.android.synthetic.main.topbar_view.*
import java.io.ByteArrayOutputStream

class RegisterActivtiy : BaseActivity(), IRegisterView {
    //    lateinit var sharedPreferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor

    var imageuri: Uri? = null
    var gender: String? = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_activtiy)
        window.statusBarColor = getColor(R.color.app_header)

        /*  sharedPreferences = getSharedPreferences("Raf", MODE_PRIVATE)
          editor = sharedPreferences.edit()*/

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        btn_register.setOnClickListener(View.OnClickListener {
            if (setValidation()) {

                var fnm = et_firstname.text.toString()
                var lnm = et_lastname.text.toString()
                var pass = et_password.text.toString()
                var email = et_email.text.toString()
                var phoneno = et_phone.text.toString()

                var iRegisterController: IRegisterController = RegisterController(this, this)
                iRegisterController.callRegisterApi(
                    fnm,
                    lnm,
                    pass,
                    email,
                    phoneno,
                    gender!!,
                    ImageFilePath.getPath(this, imageuri!!) ?: ""
                )


            }
        })



        img_register.setOnClickListener(View.OnClickListener {
            if (checkpermission()) {
                selectProfilePic()
            }
        })

    }

    private fun selectProfilePic() {
        var choosedialog = Dialog(this)
        choosedialog.setCancelable(true)
        var layoutInflater = getLayoutInflater()
        var view = layoutInflater.inflate(R.layout.choose_profile_pic, null)
        var capturephoto: LinearLayout = view.findViewById(R.id.linear_capture_photo)
        var fromgallery: LinearLayout = view.findViewById(R.id.linear_take_from_gallary)
        choosedialog.setContentView(view)
        choosedialog.show()

        capturephoto.setOnClickListener(View.OnClickListener {
            captureFromCamera()
            choosedialog.dismiss()
        })

        fromgallery.setOnClickListener(View.OnClickListener {
            selectFromgallery()
            choosedialog.dismiss()
        })

    }

    private fun selectFromgallery() {
        val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
        startActivityForResult(intent, 1)
    }

    private fun captureFromCamera() {
        val intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        startActivityForResult(intent, 2)

    }


    fun setValidation(): Boolean {
        if (et_firstname.text.toString() == "") {
            Utils.showToast(this@RegisterActivtiy, "Please enter Firstname")
            return false
        } else if (et_firstname.length() < 3) {
            Utils.showToast(this@RegisterActivtiy, "FirstName was to short")
            return false
        } else if (et_lastname.text.toString() == "") {
            Utils.showToast(this@RegisterActivtiy, "Please enter Lastname")
            return false
        } else if (et_lastname.length() < 3) {
            Utils.showToast(this@RegisterActivtiy, "LastName was to short")
            return false
        } else if (!Patterns.EMAIL_ADDRESS.matcher(et_email.text.toString()).matches()) {
            Utils.showToast(this@RegisterActivtiy, "Enter valid Email")
            return false
        } else if (et_password.length() < 6) {
            Utils.showToast(this@RegisterActivtiy, "Password should be 6 character or more")
            return false
        } else if (et_phone.text.toString() == "") {
            Utils.showToast(this@RegisterActivtiy, "Enter Number")
            return false
        } else if (imageuri == null) {
            Utils.showToast(this@RegisterActivtiy, "Please Select Image")
            return false
        }

        val id = radiogrp.checkedRadioButtonId
        if (id == -1) {


            Utils.showToast(this, "Select Gender")
            return false
        } else {
            var radioButton: RadioButton = findViewById(id)
            gender = radioButton.text.toString()
            return true
        }

    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            1 -> {
                if (resultCode == RESULT_OK) {

                    imageuri = data!!.data
                    Utils.printLog("taken image", "onActivityResult:" + imageuri)
                    ImageFilePath.getPath(this, imageuri!!)
                    Utils.printLog("texssss", "onActivityResult: " + ImageFilePath.getPath(this, imageuri!!))
                    img_register.setImageURI(imageuri)
                }
            }
            2 -> {
                if (resultCode == RESULT_OK) {
                    var bundle: Bundle? = data!!.extras
                    var bitmap: Bitmap = bundle!!.get("data") as Bitmap
                    val byte = ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byte)
                    val path =
                        MediaStore.Images.Media.insertImage(this.contentResolver, bitmap, "", null)
                    imageuri = Uri.parse(path)
                    ImageFilePath.getPath(this, imageuri!!)
                    Utils.printLog("camera", "onActivityResult: " + ImageFilePath.getPath(this, imageuri!!))
                    img_register.setImageURI(imageuri)

                }
            }

        }
    }


    fun checkpermission(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.READ_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                android.Manifest.permission.CAMERA
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        } else {
            ActivityCompat.requestPermissions(
                this, arrayOf(
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    android.Manifest.permission.READ_EXTERNAL_STORAGE,
                    android.Manifest.permission.CAMERA
                ), 0
            )
            return false

        }
    }


    override fun onRegisterSuccess(response: RegisterResponse, message: String) {

        if (message == "Register_Success") {
            /*  editor.putBoolean("islogin",true)
              editor.putString("uid", response.result!!.uid.toString())
              editor.putString("utoken", response.result!!.utoken)
              editor.putString("firsname", response.result!!.firstname)
              editor.putString("lastname", response.result!!.lastName)
              editor.putString("email", response.result!!.email)
              editor.putString("phon_no", response.result!!.contact_no)
              editor.putString("gender", response.result!!.gender)
              editor.putString("pic", response.result!!.profile_pic)
              editor.commit()
  */
            /*startActivity(Intent(this, HomeActivity::class.java))
            finish()*/

                Log.d("registerResponse",GsonBuilder().setPrettyPrinting().create().toJson(response))

            startActivity(Intent(this, OTPEmailActivity::class.java)
                .putExtra("emailOTP",""+response.getResult().emailCode)
                .putExtra("email",""+response.getResult().email)
            )
            finish()



        }
    }

    override fun onFailure(msg: String) {

    }

    override fun showProgress() {
        super.showProgress()
    }

    override fun dismissProgress() {
        super.dismissProgress()
    }
}