package com.raf.customer.view.activity

import android.content.SharedPreferences
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.ChangePasswordController
import com.raf.customer.controller.listener.IChangePasswordController
import com.raf.customer.retrofit.response.ChangePasswordResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.CustomPassword
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IChangePasswordView
import kotlinx.android.synthetic.main.activity_change_password.*
import kotlinx.android.synthetic.main.topbar_view.*

class ChangePasswordActivity : BaseActivity(), IChangePasswordView {
    lateinit var sharedPreferences: SharedPreferences
    lateinit var iChangePasswordListener: IChangePasswordController


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        window.statusBarColor = getColor(R.color.app_header)



        iChangePasswordListener = ChangePasswordController(this, this)
        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText("Change Password")
        sharedPreferences = getSharedPreferences("Raf", MODE_PRIVATE)

        CustomPassword(et_old_pass, img_old_seen)
        CustomPassword(et_new_pass, img_new_seen)
        CustomPassword(et_confirm_pass, img_confim_seen)

        btn_change_pass.setOnClickListener(View.OnClickListener {
            if (setValidation()) {
                /*var uid = sharedPreferences.getString("uid", "")
                var utoken = sharedPreferences.getString("utoken", "") */
                var appPreference = AppPreference(this)
                var uid =  appPreference.getString(AppConstant.UID)
                var utoken =  appPreference.getString(AppConstant.UTOKEN)


                var oldpass = et_old_pass.text.toString()
                var newpass = et_new_pass.text.toString()
                    iChangePasswordListener.callChangePasswordApi(uid!!, utoken!!, oldpass, newpass)
            }
        })

    }

    fun setValidation(): Boolean {
        if (et_old_pass.text.toString() == "" && et_new_pass.text.toString() == "") {
            Utils.showToast(this, "Please fill blank fields")
            return false
        } else if (et_old_pass.text.length < 6 && et_new_pass.text.length < 6) {
            Utils.showToast(this, "Password is to short")
            return false
        } else if (et_new_pass.text.toString() != et_confirm_pass.text.toString()) {
            Utils.showToast(this, "Password not match")
            return false
        }

        return true
    }


    override fun onChangePasswordSuccess(response: ChangePasswordResponse, message: String) {

    }

    override fun onFailure(msg: String) {

    }



}


