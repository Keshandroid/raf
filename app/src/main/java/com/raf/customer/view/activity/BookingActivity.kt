package com.raf.customer.view.activity

import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.ContentValues.TAG
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.R
import com.raf.customer.adapter.BookingAdapter
import com.raf.customer.controller.AddBookingController
import com.raf.customer.controller.GetBookingServiceListController
import com.raf.customer.controller.GetTimeSlotsController
import com.raf.customer.controller.listener.IAddBookingController
import com.raf.customer.controller.listener.IGetBookingServiceListController
import com.raf.customer.retrofit.response.AddBookingResponse
import com.raf.customer.retrofit.response.GetBookingServiceListResponse
import com.raf.customer.retrofit.response.GetTimeSlotsResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IAddBookingView
import com.raf.customer.view.viewInterface.IGetBookingServiceListView
import com.raf.customer.view.viewInterface.IGetTimeSlotsView
import com.raf.databinding.ActivityBookingBinding
import kotlinx.android.synthetic.main.topbar_view.*
import java.text.SimpleDateFormat
import java.util.*


class BookingActivity : AppCompatActivity(), IAddBookingView, BookingAdapter.onItemMyClickListener,
    IGetTimeSlotsView,
    IGetBookingServiceListView {
    private lateinit var binding: ActivityBookingBinding
    lateinit var calendar: Calendar
    lateinit var controller: IAddBookingController
    lateinit var controller1: IGetBookingServiceListController
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    lateinit var tv_tot: TextView
    var uid = ""
    var utoken = ""
    var adpt: BookingAdapter? = null
    private var service_pro_id = ""
    private var res: GetBookingServiceListResponse.Result? = null
    var total: String = ""
    var serviceId: String = ""
    var selected_service_id: String = ""
    var service_duration: String = ""

    lateinit var iGetTimeSlotsController: GetTimeSlotsController



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBookingBinding.inflate(layoutInflater)
        setContentView(binding.root)

        ArrayAdapter.createFromResource(
            this,
            R.array.date_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.dateSpinner.adapter = adapter
        }


        //Time spinner from static values
        /*ArrayAdapter.createFromResource(
            this,
            R.array.time_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.timeSpinner.adapter = adapter
        }*/



        window.statusBarColor = getColor(R.color.app_header)

        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        txt_heading.setText("Booking")
        /*  rc_booking.layoutManager = LinearLayoutManager(this)
          adpt = BookingAdapter(this, res)
          rc_booking.adapter = adpt*/

        calendar = Calendar.getInstance()

        var year = calendar.get(Calendar.YEAR)
        var month = calendar.get(Calendar.MONTH)
        var day = calendar.get(Calendar.DAY_OF_MONTH)
        calendar.set(year, month, (day)) //(day+1) for next day

        var datePicker = DatePickerDialog.OnDateSetListener { picker, Year, Month, Day ->
            calendar.set(Calendar.YEAR, Year)
            calendar.set(Calendar.MONTH, Month)
            calendar.set(Calendar.DAY_OF_MONTH, Day)
            setDate()
        }


        var timePicker = TimePickerDialog.OnTimeSetListener { timePicker, HOUR, Minute ->
            calendar.set(Calendar.HOUR, HOUR)
            calendar.set(Calendar.MINUTE, Minute)
            binding.etTime.setText(HOUR.toString() + ":" + Minute.toString())
            Utils.printLog(TAG, "onCreate:" + HOUR + ":" + Minute)
        }


        binding.etDate.setOnClickListener(View.OnClickListener {
            /* DatePickerDialog(
                  this,
                  datePicker,
                  calendar.get(Calendar.YEAR),
                  calendar.get(Calendar.MONTH),
                  calendar.get(Calendar.DAY_OF_MONTH)
              ).show()*/
            /*val dialog = DatePickerDialog(this, { _, year, month, day_of_month ->
                calendar[Calendar.YEAR] = year
                calendar[Calendar.MONTH] = month + 1
                calendar[Calendar.DAY_OF_MONTH] = day_of_month
                val myFormat = "dd/MM/yyyy"
                val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
                var date: String = sdf.format(calendar.time)
                binding.etDate.setText(date)
            }, calendar[Calendar.YEAR], calendar[Calendar.MONTH], calendar[Calendar.DAY_OF_MONTH])
            dialog.datePicker.minDate = calendar.timeInMillis
            dialog.show()*/

            showDatePicker()


        })

        binding.ivCalendar.setOnClickListener() { showDatePicker() }


        binding.etTime.setOnClickListener(View.OnClickListener {
            TimePickerDialog(
                this,
                timePicker,
                calendar.get(Calendar.HOUR),
                calendar.get(Calendar.MINUTE),
                true
            ).show()
        })

        val tv_book = findViewById<TextView>(R.id.tv_book)
        tv_tot = findViewById<TextView>(R.id.tv_tot)

        var appPreference = AppPreference(this)
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)
        service_pro_id = appPreference.getString(AppConstant.SERVICE_PRO_ID)



//        Log.d("service_111","ser_pro_id :"+service_pro_id)
//        Log.d("service_111","uid : "+uid)
//        Log.d("service_111"," token : "+utoken)


        selected_service_id = intent.getStringExtra(AppConstant.SELECTED_SERVICE_ID)!!
        service_duration = intent.getStringExtra("service_duration")!!


        tv_book.setOnClickListener(View.OnClickListener {
            controller = AddBookingController(this, this)
            //userId:String,userToken:String,serviceId:String,bookingDate:String,bookingTime:String
            /* var date = binding.etDate.text.toString()
             var time = binding.etTime.text.toString() */
            var date = binding.etDate.text.toString()
            var time = ""
              if (date.isNullOrEmpty()) {
                  Toast.makeText(this, "Please select date ", Toast.LENGTH_SHORT).show()
                  return@OnClickListener
              } else if (binding.timeSpinner.getSelectedItem() == null && binding.timeSpinner.getSelectedItem().toString().isEmpty()) {
                      Toast.makeText(this, "Please select time ", Toast.LENGTH_SHORT).show()
                  return@OnClickListener
              }

            time = binding.timeSpinner.getSelectedItem().toString()

            controller.callAddBookingApi(
                uid,
                utoken,
                serviceId,
                date,
                time,
                service_duration
            )
        })
        controller1 = GetBookingServiceListController(this, this)
        controller1.callGetBookingServiceListApi(uid, utoken, selected_service_id)

        //getTimeSlots(uid,utoken,service_pro_id,"2022-07-01")

    }

    fun getTimeSlots(uid: String, utkone: String, service_pro_id: String, bookingDate: String){
        iGetTimeSlotsController = GetTimeSlotsController(this, this)
        iGetTimeSlotsController.calllGetTimeslotsApi(uid!!, utoken!!, service_pro_id!!, bookingDate)
    }

    fun setDate() {
        val myformate = "yyyy-MM-dd"
        val formate = SimpleDateFormat(myformate, Locale.US)
        Utils.printLog(TAG, "onCreate:" + formate.format(calendar.time))
        binding.etDate.setText(formate.format(calendar.time))
    }

    private fun showDatePicker() {
        val dialog = DatePickerDialog(this, { view, year, month, day_of_month ->
            calendar[Calendar.YEAR] = year
            calendar[Calendar.MONTH] = month
            calendar[Calendar.DAY_OF_MONTH] = day_of_month

            view.minDate = calendar.timeInMillis

            //val myFormat = "dd/MM/yyyy"
            val myFormat = "yyyy-MM-dd"
            val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
            var date: String = sdf.format(calendar.time)
            binding.etDate.setText(date)

            //call timeslot api function here
            getTimeSlots(uid, utoken, service_pro_id, date)

        }, calendar[Calendar.YEAR], calendar[Calendar.MONTH], calendar[Calendar.DAY_OF_MONTH])
        //            dialog.datePicker.minDate = calendar.timeInMillis
        dialog.show()
    }

    override fun onIAddBookingSuccess(response: AddBookingResponse, message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG)
//        Lists.setCategoryList(ArrayList())
//        var appPreference = AppPreference(this)
//        appPreference.setString(AppConstant.SUBCAT_ID, "")
        var intent = Intent(this, HomeActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK)
        startActivity(intent)
        finish()
    }

    /* override fun onIGetServiceSuccess(response: GetServiceProviderResponse, message: String) {
         rc_booking.layoutManager = LinearLayoutManager(this)
         res = response.result
         adpt = BookingAdapter(this, res)
         adpt!!.setOnCategoryClickListener(this)
         rc_booking.adapter = adpt
     }*/

    override fun onIGetBookingServiceListSuccess(
        response: GetBookingServiceListResponse?,
        message: String
    ) {
        var tot: Int = 0
        for (i in 0..response!!.result!!.size - 1) {
            tot += response.result!!.get(i).servicePrice.toInt()
            if (serviceId.isEmpty()) {
                serviceId += response!!.result!!.get(i).serviceId.toString()
            } else {
                serviceId = serviceId + "," + response!!.result!!.get(i).serviceId.toString()
            }
        }
        total = tot.toString()
        tv_tot.text = total
        binding.rcBooking.layoutManager = LinearLayoutManager(this)
        adpt = BookingAdapter(this, response)
        adpt!!.setOnCategoryClickListener(this)
        binding.rcBooking.adapter = adpt
    }

    override fun onGetTimeSlotSuccess(response: GetTimeSlotsResponse, message: String) {
        Utils.printLog(TAG, "onSuccess: Timeslots : >>" + response.timeSlots)

        if(response.timeSlots!=null && !response.timeSlots!!.isEmpty()){
            showTimeslots(response.timeSlots)
        }


    }

    private fun showTimeslots(timeSlots: List<String?>?) {
      /*  ArrayAdapter.createFromResource(
            this,
            R.array.time_array,
            android.R.layout.simple_spinner_item
        ).also { adapter ->
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.timeSpinner.adapter = adapter
        }
*/
        val spinnerArray: MutableList<String> = ArrayList()
        for (i in 0 until timeSlots!!.size){
            spinnerArray.add(timeSlots[i].toString())
        }


        if(!spinnerArray.isEmpty()){
            val adapter: ArrayAdapter<String> = ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, spinnerArray
            )

            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            binding.timeSpinner.adapter = adapter
        }



    }


    override fun onFailure(msg: String) {
        Utils.printLog(TAG, "onFailure: Booking Error : >>" + msg)

    }

    override fun showProgress() {

    }

    override fun dismissProgress() {

    }

    override fun onDeleteService(serviceId: String) {
        val list: List<String> = listOf(*selected_service_id.split(",").toTypedArray())
        var serviceId = ""

        for (i in 0..list.size-1) {
            if(!list[i].equals(serviceId))
            {
                serviceId=list[i]
            }
        }
        controller1 = GetBookingServiceListController(this, this)
        controller1.callGetBookingServiceListApi(uid, utoken, serviceId)
    }
}