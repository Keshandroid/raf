package com.raf.customer.view.activity

import android.content.ContentValues.TAG
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.adapter.SubServiceAdapter
import com.raf.customer.controller.GetSubCategoryController
import com.raf.customer.controller.listener.IGetSubCategoryController
import com.raf.customer.retrofit.response.GetSubCategoryResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetSubCategoryView
import kotlinx.android.synthetic.main.activity_sub_service.*
import kotlinx.android.synthetic.main.topbar_view.*

class SubServiceActivity : BaseActivity(), IGetSubCategoryView,
    SubServiceAdapter.onSubItemClickListener {
    lateinit var iGetSubCategoryController: IGetSubCategoryController

    /*   lateinit var preferences: SharedPreferences
       lateinit var editor: SharedPreferences.Editor*/
    var uid = ""
    var utoken = ""
    var cat_id = 0
    var cat_nm = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sub_service)
        /*  preferences = getSharedPreferences("Raf", MODE_PRIVATE)
          editor=preferences.edit()*/

        window.statusBarColor = getColor(R.color.app_header)
        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })

        /* uid = preferences.getString("uid", "")!!
         utoken = preferences.getString("utoken", "")!!*/
        var appPreference = AppPreference(this)
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)
        appPreference.setInt(AppConstant.CAT_ID, intent.getIntExtra(AppConstant.CAT_ID, 0))
        /* editor.putInt(AppConstant.CAT_ID,intent.getIntExtra(AppConstant.CAT_ID, 0))
         editor.commit()*/
        cat_id = intent.getIntExtra(AppConstant.CAT_ID, 0)
        cat_nm = intent.getStringExtra("cat_nm") ?: ""

        Utils.printLog(TAG, "onCreate:" + intent)

        txt_heading.setText(cat_nm)

        iGetSubCategoryController = GetSubCategoryController(this, this)
        iGetSubCategoryController.callGetSubcategoryApi(
            uid, utoken,
            appPreference.getString(AppConstant.SERVICE_PRO_ID),
            cat_id.toString()
        )
    }

    override fun onGetSubCategorySuccess(response: GetSubCategoryResponse, message: String) {

        if (message == "SubCategory_Got") {
            var subCat_list = response.getSubCategoryList()
            if (subCat_list.size != 0) {
                var adpt = SubServiceAdapter(this@SubServiceActivity, subCat_list)
                adpt.setOnClickListener(this)
                rc_hair.layoutManager = LinearLayoutManager(this@SubServiceActivity)
                rc_hair.adapter = adpt
            }

        } else {
            txt_noservice.visibility = View.VISIBLE


        }

    }

    override fun onFailure(msg: String) {

    }

    override fun onClick(data: GetSubCategoryResponse.SubCategory) {
        var appPreference = AppPreference(this)
        if(appPreference.getString(AppConstant.SUBCAT_ID).isEmpty())
        {
            appPreference.setString(AppConstant.SUBCAT_ID,data.subCategoryId.toString())
        }else{
            appPreference.setString(AppConstant.SUBCAT_ID,appPreference.getString(AppConstant.SUBCAT_ID)+","+data.subCategoryId.toString())
        }
        startActivity(
            Intent(this, ServiceDetailsActivity::class.java)
                .putExtra(AppConstant.SUBCAT_ID, data.subCategoryId)
                .putExtra(AppConstant.SUBCAT_NAME, data.subCategoryName)
                .putExtra(AppConstant.CAT_ID, cat_id).putExtra("cat_nm", cat_nm)
        )
       finish()


    }

}