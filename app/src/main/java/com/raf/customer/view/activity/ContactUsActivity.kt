package com.raf.customer.view.activity

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.View
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.controller.StaticPageController
import com.raf.customer.controller.listener.IStaticPageController
import com.raf.customer.retrofit.response.StaticPageResponse
import com.raf.customer.view.viewInterface.IStaticPageView
import kotlinx.android.synthetic.main.activity_contact_us.*
import kotlinx.android.synthetic.main.topbar_view.*

class ContactUsActivity :BaseActivity(), IStaticPageView {
  lateinit  var iStaticPageController:IStaticPageController
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        window.statusBarColor = getColor(R.color.app_header)
        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })
        txt_heading.setText("Contact Us")

        iStaticPageController=StaticPageController(this,this)
            iStaticPageController.callStaticPageApi("1")

    }

    override fun onIStaticPageSuccess(response: StaticPageResponse, message: String) {
        if(message.equals("Static_page_Got"))
        {
            contact_no.setText(response.getStaticPageResult().contactNo)
            location.setText(response.getStaticPageResult().location)
        }
    }

    override fun onFailure(msg: String) {

    }
}