package com.raf.customer.view.fragment.customer

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RelativeLayout
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.zxing.integration.android.IntentIntegrator
import com.raf.R
import com.raf.customer.adapter.CategoryListAdapter
import com.raf.customer.adapter.onItemMyClickListener
import com.raf.customer.controller.GetCategoryController
import com.raf.customer.controller.listener.IGetCategoryController
import com.raf.customer.retrofit.response.GetCategoryResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Lists
import com.raf.customer.view.activity.SubServiceActivity
import com.raf.customer.view.viewInterface.IGetCategoryView
import kotlinx.android.synthetic.main.fragment_home.*

class HomeFragment() : Fragment(R.layout.fragment_home), onItemMyClickListener, IGetCategoryView {
    lateinit var iGetCategoryController: IGetCategoryController
    var uid = ""
    var utoken = ""
    var cat_id = ""

    fun getInstance(idStr: String): HomeFragment {
        val frag = HomeFragment()

        val bundle = Bundle()
        bundle.putString("idStr", idStr)
        frag.arguments = bundle
        return frag
    }

    @SuppressLint("UseRequireInsteadOfGet")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        var appPreference = AppPreference(this.requireContext())
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)

        iGetCategoryController = GetCategoryController(this.requireActivity(), this)

        val rl_scan = view.findViewById<RelativeLayout>(R.id.rl_scan)
        rl_scan.setOnClickListener(View.OnClickListener {
            val intentIntegrator = IntentIntegrator(this.requireActivity())
            intentIntegrator.setCameraId(0)
            intentIntegrator.setOrientationLocked(true)
            intentIntegrator.setPrompt("Scan Qr code")
            intentIntegrator.initiateScan()
        })

        val idStr = savedInstanceState?.getString("idStr") ?: ""

        // todo remove after testing
//        appPreference.setString(AppConstant.PKey_LAST_SCANNED_ID ,"11")

        var lastScannedId = appPreference.getString(AppConstant.PKey_LAST_SCANNED_ID)
        appPreference.setString(AppConstant.SERVICE_PRO_ID , ""+lastScannedId)

        Log.d("lastScan",""+lastScannedId)

        iGetCategoryController.callGetcategotyApi(appPreference.getString(AppConstant.UID),appPreference.getString(AppConstant.UTOKEN),lastScannedId)
        if (!Lists.result1.isNullOrEmpty()) {
            var adpt = CategoryListAdapter(activity, Lists.result1)
            rc_category_list.layoutManager = LinearLayoutManager(activity)
            rc_category_list.adapter = adpt
            adpt.setOnCategoryClickListener(this)
            rl_scan.visibility = View.GONE
            rc_category_list.visibility = View.VISIBLE
        } else if (!lastScannedId.isNullOrEmpty()) {
            iGetCategoryController.callGetcategotyApi(appPreference.getString(AppConstant.UID),appPreference.getString(AppConstant.UTOKEN),lastScannedId)
        } else {
            rc_category_list.visibility = View.GONE
            rl_scan.visibility = View.VISIBLE
        }
    }


    override fun onClick(data: GetCategoryResponse.Result) {
        var intent = Intent(activity, SubServiceActivity::class.java)
        intent.putExtra(AppConstant.CAT_ID, data.categoryId)
        intent.putExtra("cat_nm", data.categoryName)
        requireActivity().startActivity(intent)
    }

    override fun onGetCategorySuccess(response: GetCategoryResponse, message: String) {

        var adpt = CategoryListAdapter(activity, Lists.result1)
        rc_category_list.layoutManager = LinearLayoutManager(activity)
        rc_category_list.adapter = adpt
        adpt.setOnCategoryClickListener(this)
        rl_scan.visibility = View.GONE
        rc_category_list.visibility = View.VISIBLE

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        var result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

        /*if (result != null) {

            Utils.printLog(ContentValues.TAG, "onActivityResult: " + result.contents.substringAfter(","))

            var id = result.contents.substringAfter(",")
            iGetCategoryController.callGetcategotyApi(uid, utoken, id)
            var appPreference = AppPreference(this.requireContext())
            appPreference.setString(AppConstant.SERVICE_PRO_ID, id).toString()

        }
        else{
            Utils.printLog(ContentValues.TAG, "nullll " )
        }*/

    }


    override fun onFailure(msg: String) {

    }

    override fun showProgress() {

    }

    override fun dismissProgress() {

    }


}