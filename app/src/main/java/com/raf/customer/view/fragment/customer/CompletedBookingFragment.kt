package com.raf.customer.view.fragment.customer

import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.View
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.raf.R
import com.raf.customer.adapter.CompletedBookingAdapter
import com.raf.customer.adapter.CurrentBookingAdapter
import com.raf.customer.controller.GetCompletedBookingController
import com.raf.customer.controller.listener.IGetCompletedBookingController
import com.raf.customer.retrofit.response.GetCurrentBookingResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.view.viewInterface.IGetCompletedBookingView
import kotlinx.android.synthetic.main.current_booking_fragment.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors


class CompletedBookingFragment: Fragment(R.layout.current_booking_fragment) ,
    IGetCompletedBookingView {

    var controller: IGetCompletedBookingController? = null;
    lateinit var preferences: SharedPreferences
    lateinit var editor: SharedPreferences.Editor
    var uid = ""
    var utoken = ""

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        /* preferences = requireActivity().getSharedPreferences("Raf", AppCompatActivity.MODE_PRIVATE)
         editor = preferences.edit()*/
        var appPreference = AppPreference(this.requireContext())
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)


        val executor: ExecutorService = Executors.newSingleThreadExecutor()
        val handler = Handler(Looper.getMainLooper())

        executor.execute(Runnable {
            controller = GetCompletedBookingController(this.requireActivity(), this)
            controller!!.callGetCompletedBookingApi(uid, utoken, "1")
            handler.post(Runnable {
                //UI Thread work here
            })
        })
    }

    override fun onIGetBookingSuccess(response: GetCurrentBookingResponse, message: String) {

        if(message == "Provider_Got"){
            if (response.result!!.size > 0) {
                rc_currentbooking.visibility = View.VISIBLE
                tv_not.visibility = View.GONE
                var adpt = CompletedBookingAdapter(activity, response)
                rc_currentbooking.layoutManager = LinearLayoutManager(activity)
                rc_currentbooking.adapter = adpt
            }else{
                rc_currentbooking.visibility = View.GONE
                tv_not.visibility = View.VISIBLE
            }
        }else{
            rc_currentbooking.visibility = View.GONE
            tv_not.visibility = View.VISIBLE
        }

    }

    override fun onFailure(msg: String) {
    }

    override fun showProgress() {
    }

    override fun dismissProgress() {
    }
}