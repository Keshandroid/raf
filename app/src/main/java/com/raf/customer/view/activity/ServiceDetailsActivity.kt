package com.raf.customer.view.activity

import android.content.ContentValues.TAG
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.google.gson.GsonBuilder
import com.raf.BaseActivity
import com.raf.R
import com.raf.customer.adapter.ServiceDetailsAdapter
import com.raf.customer.controller.GetServiceController
import com.raf.customer.controller.listener.IGetServiceController
import com.raf.customer.model.Booking
import com.raf.customer.retrofit.response.GetServiceResponse
import com.raf.customer.utils.AppConstant
import com.raf.customer.utils.AppPreference
import com.raf.customer.utils.Lists
import com.raf.customer.utils.Utils
import com.raf.customer.view.viewInterface.IGetServiceView
import kotlinx.android.synthetic.main.activity_service_details.*
import kotlinx.android.synthetic.main.topbar_view.*

class ServiceDetailsActivity : BaseActivity(), IGetServiceView,
    ServiceDetailsAdapter.onItemMyClickListener {
    var context: Context = this@ServiceDetailsActivity

    lateinit var iGetServiceController: IGetServiceController
    private var cat_id = 0
    private var uid = ""
    private var utoken = ""
    private var subcat_id = ""
    private var service_pro_id = ""
    private var cat_nm = ""
    private var selected_service_id = ""
    private var blist = ArrayList<Booking>()
    private var res: List<GetServiceResponse.Result?>? = null

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_service_details)


        var appPreference = AppPreference(this)
        uid = appPreference.getString(AppConstant.UID)
        utoken = appPreference.getString(AppConstant.UTOKEN)
        service_pro_id = appPreference.getString(AppConstant.SERVICE_PRO_ID)
        subcat_id = appPreference.getString(AppConstant.SUBCAT_ID)


        cat_id = intent.getIntExtra(AppConstant.CAT_ID, 0)
        cat_nm = intent.getStringExtra("cat_nm") ?: ""
        var subCatName = intent.getStringExtra(AppConstant.SUBCAT_NAME)
        txt_heading.setText(subCatName)
        window.statusBarColor = getColor(R.color.app_header)


        iGetServiceController = GetServiceController(this, this)
        iGetServiceController.callGetServiceApi(uid, utoken, service_pro_id, cat_id, subcat_id)


        back.setOnClickListener(View.OnClickListener {
            onBackPressed()
        })


        btn_book.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@ServiceDetailsActivity, BookingActivity::class.java).putExtra(
                AppConstant.SELECTED_SERVICE_ID,
                selected_service_id))
            finish()
        })

        btn_select.setOnClickListener(View.OnClickListener {
            // addBooking()
            var intent = Intent(this, SubServiceActivity::class.java)
            intent.putExtra(AppConstant.CAT_ID, cat_id)
            intent.putExtra("cat_nm", cat_nm)

            startActivity(intent)
            //startActivity(Intent(this@ServiceDetailsActivity,SubServiceActivity ::class.java))
        })
    }


    override fun onBackPressed() {
        super.onBackPressed()
        var appPreference = AppPreference(this)
        appPreference.setString(AppConstant.SUBCAT_ID, "")
    }

    override fun onGetServiceSuccess(response: GetServiceResponse, message: String) {
        if (message == "sevice_got") {

            Utils.printLog("ServiceDetail",
                "::: response - " + GsonBuilder().setPrettyPrinting().create()
                    .toJson(response) + " :::===")

            res = response.getResult()
            for (i in 0..response.result!!.size - 1) {
                if (selected_service_id.isEmpty()) {
                    selected_service_id += response!!.result!!.get(i)!!.serviceId

                } else {
                    selected_service_id =
                        selected_service_id + "," + response!!.result!!.get(i)!!.serviceId
                }
            }
            setdetails()
            var adapter = ServiceDetailsAdapter(this, response, this)
            rvServiceDetails.adapter = adapter
        }
    }


    private fun addBooking() {

        var booking = Booking()

        if (res?.size != 0) {

            booking.setServiceid(res?.get(0)?.serviceId.toString())
            booking.setServiceName(res?.get(0)?.serviceName!!)
            booking.setDesc(res?.get(0)?.description!!)
            booking.setPrice(res?.get(0)?.price!!)
            booking.setServiceImage(res?.get(0)?.serviceImage!!)
            blist.add(booking)

        }
        if (blist.size != 0) {

            Utils.printLog(TAG, "addBooking:" + blist.size)
            //  Utils.showToast(this,"BookingAdded")
            Lists.setBookingList(blist)
        }

    }


    override fun onFailure(msg: String) {
        showToast(msg)
    }

    private fun setdetails() {
//        txt_heading.setText(res?.get(0)?.serviceName)

    }

    override fun onClick(data: GetServiceResponse.Result) {
        var intent = Intent(this@ServiceDetailsActivity, BookingActivity::class.java)
        intent.putExtra(AppConstant.SELECTED_SERVICE_ID, "" + data.serviceId)
        intent.putExtra("service_duration", "" + data.avgTime)
        startActivity(intent)
    }

}